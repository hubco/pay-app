package com.gappscorp.aeps.library.common.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.extensions.dialog.alert
import com.gappscorp.aeps.library.data.model.BiometricDevice
import timber.log.Timber

private const val MARKET_URL_PREFIX = "https://play.google.com/store/apps/details?id="

const val GET_DEVICE_INFO = 111
const val DEVICE_AUTHENTICATION_REQUEST = 112

//private fun getDevicePackageName(deviceName: String): String? {
//    return when (deviceName.toLowerCase(Locale.getDefault())) {
//        "mantra" -> "com.mantra.rdservice"
//        "morpho" -> "com.scl.rdservice"
//        "nextrd" -> "com.nextbiometrics.onetouchrdservice"//"com.nextbiometrics.rdservice"
//        "secugen" -> "com.secugen.rdservice"
//        "precision" -> "com.precision.pb510.rdservice"
//        "startek" -> "com.acpl.registersdk"
//        "aratex" -> "co.aratek.asix_gms.rdservice"
//        else -> null
//    }
//}

fun Activity.checkDeviceStatus(device: BiometricDevice) {
    try {
        val intent = Intent("in.gov.uidai.rdservice.fp.INFO")
            .setPackage(device.packageName)
        startActivityForResult(intent, GET_DEVICE_INFO)
    } catch (ex: ActivityNotFoundException) {
        alert {
            title = getString(R.string.alert_title_error)
            message = getString(R.string.device_service_not_found)
            positiveButton(R.string.dialog_yes) {
                openInPlayStore(device.packageName)
            }
            negativeButton(R.string.dialog_no) {}
        }.show()
    }
}

fun Activity.scanFingerprint(device: BiometricDevice, pidOptionsXml: String) {
    Timber.d("scanFingerprint: pidOptionsXml -> $pidOptionsXml")
    try {
        val intent: Intent =
            Intent("in.gov.uidai.rdservice.fp.CAPTURE")
                .setPackage(device.packageName)
                .putExtra("PID_OPTIONS", pidOptionsXml)
        startActivityForResult(
            intent,
            DEVICE_AUTHENTICATION_REQUEST
        )
    } catch (ex: ActivityNotFoundException) {
        alert {
            title = getString(R.string.alert_title_error)
            message = getString(R.string.device_service_not_found)
            positiveButton(R.string.dialog_yes) {
                openInPlayStore(device.packageName)
            }
            negativeButton(R.string.dialog_no) {}
        }.show()
    }
}

fun Intent?.getDeviceInfo(): String? {
    this?.extras?.let {
        val deviceInfo = it.getString("DEVICE_INFO", "")
        val rdServiceInfo = it.getString("RD_SERVICE_INFO", "")
        val dnc = it.getString("DNC", "")
        val dnr = it.getString("DNR", "")
        Timber.d("getDeviceInfo: $dnc$dnr $deviceInfo$rdServiceInfo")
//        return if (dnc.isNotEmpty() || dnr.isNotEmpty()) {
//            "$dnc$dnr $deviceInfo$rdServiceInfo"
//        } else {
//            "$deviceInfo$rdServiceInfo"
//        }
        return rdServiceInfo
    }
    return null
}

fun Intent?.getFingerprintScanResult(): String? {
    this?.extras?.let {
        return it.getString("PID_DATA")
//        val dnr = it.getString("DNR", "")
    }
    return null
}

//private fun getXmlData(xmlString: String, tag: String) : String {
//
//}

fun Context.openInPlayStore(packageName: String?) {
    val uri = Uri.parse("$MARKET_URL_PREFIX$packageName")
    val intent = Intent(Intent.ACTION_VIEW, uri)
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    }
}