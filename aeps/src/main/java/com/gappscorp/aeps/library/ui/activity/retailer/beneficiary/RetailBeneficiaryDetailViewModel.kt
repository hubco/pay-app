package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.RetailRegisterRequest
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class RetailBeneficiaryDetailViewModel(private val repository: RetailerRepository) : ViewModel() {

    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()

    val firstNameData = MutableLiveData<String>()
    val lastNameData = MutableLiveData<String>()
    val mobileNumberData = MutableLiveData<String>()
    val addressData = MutableLiveData<String>()
    val stateIdData = MutableLiveData<String>()
    val pincodeData = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()

    val registerButtonEnable = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(firstNameData) {
            value = hasRegisterData()
        }
        addSource(lastNameData) {
            value = hasRegisterData()
        }
        addSource(mobileNumberData) {
            value = hasRegisterData()
        }
        addSource(addressData) {
            value = hasRegisterData()
        }
        addSource(stateIdData) {
            value = hasRegisterData()
        }
        addSource(pincodeData) {
            value = hasRegisterData()
        }
    }

    fun onSelectBankClicked() {
        selectBankClickEvent.call()
    }

    val request
        get() =
            RetailRegisterRequest(
                firstName = firstNameData.value,
                lastName = lastNameData.value,
                mobileNumber = mobileNumberData.value,
                address = addressData.value,
                stateId = stateIdData.value,
                pinCode = pincodeData.value
            )

    val selectBankClickEvent = SingleLiveEvent<Void>()
    val quickLoginClickEvent = SingleLiveEvent<Void>()
    val forgotClickEvent = SingleLiveEvent<Void>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<PhoneRemitterResponse>>()
    val resultData: LiveData<Result<PhoneRemitterResponse>>
        get() = _resultData

    private fun hasRegisterData(): Boolean {
        return !firstNameData.value.isNullOrEmpty() && !lastNameData.value.isNullOrEmpty() &&
                !mobileNumberData.value.isNullOrEmpty() && !addressData.value.isNullOrEmpty() &&
                !stateIdData.value.isNullOrEmpty() && !pincodeData.value.isNullOrEmpty() &&
                mobileNumberData.value!!.length == 10
    }

    fun onRegisterButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.retailRegister(request)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }






    override fun onCleared() {
        super.onCleared()
        registerButtonEnable.removeSource(firstNameData)
        registerButtonEnable.removeSource(lastNameData)
        registerButtonEnable.removeSource(mobileNumberData)
        registerButtonEnable.removeSource(addressData)
        registerButtonEnable.removeSource(stateIdData)
        registerButtonEnable.removeSource(pincodeData)
    }
}