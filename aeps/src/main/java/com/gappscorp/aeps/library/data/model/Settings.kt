package com.gappscorp.aeps.library.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
data class Settings(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("biometric_device_types_new")
    val biometricDeviceList: Map<String, String> = mutableMapOf(),
    @SerializedName("payout_type")
    val payoutTypes: List<String> = mutableListOf(),
    @SerializedName("dmt_type")
    val dmtTypes: List<String> = mutableListOf(),
    @SerializedName("bank_list")
    val bankList: List<BankInfo> = mutableListOf(),
    @SerializedName("rnfi_aeps_bank_list")
    val aepsBankList: List<BankInfo> = mutableListOf(),
    @SerializedName("rnfi_aeps_bank_list_ms")
    val aepsMiniStatementBankList: List<BankInfo> = mutableListOf(),
    @SerializedName("states")
    val states: List<List<String>>
)

@Parcelize
data class BankInfo(
    @SerializedName("bank_name")
    val bankName: String,
    @SerializedName("bank_code")
    val bankCode: String
) : Parcelable

enum class BankListType {
    BANK_LIST,
    AEPS_BANK_LIST,
    AEPS_MINI_STATEMENT_BANK_LIST
}