package com.gappscorp.aeps.library.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gappscorp.aeps.library.data.database.dao.SettingsDao
import com.gappscorp.aeps.library.data.database.dao.UserDao
import com.gappscorp.aeps.library.data.model.RetailerInfo
import com.gappscorp.aeps.library.data.model.Settings
import com.gappscorp.aeps.library.data.model.UserInfo

@Database(
    entities = [Settings::class, UserInfo::class, RetailerInfo::class],
    version = 4,
    exportSchema = false
)
@TypeConverters(Converter::class)
abstract class AepsDatabase : RoomDatabase() {
    abstract fun settingsDao(): SettingsDao
    abstract fun userDao(): UserDao
}