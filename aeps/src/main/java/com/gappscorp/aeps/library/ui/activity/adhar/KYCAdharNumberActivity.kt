package com.gappscorp.aeps.library.ui.activity.adhar

import android.location.Location
import android.os.Bundle
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.extensions.*
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateUpTo
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityAdharNumberBinding
import com.gappscorp.aeps.library.ui.activity.aeps.base.BaseAepsActivity
import kotlinx.android.synthetic.main.aeps_activity_cash_withdrawal.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class KYCAdharNumberActivity : BaseAepsActivity<AepsActivityAdharNumberBinding>() {

    override val viewModel: AdharViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_adhar_number
    val pidXml = """<?xml version="1.0"?>
            <PidOptions ver="1.0">
                <Opts
                    fCount="1"
                    fType="0"
                    iCount="0"
                    pCount="0"
                    format="0"
                    pidVer="2.0"
                    timeout="10000"
                    wadh="E0jzJ/P8UopUHAieZn8CKqS4WPMi5ZSYXgfnlfkWjrc="
                    posh="UNKNOWN"
                    env="P" / >
                <CustOpts >
                    <Param
                        name="mantrakey"
                        value="" / >
                </CustOpts>
            </PidOptions>"""

    override fun registerObserver() {
        viewModel.isKYCSuccess.observe(this,{
            if(it.equals(true)){
                this.navigateUpTo(Routes.DASHBOARD_SCREEN,true)
            }
        })

        viewModel.statusData.observe(this,{
            if(it.equals(Status.ERROR))this.toast("Authentication failed")
        })
        viewModel.displayMessage.observe(this,{
            this.toast(it)
        })

        viewModel.selectDeviceClickEvent.observe(this, {
            spScanDevice.performClick()
        })

        viewModel.deviceName.observeOnce(this) {
            if (!it.equals(sessionManager.lastUsedDevice, ignoreCase = true)) {
                viewModel.deviceName.value =
                    sessionManager.lastUsedDevice.capitalize(Locale.getDefault())
            }
        }
        viewModel.deviceScanClickEvent.observe(this, {
            // check for device online
            checkDeviceStatus(viewModel.selectedDevice!!)
        })
        viewModel.fingerprintScanClickEvent.observe(this, {
            scanFingerprint(viewModel.selectedDevice!!, pidXml)
        })

    }

    override fun onDeviceServiceConnected(connected: Boolean, serviceInfo: String?) {
        viewModel.onDeviceScanCompleted(connected)
        viewModel.isDeviceOnline.value = connected
    }

    override fun onFingerprintScanned(scanned: Boolean, pidData: String?) {
        viewModel.onFingerprintScanCompleted(scanned)
        viewModel.fingerPrintScanData.value = pidData
    }

    override fun onLocationReceived(location: Location?) {
        location?.let {
            viewModel.latitude.value = "${it.latitude}"
            viewModel.longitude.value = "${it.longitude}"
        }
    }
}