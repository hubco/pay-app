package com.gappscorp.aeps.library.ui.activity.profile

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.databinding.AepsActivityUserProfileBinding
import com.gappscorp.aeps.library.ui.activity.adhar.KYCAdharNumberActivity
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import com.google.android.material.navigation.NavigationView
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserProfileActivity : BaseActivity<AepsActivityUserProfileBinding>() {

    override val viewModel: UserProfileViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_user_profile

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        super.registerObserver()
        viewModel.getKycInvoked.observe(this,{
            navigateTo(Routes.KYC_SCREEN)
        })
    }
}