package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhoneRemitterResponse(
    val status: String,
    val message: String?,
    @SerializedName("remitter")
    var remitterInfo: RemitterInfo?,
    @SerializedName("beneficiaries")
    var beneficiaryList: ArrayList<BeneficiaryInfo>?
) : Parcelable

@Parcelize
data class RemitterInfo(
    @SerializedName("id")
    val remitterId: String?,
    @SerializedName("company_id")
    val companyId: String?,
    @SerializedName("user_id")
    val userId: String?,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("dob")
    val dateOfBirth: String?,
    @SerializedName("address")
    val address: String?,
    @SerializedName("pincode")
    val pincode: String?,
    @SerializedName("state_id")
    val stateId: String?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("is_verified")
    val isVerified: Boolean?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("ref_id")
    val refId: String?,
    @SerializedName("consumedlimit")
    val consumeLimit: String?,
    @SerializedName("remaininglimit")
    val remainingBalance: String?
) : Parcelable

@Parcelize
data class BeneficiaryInfo(
    @SerializedName("id")
    val beneficiaryId: String?,
    @SerializedName("name")
    val beneficiaryName: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("ifsc_code")
    val ifscCode: String?,
    @SerializedName("account_no")
    val accountNumber: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("ref_id")
    val refId: String?,
    @SerializedName("b_type")
    val balanceType: String?,
    @SerializedName("is_verified")
    val isVerified: Boolean?,
    @SerializedName("is_otp_verified")
    val isOtpVerified: Boolean?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("bank_name")
    val bankName: String?
) : Parcelable