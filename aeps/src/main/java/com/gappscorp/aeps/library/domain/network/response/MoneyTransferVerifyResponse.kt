package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoneyTransferVerifyResponse(
    @SerializedName("status")
    var status: String,
    @SerializedName("message")
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    @SerializedName("amount")
    val amount: String?,
    @SerializedName("charges")
    val charges: String?,
    @SerializedName("t_amount")
    val totalAmount: String?
) : Parcelable