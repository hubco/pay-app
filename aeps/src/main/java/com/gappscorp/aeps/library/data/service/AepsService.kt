package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.domain.network.request.BalanceEnquiryRequest
import com.gappscorp.aeps.library.domain.network.request.CashWithdrawalRequest
import com.gappscorp.aeps.library.domain.network.request.MATMProcessRequest
import com.gappscorp.aeps.library.domain.network.request.MiniStatementRequest
import com.gappscorp.aeps.library.domain.network.response.*
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AepsService {

    @POST("api/retailer-api/v1/aeps/cash-withdrawal")
    suspend fun cashWithdrawal(@Body request: CashWithdrawalRequest) : Response<CashWithdrawalResponse>

    @POST("api/retailer-api/v1/aeps/balance-enquiry")
    suspend fun balanceEnquiry(@Body request: BalanceEnquiryRequest) : Response<BalanceEnquiryResponse>

    @POST("api/retailer-api/v1/aeps/mini-statement")
    suspend fun miniStatement(@Body request: MiniStatementRequest) : Response<MiniStatementResponse>

    @POST("api/retailer-api/v1/matm/initiate-transaction")
    suspend fun sendMATMBeData(@Body request: MATMProcessRequest): Response<MAtmBEResponse>

    @POST("api/retailer-api/v1/matm/process-transaction")
    suspend fun processMATMTransaction(@Body response: RequestBody?):  Response<MAtmTransactionResponse>

//    @POST("api/retailer-api/v1/aeps/cash-deposit")
//    suspend fun cashDeposit(request: CashWithdrawalRequest)
}