package com.gappscorp.aeps.library.data.network

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}