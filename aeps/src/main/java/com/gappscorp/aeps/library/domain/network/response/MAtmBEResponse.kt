package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MAtmBEResponse(
    val status: String,
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    @SerializedName("t_id")
    val tId: String?,
    @SerializedName("partner_id")
    val partnerId: String?,
    @SerializedName("jwt_key")
    val jwtKey: String?,
    @SerializedName("latitude")
    val latitude: String?,
    @SerializedName("longitude")
    val longitude: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("sub_merchant_id")
    val subMerchantId: String?,
    var matmType: String?,
    var remark: String?,
    @SerializedName("amount")
    val amount: String?,
    @SerializedName("manufacturer_code")
    val manufacturerId: Int?
): Parcelable