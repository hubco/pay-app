package com.gappscorp.aeps.library.ui.activity.aeps.statement.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.databinding.AepsLayoutStatementItemBinding
import com.gappscorp.aeps.library.domain.network.response.MiniStatement

class MiniStatementAdapter(
    private val miniStatementList: List<MiniStatement>) :  RecyclerView.Adapter<MiniStatementAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiniStatementAdapter.ViewHolder {
        val binding = AepsLayoutStatementItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return  ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (miniStatementList.isNullOrEmpty()) 0 else miniStatementList.size
    }

    override fun onBindViewHolder(holder: MiniStatementAdapter.ViewHolder, position: Int) {
            holder.bind(miniStatementList[position])
    }

    inner class ViewHolder(val binding: AepsLayoutStatementItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MiniStatement) {
            binding.item = item
        }
    }
}