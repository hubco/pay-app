package com.gappscorp.aeps.library.ui.activity.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.toast
import com.gappscorp.aeps.library.common.navigation.navigateToLogin
import com.gappscorp.aeps.library.common.session.SessionManager
import org.koin.android.ext.android.inject

abstract class CoreActivity : AppCompatActivity() {

    protected val sessionManager: SessionManager by inject()

    protected fun setupToolBar(toolbar: Toolbar?, hasUpAction: Boolean) {
        toolbar?.let {
            setSupportActionBar(it)
            // set up button state
            supportActionBar?.run {
                title = null
                setDisplayHomeAsUpEnabled(hasUpAction)
                setDisplayShowHomeEnabled(hasUpAction)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, IntentFilter(Constants.ACTION_SESSION_EXPIRED))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            toast(R.string.session_expired)
            sessionManager.logout()
            navigateToLogin()
        }
    }
}