package com.gappscorp.aeps.library.data.mapper

import com.gappscorp.aeps.library.data.mapper.base.BaseMapper
import com.gappscorp.aeps.library.data.model.State

class StateListMapper : BaseMapper<List<String>, State>() {

    override fun map(entity: List<String>?): State {
        if (entity == null) return State("", "")
        return State(entity[0], entity[1])
    }
}