package com.gappscorp.aeps.library.common.extensions.dialog

import `in`.aabhasjindal.otptextview.OTPListener
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import androidx.core.widget.doAfterTextChanged
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.architecture.singleClickEvent
import com.gappscorp.aeps.library.common.extensions.loadGif
import com.gappscorp.aeps.library.databinding.AepsAlertMatmCwBinding
import com.gappscorp.aeps.library.databinding.AepsDialogOtpVerificationBinding
import com.gappscorp.aeps.library.databinding.AepsViewAlertSuccessfulBinding


fun Context.alert(
    messageResource: Int,
    titleResource: Int? = null,
    init: (AlertBuilder<DialogInterface>.() -> Unit)? = null
): AlertBuilder<DialogInterface> {
    return AppThemeAlertBuilder(this).apply {
        if (titleResource != null) {
            this.titleResource = titleResource
        }
        this.messageResource = messageResource
        if (init != null) init()
    }
}

fun Context.alert(init: AlertBuilder<DialogInterface>.() -> Unit): AlertBuilder<DialogInterface> =
    AppThemeAlertBuilder(this).apply { init() }

fun Context.genericAlert(): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_error)
        message = getString(R.string.alert_generic_error_message)
        positiveButton(android.R.string.ok) {}
    }.show()

fun Context.errorAlert(error: String?, callback: (() -> Unit)? = null): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_error)
        message = error ?: getString(R.string.alert_generic_error_message)
        positiveButton(android.R.string.ok) { callback?.invoke() }
    }.show()

fun Context.confirmAlert(msg: String?, callback: (() -> Unit)? = null): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title = getString(R.string.alert_title_confirm)
        message = msg ?: getString(R.string.alert_generic_confirmation)
        positiveButton(android.R.string.ok) { callback?.invoke() }
        negativeButton(android.R.string.cancel) {

        }
    }.show()

fun Context.infoAlert(
    message: String? = getString(R.string.alert_message_action_completed),
    title: String? = getString(R.string.alert_title_info),
    callback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title?.let {
            this.title = it
        }
        message?.let {
            this.message = it
        }
        positiveButton(android.R.string.ok) { callback?.invoke() }
    }.show()

fun Context.successAlert(
    message: String?,
    title: String? = getString(R.string.alert_title_success),
    callback: (() -> Unit)? = null
): AlertDialog =
    AppThemeAlertBuilder(this).apply {
        title?.let {
            this.title = it
        }
        val binding =
            AepsViewAlertSuccessfulBinding.inflate(LayoutInflater.from(this@successAlert))
        binding.ivSuccess.loadGif(R.drawable.aeps_ic_successful)
        binding.tvMessage.text = message
        customView = binding.root

        positiveButton(android.R.string.ok) {
            callback?.invoke()
        }
    }.show()

fun Context.otpAlert(
    message: String?,
    otpPrefix: String?,
    callback: ((isResendOtp: Boolean, otpCode: String?) -> Unit)? = null
): AlertDialog {
    var alertDialog: AlertDialog? = null
    alertDialog = AppThemeAlertBuilder(this).apply {
        title = getString(R.string.title_verify_otp)
        val binding =
            AepsDialogOtpVerificationBinding.inflate(LayoutInflater.from(this@otpAlert))
        binding.tvMessage.text = message
        binding.tvPrefix.text = otpPrefix
        binding.btnVerify.isEnabled = false

        binding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                binding.btnVerify.isEnabled = false
            }

            override fun onOTPComplete(otp: String) {
                binding.btnVerify.isEnabled = true
            }
        }
        customView = binding.root

        binding.btnResendOtp.singleClickEvent {
            callback?.invoke(true, null)
        }

        binding.btnVerify.singleClickEvent {
            callback?.invoke(false, binding.otpView.otp)
        }

        binding.btnCancel.singleClickEvent {
            alertDialog?.dismiss()
        }

    }.show()
    return alertDialog
}

fun Context.matmCWAlert(
    isMATMCw: Boolean,
    callback: ((amount: String?, mobileNo: String?, remark: String?) -> Unit)? = null
): AlertDialog {
    var alertDialog: AlertDialog? = null
    alertDialog = AppThemeAlertBuilder(this).apply {

        title = if (isMATMCw) getString(R.string.label_matmcw) else getString(R.string.label_matmbe)
        val binding =
            AepsAlertMatmCwBinding.inflate(LayoutInflater.from(this@matmCWAlert))
        if (!isMATMCw) {
            binding.tlAmount.visibility = View.GONE
        }
        binding.btnSubmitCW.isEnabled = false

        binding.etMoblie.doAfterTextChanged {
            run {
                binding.btnSubmitCW.isEnabled = isMATMCWValid(binding, isMATMCw)
            }
        }

        binding.etAmount.doAfterTextChanged {
            run {
                binding.btnSubmitCW.isEnabled = isMATMCWValid(binding, isMATMCw)

            }
        }

        binding.etRemark.doAfterTextChanged {
            run {
                binding.btnSubmitCW.isEnabled = isMATMCWValid(binding, isMATMCw)

            }
        }

        customView = binding.root

        binding.btnSubmitCW.singleClickEvent {
            callback?.invoke(
                binding.etAmount.text.toString(),
                binding.etMoblie.text.toString(),
                binding.etRemark.text.toString()
            )
            alertDialog?.dismiss()
        }

        binding.btnCancel.singleClickEvent {
            alertDialog?.dismiss()
        }

    }.show()
    return alertDialog
}

private fun isMATMCWValid(binding: AepsAlertMatmCwBinding, isMATMCW: Boolean): Boolean {
    return if (isMATMCW)
        !binding.etMoblie.text.isNullOrEmpty()
                && binding.etMoblie.length() == 10
                && !binding.etAmount.text.isNullOrEmpty()
                && !binding.etRemark.text.isNullOrEmpty()
    else
        !binding.etMoblie.text.isNullOrEmpty()
                && binding.etMoblie.length() == 10
                && !binding.etRemark.text.isNullOrEmpty()
}