package com.gappscorp.aeps.library.ui.activity.splash

import android.os.Bundle
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.ui.activity.base.CoreActivity
import org.koin.android.ext.android.inject

open class SplashActivity : CoreActivity() {

    private val session: SessionManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.BaseTheme)
        super.onCreate(savedInstanceState)
        // check for login state
        val target = if (session.isLoggedIn()) {
            Routes.DASHBOARD_SCREEN
        } else {
            Routes.LOGIN_SCREEN
        }

        navigateTo(target)
        finish()
    }
}