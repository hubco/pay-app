package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResendOtpResponse(
    @SerializedName("status")
    var status: String,
    @SerializedName("message")
    val message: String?,
    @SerializedName("slug")
    val slug: String?
) : Parcelable