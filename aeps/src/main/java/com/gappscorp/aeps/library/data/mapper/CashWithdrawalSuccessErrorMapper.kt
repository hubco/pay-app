package com.gappscorp.aeps.library.data.mapper

import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.extensions.maskAadharNumber
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.data.model.SuccessErrorData
import com.gappscorp.aeps.library.data.model.SuccessErrorItem
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.request.CashWithdrawalRequest
import com.gappscorp.aeps.library.domain.network.response.CashWithdrawalResponse
import org.koin.core.KoinComponent
import org.koin.core.inject

class CashWithdrawalSuccessErrorMapper : KoinComponent {

    private val sessionManager: SessionManager by inject()

    fun success(entity: CashWithdrawalResponse?): SuccessErrorData {
        val context = CoreApp.appContext
        val itemList = mutableListOf<SuccessErrorItem>().apply {
            add(
                SuccessErrorItem(
                    context.getString(R.string.label_withdrawal_amount), "${sessionManager.currency} ${entity?.amount}"
                )
            )
            add(
                SuccessErrorItem(
                    context.getString(R.string.available_balance_txt), "${sessionManager.currency} ${entity?.availableBalance}"
                )
            )
            add(
                SuccessErrorItem(
                    context.getString(R.string.aadhaar_number_txt), entity?.aadhaarNumber?.maskAadharNumber()
                )
            )
            add(SuccessErrorItem(context.getString(R.string.bank_name_txt), entity?.bankName))
            add(SuccessErrorItem(context.getString(R.string.rrn_number_txt), entity?.bankRrnNumber))
        }
        return SuccessErrorData(
            orderId = entity?.transactionId,
            amount = entity?.amount,
            message = entity?.message,
            itemList = itemList
        )
    }

    fun failure(error: Result.Error?, request: CashWithdrawalRequest?): SuccessErrorData {
        val context = CoreApp.appContext
        val notApplicable = context.getString(R.string.not_applicable)
        val itemList = mutableListOf<SuccessErrorItem>().apply {
            add(SuccessErrorItem(context.getString(R.string.label_withdrawal_amount), request?.amount ?: notApplicable))
            add(SuccessErrorItem(context.getString(R.string.available_balance_txt), notApplicable))
            add(SuccessErrorItem(context.getString(R.string.aadhaar_number_txt), request?.aadhaarNumber?.maskAadharNumber()))
            add(SuccessErrorItem(context.getString(R.string.bank_name_txt), request?.bankName))
            add(SuccessErrorItem(context.getString(R.string.rrn_number_txt), notApplicable))
        }
        return SuccessErrorData(
            orderId = error?.id ?: notApplicable,
            amount = request?.amount ?: notApplicable,
            message = error?.error,
            itemList = itemList
        )
    }
}