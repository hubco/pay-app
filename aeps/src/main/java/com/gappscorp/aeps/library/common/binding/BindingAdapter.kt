package com.gappscorp.aeps.library.common.binding

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.callback.BankItemCallback
import com.gappscorp.aeps.library.common.callback.BeneficiaryItemCallback
import com.gappscorp.aeps.library.common.callback.StateItemCallback
import com.gappscorp.aeps.library.common.extensions.*
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.data.model.SuccessErrorItem
import com.gappscorp.aeps.library.data.model.UserProfileItem
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsUserProfileListItemBinding
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo
import com.gappscorp.aeps.library.domain.network.response.MiniStatement
import com.gappscorp.aeps.library.ui.activity.aeps.banklist.adapter.BankListAdapter
import com.gappscorp.aeps.library.ui.activity.aeps.statement.list.adapter.MiniStatementAdapter
import com.gappscorp.aeps.library.ui.activity.result.adapter.SuccessErrorAdapter
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list.adapter.GetBeneficiaryListAdapter
import com.gappscorp.aeps.library.ui.activity.state.adapter.StateListAdapter
import com.gappscorp.aeps.library.ui.widget.ProgressButton
import java.util.*

@BindingAdapter("loadingStatus")
fun showLoadingStatus(view: View, status: Status?) {
    status?.let {
        when (it) {
            Status.LOADING -> view.showView()
            else -> view.hideView()
        }
    }
}

@BindingAdapter("loadingStatus")
fun showLoadingStatus(view: ProgressButton, status: Status?) {
    status?.let {
        view.setLoadingVisibility(it == Status.LOADING)
    }
}

@BindingAdapter("animationStatus")
fun showAnimationStatus(view: View, status: Status?) {
    status?.let {
        when (it) {
            Status.LOADING -> view.rotate()
            else -> view.animation?.cancel()
        }
    }
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean?) {
    visible?.let { view.isVisible = it }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("amount")
fun setAmount(view: TextView, amount: String?) {
    if (!amount.isNullOrEmpty()) {
        try {
            amount.toDouble()
            val currency = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(SessionManager(view.context).currency, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(SessionManager(view.context).currency)
            }
            view.text = "$currency ${amount.trim()}"
        } catch (ex: NumberFormatException) {
            view.text = amount.trim()
        }
    } else {
        view.text = amount?.trim()
    }
}

@BindingAdapter(value = ["bankList", "callback"], requireAll = false)
fun setBankListAdapter(view: RecyclerView, list: List<BankInfo>?, callback: BankItemCallback?) {
    list?.let {
        view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = BankListAdapter(it, callback)
        }
    }
}

@BindingAdapter("deviceList")
fun setSpinnerAdapter(view: Spinner, list: List<String>?) {
    list?.let { deviceList ->
        view.adapter = ArrayAdapter(
            view.context,
            R.layout.aeps_layout_spinner_item,
            deviceList.map { it.capitalize(Locale.getDefault()) }).apply {
            setDropDownViewResource(android.R.layout.simple_list_item_1)
        }
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter(value = ["selectedValue", "selectedValueAttrChanged"], requireAll = false)
fun bindSpinnerData(
    spinner: Spinner,
    newSelectedValue: String?,
    newTextAttrChanged: InverseBindingListener
) {
    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View,
            position: Int,
            id: Long
        ) {
            newTextAttrChanged.onChange()
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    newSelectedValue?.let { value ->
        (spinner.adapter as ArrayAdapter<Any>?)?.getPosition(value)?.let {
            spinner.setSelection(it, true)
        }
    }
}

@InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
fun captureSelectedSpinnerValue(spinner: Spinner): Any? {
    return spinner.selectedItem
}

@BindingAdapter("miniStatementList")
fun setMiniStatementList(view: RecyclerView, list: List<MiniStatement>?) {
    list?.let {
        view.apply {
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            layoutManager = LinearLayoutManager(context)
            adapter = MiniStatementAdapter(it)
        }
    }
}

@BindingAdapter("radioList")
fun setRadioList(view: RadioGroup, list: List<String>?) {
    val params = RadioGroup.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT
    ).apply {
        marginEnd = 8
    }
    view.removeAllViews()
    list?.forEach {
        val radio = RadioButton(view.context).apply {
            layoutParams = params
            text = it
        }
        view.addView(radio)
    }
    (view.getChildAt(0) as RadioButton?)?.isChecked = true
}

@BindingAdapter("successErrorList")
fun setSuccessErrorListAdapter(view: RecyclerView, list: List<SuccessErrorItem>?) {
    list?.let {
        view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SuccessErrorAdapter(it)
        }
    }
}

@BindingAdapter("successErrorIcon")
fun setImageResource(imageView: ImageView, resource: Int?) {
    resource?.let {
        imageView.setImageResource(it)
    }
}

@BindingAdapter("successErrorBackground")
fun setBackgroundImageResource(view: View, resource: Int?) {
    resource?.let {
        view.background = ContextCompat.getDrawable(view.context, it)
    }
}

@BindingAdapter("profileImage")
fun loadImage(view: ImageView, profileImage: String?) {
    view.load(profileImage, R.drawable.aeps_ic_person)
}

@BindingAdapter("inflateData")
fun inflateData(layout: LinearLayout, data: List<UserProfileItem?>?) {
    if (data != null) {
        for (item in data) {
            val binding = AepsUserProfileListItemBinding.inflate(
                LayoutInflater.from(layout.context),
                layout,
                true
            )
            binding.title = item?.title
            binding.value = item?.value
        }
    }
}

@BindingAdapter("formValidator")
fun formValidator(editText: AppCompatEditText, errorText: String?) {
    // ignore infinite loops
    if (TextUtils.isEmpty(errorText)) {
        editText.error = null
        return
    }
    editText.error = errorText
}

@BindingAdapter(
    value = ["beneficiaryList", "callback", "isRemoveItem", "index"],
    requireAll = false
)
fun setBeneficiaryList(view: RecyclerView, list: ArrayList<BeneficiaryInfo>?,
                       callback: BeneficiaryItemCallback?,
                       isRemoved: Boolean?,
                       index: Int?
) {
    view.apply {
        isRemoved?.let {
            if (it) {
                index?.let {
                    list?.removeAt(index)
                    view.adapter?.notifyItemRemoved(index)
                }
                return@apply
            }
        }

        list?.let {
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            layoutManager = LinearLayoutManager(context)
            adapter = GetBeneficiaryListAdapter(it, callback)
        }
    }
}

@BindingAdapter(value = ["stateList", "callback"], requireAll = false)
fun setStateListAdapter(view: RecyclerView, list: List<State>?, callback: StateItemCallback?) {
    list?.let {
        view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = StateListAdapter(list, callback)
        }
    }
}