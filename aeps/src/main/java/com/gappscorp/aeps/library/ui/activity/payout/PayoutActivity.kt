package com.gappscorp.aeps.library.ui.activity.payout

import android.app.AlertDialog
import android.os.Bundle
import android.widget.RadioButton
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.dialog.infoAlert
import com.gappscorp.aeps.library.common.extensions.dialog.otpAlert
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateToSuccessErrorScreen
import com.gappscorp.aeps.library.data.mapper.PayoutSuccessErrorMapper
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsActivityPayoutBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.android.ext.android.inject

class PayoutActivity : BaseActivity<AepsActivityPayoutBinding>() {
    override val viewModel: PayoutViewModel by inject()
    override val layoutRes = R.layout.aeps_activity_payout

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    private var otpAlertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // add radio button check change listener
        dataBinding.rgPayout.setOnCheckedChangeListener { group, checkedId ->
            // get radio button using id
            viewModel.payoutType.value = group.findViewById<RadioButton>(checkedId).text as String?
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBankInfoAndPayoutList()
    }

    override fun registerObserver() {
        viewModel.settingsErrorEvent.observe(this, Observer {
            errorAlert(getString(R.string.error_getting_settings_data)) {
                onBackPressed()
            }
        })

        viewModel.editBankDetailsClickEvent.observe(this, Observer {
            navigateTo(Routes.UPDATE_BANK_DETAILS_SCREEN)
        })

        viewModel.cancelClickEvent.observe(this, Observer {
            onBackPressed()
        })

        viewModel.otpRequestResultData.observe(this, Observer {
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    // show OTP verification dialog
                    otpAlertDialog =
                        otpAlert(it.data.message, it.data.otpPrefix) { isResendOtp, otpCode ->
                            when (isResendOtp) {
                                true -> {
                                    otpAlertDialog?.dismiss()
                                    // resend otp call
                                    showProgressDialog()
                                    viewModel.resendOtp(it.data.otpId)
                                }
                                false -> {
                                    // verify otp call
                                    showProgressDialog()
                                    viewModel.verifyOtp(it.data.otpId, otpCode)
                                }
                            }
                        }
                }
                is Result.Error -> {
                    otpAlertDialog?.dismiss()
                    errorAlert(it.error)
                }
            }
        })

        viewModel.payoutResultData.observe(this, Observer {
            otpAlertDialog?.dismiss()
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    when (it.data.isPending) {
                        true -> {
                            infoAlert(it.data.message, it.data.status) {
                                onBackPressed()
                            }
                        }
                        false -> {
                            navigateToSuccessErrorScreen(
                                Constants.StatusScreenType.SUCCESS_SCREEN,
                                PayoutSuccessErrorMapper().success(it.data)
                            ) {
                                finish()
                            }
                        }
                    }
                }
                is Result.Error -> {
                    navigateToSuccessErrorScreen(
                        Constants.StatusScreenType.ERROR_SCREEN,
                        PayoutSuccessErrorMapper().failure(it)
                    ) { retry ->
                        if (retry != true) {
                            finish()
                        }
                    }
                }
            }
        })
    }
}