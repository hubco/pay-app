package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.database.dao.SettingsDao
import com.gappscorp.aeps.library.data.mapper.StateListMapper
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.BiometricDevice
import com.gappscorp.aeps.library.data.model.Settings
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.SettingsService
import com.gappscorp.aeps.library.domain.repository.SettingsRepository

class SettingsRepositoryImpl(private val dao: SettingsDao, private val service: SettingsService) :
    SettingsRepository {

    override suspend fun getSettings(): Result<Settings> {
        // get settings data
        val result = safeCall("appSettings.json") { service.getSettings() }

        // save settings data in db
        when (result) {
            is Result.Success -> dao.saveSettings(result.data)
        }

        // return result
        return result
    }

    override suspend fun hasSettings(): Boolean {
        return !dao.getSettings().biometricDeviceList.isNullOrEmpty()
    }

    override suspend fun getBiometricDeviceList(): List<BiometricDevice> {
        return dao.getSettings().biometricDeviceList.map {
            BiometricDevice(it.key, it.value)
        }
    }

    override suspend fun getPayoutTypes(): List<String> {
        return dao.getSettings().payoutTypes
    }

    override suspend fun getDmtTypes(): List<String> {
        return dao.getSettings().dmtTypes
    }

    override suspend fun getBankList(): List<BankInfo> {
        return dao.getSettings().bankList
    }

    override suspend fun getAepsBankList(): List<BankInfo> {
        return dao.getSettings().aepsBankList
    }

    override suspend fun getAepsMsBankList(): List<BankInfo> {
        return dao.getSettings().aepsMiniStatementBankList
    }

    override suspend fun getStateList(): List<State> {
        return StateListMapper().map(dao.getSettings().states)
    }
}