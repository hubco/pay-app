package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.request.*
import com.gappscorp.aeps.library.domain.network.response.*
import com.gappscorp.aeps.library.domain.repository.base.IRepository

interface RetailerRepository : IRepository {
    suspend fun verifyNumber(phoneNumber: String):  Result<PhoneRemitterResponse>
    suspend fun retailRegister(request: RetailRegisterRequest): Result<PhoneRemitterResponse>
    suspend fun verifyOtp(otpCode: String, remitterId: String): Result<PhoneRemitterResponse>
    suspend fun verifyResendOtp(remitterId: String): Result<ResendOtpResponse>
    suspend fun addBeneficiary(addRequest: AddBeneficiaryRequest): Result<AddBeneficiaryResponse>
    suspend fun moneyTransfer(request: MoneyTransferRequest): Result<MoneyTransferResponse>
    suspend fun removeBeneficiary(request: RemoveBeneficiaryRequest): Result<GenericResponse>
    suspend fun verifyRemovedBeneficiary(verifyRemoveBeneficiaryRequest: VerifyRemoveBeneficiaryRequest): Result<GenericResponse>
    suspend fun verifyMoneyTransfer(verifyMoneyTransferRequest: VerifyMoneyTransferRequest): Result<MoneyTransferVerifyResponse>
    suspend fun resetMoneyTransferOtp(verifyMoneyTransferRequest: VerifyMoneyTransferRequest): Result<MoneyTransferResponse>
}