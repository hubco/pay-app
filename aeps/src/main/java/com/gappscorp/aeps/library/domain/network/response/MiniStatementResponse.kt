package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MiniStatementResponse(
    val status: String,
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    @SerializedName("bankrrn")
    val bankRrnNumber: String?,
    @SerializedName("balance")
    val availableBalance: String?,
    var aadhaarNumber: String?,
    var bankName: String?,
    @SerializedName("statement")
    val statementList: List<MiniStatement>?
) : Parcelable

@Parcelize
data class MiniStatement(
    @SerializedName("date")
    val statementDate: String,
    @SerializedName("narration")
    val naration: String,
    @SerializedName("txnType")
    val txnType: String,
    @SerializedName("amount")
    val amount: String
) : Parcelable