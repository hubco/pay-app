package com.gappscorp.aeps.library.ui.activity.state

import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.callback.StateItemCallback
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.databinding.AepsActivityStateListBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import com.gappscorp.aeps.library.ui.activity.state.adapter.StateListAdapter
import kotlinx.android.synthetic.main.aeps_activity_bank_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class StateListActivity : BaseActivity<AepsActivityStateListBinding>() {

    override val viewModel: StateListViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_state_list

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.callback = itemCallback
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        searchItem.expandActionView()
        searchView.setIconifiedByDefault(false)
        searchView.clearFocus()

        try {
            val searchEditText =
                searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            searchEditText.setTextColor(Color.WHITE)
            searchEditText.setHintTextColor(Color.parseColor("#f5f2d0"))
        } catch (ex: Exception) {
            Timber.d(ex)
        }

        searchView.queryHint = getString(R.string.hint_type_to_search)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchState(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchState(newText)
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    private fun searchState(query: String?) {
        if (rvBank.adapter is StateListAdapter) {
            (rvBank.adapter as StateListAdapter).searchState(query) { found ->
                emptySearchView.isVisible = !found
            }
        }
    }

    private val itemCallback = object : StateItemCallback {
        override fun onStateClicked(state: State) {
            setResult(state)
        }
    }
}