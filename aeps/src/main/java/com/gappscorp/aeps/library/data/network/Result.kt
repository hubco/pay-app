package com.gappscorp.aeps.library.data.network

sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val id: String? = null, val status: String? = null, val error: String? = null) : Result<Nothing>()
    data class Unauthorized(val error: String? = null) : Result<Nothing>()
}