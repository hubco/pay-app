package com.gappscorp.aeps.library.ui.activity.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StateListViewModel(private val repository: SettingsRepository) : ViewModel() {

    private val _stateList = MutableLiveData<List<State>>()
    val stateList: LiveData<List<State>>
        get() = _stateList

    init {
        viewModelScope.launch(Dispatchers.IO) {
            _stateList.postValue(repository.getStateList())
        }
    }
}