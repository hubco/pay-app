package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddBeneficiaryResponse(
    val status: String,
    @SerializedName("message")
    val message: String?,
    @SerializedName("beneficiary")
    val beneficiaryInfo: AddBeneficiaryInfo?
) : Parcelable

@Parcelize
data class AddBeneficiaryInfo(
    @SerializedName("id")
    val beneficiaryId: String?,
    @SerializedName("name")
    val beneficiaryName: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("ifsc_code")
    val ifscCode: String?,
    @SerializedName("account_no")
    val accountNumber: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("ref_id")
    val refId: String?,
    @SerializedName("b_type")
    val balanceType: String?,
    @SerializedName("is_verified")
    val isVerified: Boolean?,
    @SerializedName("is_otp_verified")
    val isOtpVerified: Boolean?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("bank_name")
    var bankName: String?,
    val isListingFlow: Boolean
) : Parcelable