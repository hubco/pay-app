package com.gappscorp.aeps.library.ui.activity.login

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.authentication.Biometric
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.extensions.toast
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateToDashboard
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityLoginBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import kotlinx.android.synthetic.main.aeps_activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class LoginActivity : BaseActivity<AepsActivityLoginBinding>() {

    override val viewModel: LoginViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // show quick login if enabled
        btQuickLogin.isVisible = sessionManager.isBiometricLoginEnabled()

        // update view visibility as per gradle configuration
        llBottom.isVisible = CoreApp.configuration.hasPoweredByInfo
        btnBack.isVisible = CoreApp.configuration.backButtonEnabled
    }

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.quickLoginClickEvent.observe(this, Observer {
            Biometric.authenticate(this) { success, error ->
                if (success) {
                    viewModel.authenticateQuickLogin()
                } else {
                    toast(error ?: getString(R.string.biometric_authentication_error))
                }
            }
        })

        viewModel.backButtonClickEvent.observe(this, Observer {
            onBackPressed()
        })

        viewModel.forgotClickEvent.observe(this, Observer {
            navigateTo(Routes.FORGET_PASSWORD_SCREEN)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    // save user data
                    sessionManager.saveUser(it.data)
                    // navigate to dashboard
                    navigateToDashboard()
                }
                is Result.Error -> errorAlert(it.error)
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        (application as CoreApp).onLeavingAeps(isTaskRoot)
    }
}