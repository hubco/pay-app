package com.gappscorp.aeps.library.ui.fragment.retailer.beneficiary

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.RemoveBeneficiaryRequest
import com.gappscorp.aeps.library.domain.network.request.VerifyRemoveBeneficiaryRequest
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class RemoveBeneficiaryOtpViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val beneficiaryInfo = MutableLiveData<BeneficiaryInfo>()
    val indexPosition = MutableLiveData<Int>()
    val otpCode = MutableLiveData<String>()
    val remitterId = MutableLiveData<String>()
    val responseMsg = MutableLiveData<String>()

    val isTimerEnabled = MutableLiveData<Boolean>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _verifyStatusData = MutableLiveData<Status>()
    val verifyStatusData: LiveData<Status>
        get() = _verifyStatusData

    private val _verifyRemovedBeneficiaryData = MutableLiveData<Result<GenericResponse>>()
    val verifyRemoveBeneficiaryData: LiveData<Result<GenericResponse>>
        get() = _verifyRemovedBeneficiaryData

    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(otpCode) {
            value = !otpCode.value.isNullOrEmpty()
        }
    }

    private val _resendResultData = MutableLiveData<Result<GenericResponse>>()
    val resendResultData: LiveData<Result<GenericResponse>>
        get() = _resendResultData

    fun onSubmitButtonClicked()  {
        viewModelScope.launch {
            _verifyStatusData.postValue(Status.LOADING)
            val result = retailerRepository.verifyRemovedBeneficiary(
                VerifyRemoveBeneficiaryRequest(
                    beneficiaryId = beneficiaryInfo.value?.slug,
                    remitterId = remitterId.value,
                    otp = otpCode.value
                )
            )
            _verifyRemovedBeneficiaryData.postValue(result)
            when (result) {
                is Result.Success -> _verifyStatusData.postValue(Status.SUCCESS)
                is Result.Error -> _verifyStatusData.postValue(Status.ERROR)
            }
        }
    }

    fun onResendOtpClick(){
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.removeBeneficiary(
                RemoveBeneficiaryRequest(
                    beneficiaryId = beneficiaryInfo.value?.slug,
                    remitterId = remitterId.value
                )
            )
            _resendResultData.postValue(result)
            when (result) {
                is Result.Success ->{
                    otpCode.value  = ""
                    _statusData.postValue(Status.SUCCESS)
                }
                is Result.Error ->{
                    _statusData.postValue(Status.ERROR)
                }
            }
        }
    }

    val closeClickEvent = SingleLiveEvent<Void>()

    fun closeDialog(){
        closeClickEvent.call()
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(otpCode)
    }
}