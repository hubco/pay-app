package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.database.dao.UserDao
import com.gappscorp.aeps.library.data.model.RetailerInfo
import com.gappscorp.aeps.library.data.model.User
import com.gappscorp.aeps.library.data.model.UserDetail
import com.gappscorp.aeps.library.data.model.UserInfo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.UserService
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.repository.UserRepository
import com.gappscorp.aeps.library.data.model.KYCResponse

class UserRepositoryImpl(private val dao: UserDao, private val service: UserService) :
    UserRepository {

    override suspend fun login(userName: String, password: String): Result<User> {
        return safeCall("userLogin.json") { service.login(userName, password) }
    }

    override suspend fun quickLogin(): Result<User> {
        return safeCall("userLogin.json") { service.quickLogin() }
    }

    override suspend fun forgetPassword(userName: String): Result<OtpVerificationResponse> {
        return safeCall("forgetPassword.json") { service.forgetPassword(userName) }
    }

    override suspend fun forgetUsername(mobileNumber: String): Result<OtpVerificationResponse> {
        return safeCall { service.forgetUsername(mobileNumber) }
    }

    override suspend fun verifyOtp(
        userName: String,
        otpId: String,
        otpCode: String
    ): Result<GenericResponse> {
        return safeCall("verifyOtp.json") { service.verifyOtp(userName, otpId, otpCode) }
    }

    override suspend fun changePassword(
        oldPassword: String,
        newPassword: String
    ): Result<GenericResponse> {
        return safeCall { service.changePassword(oldPassword, newPassword) }
    }

    override suspend fun userDetails(): Result<UserDetail> {
        val result = safeCall("userInfo.json") { service.userDetails() }
        // save user details in db
        when(result) {
            is Result.Success -> dao.saveUserDetails(result.data)
        }
        return result
    }

    override suspend fun sendAadhaarOtp(aadhaarNo: String?): Result<KYCResponse> {
        return safeCall { service.sendAdharOtp(aadhaarNo!!) }
    }

    override suspend fun submitAadhaarOtp(tId: String, otp: String): Result<KYCResponse> {
        return safeCall { service.submitAadhaarOtp(tId,otp) }
    }

    override suspend fun resendAadhaarOtp(tId: String): Result<KYCResponse> {
        return safeCall { service.resendAadhaarOtp(tId) }
    }

    override suspend fun submitBiometric(
        tId: String,
        deviceType: String,
        txtPidData: String
    ): Result<KYCResponse> {
        return safeCall { service.submitBiometric(tId,deviceType,txtPidData) }
    }

    override suspend fun getUserInfo(): UserInfo? {
        return dao.getUserInfo()
    }

    override suspend fun getRetailerInfo(): RetailerInfo? {
        return dao.getRetailerInfo()
    }
}