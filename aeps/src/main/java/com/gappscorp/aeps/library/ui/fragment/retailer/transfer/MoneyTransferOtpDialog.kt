package com.gappscorp.aeps.library.ui.fragment.retailer.transfer

import `in`.aabhasjindal.otptextview.OTPListener
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.startTimer
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsDialogMoneyTransferOtpBinding
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferVerifyResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.fragment.base.BaseDialogFragment
import kotlinx.android.synthetic.main.aeps_dialog_register_otp_screen.otpView
import kotlinx.android.synthetic.main.aeps_dialog_remove_beneficiary_otp.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoneyTransferOtpDialog : BaseDialogFragment<AepsDialogMoneyTransferOtpBinding>() {

    override val viewModel: MoneyTransferOtpViewModel by viewModel()

    override val layoutRes = R.layout.aeps_dialog_money_transfer_otp
    lateinit var dialogFinished: IDialogFinished

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.beneficiaryId.value = arguments?.getString(Constants.EXTRA_BENEFICIARY_ID)
        viewModel.moneyTransferResponse.value =
            arguments?.getParcelable(Constants.EXTRA_MONEY_TRANSFER_RESPONSE)
        viewModel.remitterId.value = arguments?.getString(Constants.EXTRA_REMITTER_ID)
        viewModel.modeType.value = arguments?.getString(Constants.EXTRA_MODE_TYPE)
        viewModel.remitterResponse.value =
            arguments?.getParcelable(Constants.EXTRA_REMITTER_RESPONSE)
        viewModel.beneficiaryInfo.value = arguments?.getParcelable(Constants.EXTRA_BENEFICIARY_INFO)
    }

    fun setDialogListener(listener: IDialogFinished) {
        dialogFinished = listener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register otp event listener
        otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                viewModel.otpCode.value = ""
            }

            override fun onOTPComplete(otp: String) {
                viewModel.otpCode.value = otp
            }
        }
        startCountTimer()
    }


    override fun registerObserver() {
        viewModel.verifyTransferData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    dialogFinished.onDialogFinished(
                        viewModel.remitterResponse.value,
                        it.data,
                        viewModel.beneficiaryInfo.value,
                        viewModel.modeType.value
                    )
                    dismiss()
                }
                is Result.Error -> {
                    if(it.status?.equals("failed", ignoreCase = true) == true) {
                        dismiss()
                    }
                    context?.errorAlert(it.error)
                }
            }
        })

        viewModel.resendResultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    otpView.setOTP("")
                    startCountTimer()
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })

        viewModel.closeClickEvent.observe(this, Observer {
            dismiss()
        })
    }

    private fun startCountTimer() {
        startTimer { timerResult, isFinished ->
            tvCountTime?.text = timerResult
            viewModel.isTimerEnabled.value = isFinished
        }
    }

    companion object {
        val TAG = MoneyTransferOtpDialog::class.java.simpleName
        fun newInstance(
            beneficiaryId: String,
            remitterId: String,
            moneyTransferResponse: MoneyTransferResponse,
            remitterResponse: PhoneRemitterResponse,
            beneficiaryInfo: AddBeneficiaryResponse,
            modeType: String
        ): MoneyTransferOtpDialog =
            MoneyTransferOtpDialog().apply {
                arguments = Bundle().apply {
                    putString(Constants.EXTRA_BENEFICIARY_ID, beneficiaryId)
                    putString(Constants.EXTRA_REMITTER_ID, remitterId)
                    putString(Constants.EXTRA_MODE_TYPE, modeType)
                    putParcelable(Constants.EXTRA_MONEY_TRANSFER_RESPONSE, moneyTransferResponse)
                    putParcelable(Constants.EXTRA_REMITTER_RESPONSE, remitterResponse)
                    putParcelable(Constants.EXTRA_BENEFICIARY_INFO, beneficiaryInfo)
                }
            }
    }

    interface IDialogFinished {
        fun onDialogFinished(
            value: PhoneRemitterResponse?,
            data: MoneyTransferVerifyResponse,
            beneficiary: AddBeneficiaryResponse?,
            modeType: String?
        )
    }


}