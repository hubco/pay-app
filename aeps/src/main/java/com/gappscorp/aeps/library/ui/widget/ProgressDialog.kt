package com.gappscorp.aeps.library.ui.widget

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import com.gappscorp.aeps.library.R

object ProgressDialog {

    fun show(
        context: Context, cancelable: Boolean = true
    ): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.aeps_layout_progress_bar, null)
        return Dialog(context, R.style.FullScreenProgressDialog).apply {
            setContentView(view)
            setCancelable(cancelable)
            show()
        }
    }

    fun create(
        context: Context, cancelable: Boolean = true
    ): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.aeps_layout_progress_bar, null)
        return Dialog(context, R.style.FullScreenProgressDialog).apply {
            setContentView(view)
            setCancelable(cancelable)
        }
    }
}