package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.network.response.PayoutResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PayoutService {

    @POST("api/retailer-api/v1/payouts/transfer")
    @FormUrlEncoded
    suspend fun transfer(
        @Field("t_mode") transferMode: String?,
        @Field("amount") amount: String?,
        @Field("remarks") remarks: String?
    ): Response<OtpVerificationResponse>

    @POST("api/retailer-api/v1/payouts/resend-otp")
    @FormUrlEncoded
    suspend fun resendOtp(@Field("t_id") otpId: String): Response<OtpVerificationResponse>

    @POST("api/retailer-api/v1/payouts/verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(
        @Field("t_id") otpId: String,
        @Field("otp") otpCode: String?
    ): Response<PayoutResponse>
}