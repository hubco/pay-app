package com.gappscorp.aeps.library.di

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.gappscorp.aeps.library.BuildConfig
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.data.database.AepsDatabase
import com.gappscorp.aeps.library.ui.activity.adhar.AdharViewModel
import com.gappscorp.aeps.library.ui.activity.aeps.banklist.BankListViewModel
import com.gappscorp.aeps.library.ui.activity.aeps.enquiry.BalanceEnquiryViewModel
import com.gappscorp.aeps.library.ui.activity.aeps.statement.MiniStatementViewModel
import com.gappscorp.aeps.library.ui.activity.aeps.statement.list.MiniStatementListViewModel
import com.gappscorp.aeps.library.ui.activity.aeps.withdrawal.CashWithdrawalViewModel
import com.gappscorp.aeps.library.ui.activity.bankdetails.UpdateBankDetailsViewModel
import com.gappscorp.aeps.library.ui.activity.dashboard.MainViewModel
import com.gappscorp.aeps.library.ui.activity.forgot.ForgetPasswordViewModel
import com.gappscorp.aeps.library.ui.activity.forgot.ForgetUsernameViewModel
import com.gappscorp.aeps.library.ui.activity.login.LoginViewModel
import com.gappscorp.aeps.library.ui.activity.otp.OtpVerificationViewModel
import com.gappscorp.aeps.library.ui.activity.password.ChangePasswordViewModel
import com.gappscorp.aeps.library.ui.activity.payout.PayoutViewModel
import com.gappscorp.aeps.library.ui.activity.profile.UserProfileViewModel
import com.gappscorp.aeps.library.ui.activity.result.SuccessErrorViewModel
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.RetailBeneficiaryDetailViewModel
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.add.AddBeneficiaryViewModel
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list.BeneficiaryListViewModel
import com.gappscorp.aeps.library.ui.activity.retailer.register.RetailRegisterViewModel
import com.gappscorp.aeps.library.ui.activity.retailer.transfer.MoneyTransferDetailViewModel
import com.gappscorp.aeps.library.ui.activity.state.StateListViewModel
import com.gappscorp.aeps.library.ui.fragment.retailer.PhoneVerificationViewModel
import com.gappscorp.aeps.library.ui.fragment.retailer.beneficiary.RemoveBeneficiaryOtpViewModel
import com.gappscorp.aeps.library.ui.fragment.retailer.otp.RegisterOtpVerificationViewModel
import com.gappscorp.aeps.library.ui.fragment.retailer.success.MoneyTransferSuccessViewModel
import com.gappscorp.aeps.library.ui.fragment.retailer.transfer.MoneyTransferOtpViewModel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val koinModules: List<Module>
    get() = listOf(
        sessionModule,
        databaseModule,
        daoModule,
        repositoriesModule,
        viewModelsModule,
        retrofitModule,
        servicesModule
    )

val sessionModule = module {
    single { SessionManager(CoreApp.appContext) }
}

val databaseModule = module {
    single { provideDbModule(CoreApp.appContext) }
}

val daoModule = module {
    factory { provideSettingsDao(get()) }
    factory { provideUserDao(get()) }
}

val repositoriesModule = module {
    factory { provideUserRepository(get(), get()) }
    factory { provideAepsRepository(get()) }
    factory { provideSettingsRepository(get(), get()) }
    factory { providePayoutRepository(get()) }
    factory { provideBankRepository(get()) }
    factory { provideRetailerRepository(get()) }
}

val viewModelsModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { AdharViewModel(get() ,get()) }
    viewModel { ForgetPasswordViewModel(get()) }
    viewModel { ForgetUsernameViewModel(get()) }
    viewModel { OtpVerificationViewModel(get()) }
    viewModel { CashWithdrawalViewModel(get(), get()) }
    viewModel { BalanceEnquiryViewModel(get(), get()) }
    viewModel { MiniStatementViewModel(get(), get()) }
    viewModel { BankListViewModel(get()) }
    viewModel { MiniStatementListViewModel() }
    viewModel { SuccessErrorViewModel() }
    viewModel { PayoutViewModel(get(), get(), get()) }
    viewModel { UserProfileViewModel(get()) }
    viewModel { UpdateBankDetailsViewModel(get(), get()) }
    viewModel { ChangePasswordViewModel(get()) }
    viewModel { PhoneVerificationViewModel(get()) }
    viewModel { RetailRegisterViewModel(get()) }
    viewModel { RegisterOtpVerificationViewModel(get()) }
    viewModel { RetailBeneficiaryDetailViewModel(get()) }
    viewModel { AddBeneficiaryViewModel(get()) }
    viewModel { BeneficiaryListViewModel(get()) }
    viewModel { MoneyTransferDetailViewModel(get(),get()) }
    viewModel { RemoveBeneficiaryOtpViewModel(get()) }
    viewModel { MoneyTransferOtpViewModel(get()) }
    viewModel { MoneyTransferSuccessViewModel() }
    viewModel { StateListViewModel(get()) }
}

val retrofitModule = module {
    factory { provideOkHttpClientBuilder(providesHttpLoggingInterceptor()) }
    factory { provideRetrofit(get(), get()) }
}

val servicesModule = module {
    factory { provideUserService(get()) }
    factory { provideAepsService(get()) }
    factory { provideSettingsService(get()) }
    factory { providePayoutService(get()) }
    factory { provideBankService(get()) }
    factory { provideRetailerService(get()) }
}

fun provideDbModule(context: Context): AepsDatabase {
    return Room.databaseBuilder(
        context,
        AepsDatabase::class.java, CoreApp.configuration.dbName
    ).fallbackToDestructiveMigration().build()
}

fun provideRetrofit(
    okHttpClientBuilder: OkHttpClient.Builder,
    sessionManager: SessionManager
): Retrofit {
    if (sessionManager.getUserId().isNotEmpty() && sessionManager.getUserToken().isNotEmpty()) {
        okHttpClientBuilder.addInterceptor { chain ->
            val request: Request = chain.request().newBuilder()
                .addHeader("User-Id", sessionManager.getUserId())
                .addHeader("Authorization", "Bearer ${sessionManager.getUserToken()}")
                .build()
            chain.proceed(request)
        }
        Log.d("TAG", "provideRetrofit: ${sessionManager.getUserId()}")
    }
    return Retrofit.Builder().baseUrl(CoreApp.configuration.baseUrl)
        .client(okHttpClientBuilder.build())
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClientBuilder(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient.Builder {
    return OkHttpClient().newBuilder()
        .writeTimeout(1, TimeUnit.MINUTES)
        .readTimeout(1, TimeUnit.MINUTES)
        .callTimeout(1, TimeUnit.MINUTES)
        .addInterceptor(loggingInterceptor)
}

fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
    return if (BuildConfig.DEBUG)
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    else
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.NONE
        }
}