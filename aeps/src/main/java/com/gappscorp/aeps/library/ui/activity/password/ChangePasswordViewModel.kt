package com.gappscorp.aeps.library.ui.activity.password

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.launch

class ChangePasswordViewModel(private val repository: UserRepository) : ViewModel() {

    val oldPassword = MutableLiveData<String>()
    val newPassword = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()

    val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    val _resultData = MutableLiveData<Result<GenericResponse>>()
    val resultData: LiveData<Result<GenericResponse>>
        get() = _resultData

    val isButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(oldPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
        addSource(newPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
        addSource(confirmPassword) {
            value = !oldPassword.value.isNullOrEmpty()
                    && !newPassword.value.isNullOrEmpty()
                    && !confirmPassword.value.isNullOrEmpty()
        }
    }

    val passwordLengthFailureEvent = SingleLiveEvent<Void>()
    val passwordMismatchEvent = SingleLiveEvent<Void>()

    fun onChangePasswordClicked() {
        if (newPassword.value!!.length < 6) {
            passwordLengthFailureEvent.call()
            return
        }
        if (newPassword.value != confirmPassword.value) {
            passwordMismatchEvent.call()
            return
        }

        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.changePassword(oldPassword.value!!, newPassword.value!!)
            _resultData.postValue(result)
            when(result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        isButtonEnabled.removeSource(oldPassword);
        isButtonEnabled.removeSource(newPassword);
        isButtonEnabled.removeSource(confirmPassword);
    }
}