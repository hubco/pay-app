package com.gappscorp.aeps.library.data.model

class KYCResponse(
    val status: String,
    val message: String,
    val t_id: String
)