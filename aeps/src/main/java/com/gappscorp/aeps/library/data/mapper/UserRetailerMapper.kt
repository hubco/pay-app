package com.gappscorp.aeps.library.data.mapper

import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.data.model.RetailerInfo
import com.gappscorp.aeps.library.data.model.UserInfo
import com.gappscorp.aeps.library.data.model.UserProfileItem

class UserRetailerMapper {

    val context = CoreApp.appContext
    val notApplicable get() = context.getString(R.string.not_applicable)

    fun map(entity: RetailerInfo?): List<UserProfileItem> {
        return  mutableListOf<UserProfileItem>().apply {
            add(UserProfileItem(context.getString(R.string.label_retailer_code),entity?.retailerCode ?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_name),entity?.name?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_father_name),entity?.fatherName?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_spouse),entity?.spouseName?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_gender),entity?.gender?: notApplicable))
            add(UserProfileItem(context.getString(R.string.aadhaar_number_txt),entity?.aadhaarNo?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_pan_number),entity?.panNo?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_dob),entity?.dob?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_age),entity?.age?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_email),entity?.email?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_mobile_number),entity?.mobileNo?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_address),entity?.address?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_state),entity?.state?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_pin_code),entity?.pincode?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_enrollment_date),entity?.enrollmentDate?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_kyc),entity?.kycStatus?: notApplicable))
        }
    }

    fun map(entity: UserInfo?): List<UserProfileItem> {
        return  mutableListOf<UserProfileItem>().apply {
            add(UserProfileItem(context.getString(R.string.label_username),entity?.username?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_designation),entity?.designation?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_email),entity?.email?: notApplicable))
            add(UserProfileItem(context.getString(R.string.label_mobile_number),entity?.mobile_no?: notApplicable))
        }
    }
}