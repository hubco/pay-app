package com.gappscorp.aeps.library.ui.fragment.retailer.success

import android.os.Bundle
import androidx.core.view.drawToBitmap
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.share.Share
import com.gappscorp.aeps.library.databinding.AepsDialogMoneyTransferSuccessBinding
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferVerifyResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.fragment.base.BaseDialogFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoneyTransferSuccessDialog : BaseDialogFragment<AepsDialogMoneyTransferSuccessBinding>() {

    override val viewModel: MoneyTransferSuccessViewModel by viewModel()

    override val layoutRes = R.layout.aeps_dialog_money_transfer_success
    lateinit var dialogFinished: ISuccessDialogFinished

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = arguments?.getParcelable<MoneyTransferVerifyResponse>(Constants.EXTRA_VERIFY_SUCCESS_DATA)
        viewModel.moneyTransferResponse.value = data
        viewModel.status.value = data?.status
        viewModel.remitterResponse.value = arguments?.getParcelable(Constants.EXTRA_REMITTER_RESPONSE)
        viewModel.beneficiaryInfo.value = arguments?.getParcelable(Constants.EXTRA_BENEFICIARY_INFO)
        viewModel.modeType.value = arguments?.getString(Constants.EXTRA_MODE_TYPE)

        isCancelable = false
    }

    fun setDialogListener(listener: ISuccessDialogFinished) {
        dialogFinished = listener
    }

    override fun registerObserver() {

        viewModel.closeClickEvent.observe(this, Observer {
            dialogFinished.onDialogFinished()
            dismiss()
        })

        viewModel.shareClickEvent.observe(this, Observer {
            val bitmap = dataBinding.root.drawToBitmap()
            Share.shareImage(dataBinding.root.context, bitmap)
        })
    }

    companion object {
        val TAG = MoneyTransferSuccessDialog::class.java.simpleName
        fun newInstance(
            remitterResponse: PhoneRemitterResponse?,
            verifyResponse: MoneyTransferVerifyResponse,
            beneficiary: AddBeneficiaryResponse?,
            modeType: String?
        ): MoneyTransferSuccessDialog =
            MoneyTransferSuccessDialog().apply {
                arguments = Bundle().apply {
                    putParcelable(Constants.EXTRA_REMITTER_RESPONSE, remitterResponse)
                    putParcelable(Constants.EXTRA_VERIFY_SUCCESS_DATA, verifyResponse)
                    putParcelable(Constants.EXTRA_BENEFICIARY_INFO, beneficiary)
                    putString(Constants.EXTRA_MODE_TYPE,modeType)
                }
            }
    }

    interface ISuccessDialogFinished {
        fun onDialogFinished()
    }


}