package com.gappscorp.aeps.library.ui.activity.aeps.banklist

import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.callback.BankItemCallback
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.databinding.AepsActivityBankListBinding
import com.gappscorp.aeps.library.ui.activity.aeps.banklist.adapter.BankListAdapter
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import kotlinx.android.synthetic.main.aeps_activity_bank_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class BankListActivity : BaseActivity<AepsActivityBankListBinding>() {

    override val viewModel: BankListViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_bank_list

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.callback = itemCallback

        // get request type
        val typeIndex = intent.getIntExtra(Constants.EXTRA_BANK_INFO_TYPE, 0)
        val bankInfoType = BankListType.values()[typeIndex]

        // load bank list based on type specified
        viewModel.getBankList(bankInfoType)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        searchItem.expandActionView()
        searchView.setIconifiedByDefault(false)
        searchView.clearFocus()

        try {
            val searchEditText =
                searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            searchEditText.setTextColor(Color.WHITE)
            searchEditText.setHintTextColor(Color.parseColor("#f5f2d0"))
        } catch (ex: Exception) {
            Timber.d(ex)
        }

        searchView.queryHint = getString(R.string.hint_type_to_search)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchBank(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchBank(newText)
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    private fun searchBank(query: String?) {
        if (rvBank.adapter is BankListAdapter) {
            (rvBank.adapter as BankListAdapter).searchBank(query) { found ->
                emptySearchView.isVisible = !found
            }
        }
    }

    private val itemCallback = object : BankItemCallback {
        override fun onBankItemClicked(item: BankInfo) {
            setResult(item)
        }
    }
}