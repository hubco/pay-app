package com.gappscorp.aeps.library.common.navigation

import com.gappscorp.aeps.library.ui.activity.adhar.KYCAdharNumberActivity
import com.gappscorp.aeps.library.ui.activity.aeps.banklist.BankListActivity
import com.gappscorp.aeps.library.ui.activity.aeps.deposit.CashDepositActivity
import com.gappscorp.aeps.library.ui.activity.aeps.enquiry.BalanceEnquiryActivity
import com.gappscorp.aeps.library.ui.activity.aeps.statement.MiniStatementActivity
import com.gappscorp.aeps.library.ui.activity.aeps.statement.list.MiniStatementListActivity
import com.gappscorp.aeps.library.ui.activity.aeps.withdrawal.CashWithdrawalActivity
import com.gappscorp.aeps.library.ui.activity.bankdetails.UpdateBankDetailsActivity
import com.gappscorp.aeps.library.ui.activity.dashboard.MainActivity
import com.gappscorp.aeps.library.ui.activity.forgot.ForgetPasswordActivity
import com.gappscorp.aeps.library.ui.activity.forgot.ForgetUsernameActivity
import com.gappscorp.aeps.library.ui.activity.login.LoginActivity
import com.gappscorp.aeps.library.ui.activity.otp.OtpVerificationActivity
import com.gappscorp.aeps.library.ui.activity.password.ChangePasswordActivity
import com.gappscorp.aeps.library.ui.activity.payout.PayoutActivity
import com.gappscorp.aeps.library.ui.activity.profile.UserProfileActivity
import com.gappscorp.aeps.library.ui.activity.result.SuccessErrorActivity
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.RetailBeneficiaryDetailActivity
import com.gappscorp.aeps.library.ui.activity.retailer.register.RetailRegisterActivity
import com.gappscorp.aeps.library.ui.activity.retailer.transfer.MoneyTransferDetailActivity
import com.gappscorp.aeps.library.ui.activity.state.StateListActivity

object Routes {

    const val LOGIN_SCREEN = "LOGIN_SCREEN"
    const val FORGET_PASSWORD_SCREEN = "FORGET_PASSWORD_SCREEN"
    const val FORGET_USER_NAME_SCREEN = "FORGET_USER_NAME_SCREEN"
    const val OTP_VERIFICATION_SCREEN = "OTP_VERIFICATION_SCREEN"
    const val DASHBOARD_SCREEN = "DASHBOARD_SCREEN"
    const val PAYOUT_SCREEN = "PAYOUT_SCREEN"
    const val CASH_DEPOSIT_SCREEN = "CASH_DEPOSIT_SCREEN"
    const val CASH_WITHDRAWAL_SCREEN = "CASH_WITHDRAWAL_SCREEN"
    const val BALANCE_ENQUIRY_SCREEN = "BALANCE_ENQUIRY_SCREEN"
    const val MINI_STATEMENT_SCREEN = "MINI_STATEMENT_SCREEN"
    const val SELECT_BANK_SCREEN = "SELECT_BANK_SCREEN"
    const val MINI_STATEMENT_LIST_SCREEN = "MINI_STATEMENT_LIST_SCREEN"
    const val SUCCESS_ERROR_SCREEN = "SUCCESS_ERROR_SCREEN"
    const val UPDATE_BANK_DETAILS_SCREEN = "UPDATE_BANK_DETAILS_SCREEN"
    const val USER_PROFILE_SCREEN = "USER_PROFILE_SCREEN"
    const val KYC_SCREEN = "KYC_SCREEN"
    const val CHANGE_PASSWORD_SCREEN = "CHANGE_PASSWORD_SCREEN"
    const val RETAIL_REGISTER_SCREEN = "RETAIL_REGISTER_SCREEN"
    const val BENEFICIARY_DETAIL_SCREEN = "BENEFICIARY_DETAIL_SCREEN"
    const val MONEY_TRANSFER_SCREEN = "MONEY_TRANSFER_SCREEN"
    const val STATE_LIST_SCREEN = "STATE_LIST_SCREEN"

    val navigationMap: Map<String, Class<*>> = HashMap<String, Class<*>>().apply {
        put(LOGIN_SCREEN, LoginActivity::class.java)
        put(FORGET_PASSWORD_SCREEN, ForgetPasswordActivity::class.java)
        put(FORGET_USER_NAME_SCREEN, ForgetUsernameActivity::class.java)
        put(OTP_VERIFICATION_SCREEN, OtpVerificationActivity::class.java)
        put(DASHBOARD_SCREEN, MainActivity::class.java)
        put(PAYOUT_SCREEN, PayoutActivity::class.java)
        put(CASH_DEPOSIT_SCREEN, CashDepositActivity::class.java)
        put(CASH_WITHDRAWAL_SCREEN, CashWithdrawalActivity::class.java)
        put(BALANCE_ENQUIRY_SCREEN, BalanceEnquiryActivity::class.java)
        put(MINI_STATEMENT_SCREEN, MiniStatementActivity::class.java)
        put(SELECT_BANK_SCREEN, BankListActivity::class.java)
        put(MINI_STATEMENT_LIST_SCREEN, MiniStatementListActivity::class.java)
        put(SUCCESS_ERROR_SCREEN, SuccessErrorActivity::class.java)
        put(UPDATE_BANK_DETAILS_SCREEN, UpdateBankDetailsActivity::class.java)
        put(USER_PROFILE_SCREEN, UserProfileActivity::class.java)
        put(CHANGE_PASSWORD_SCREEN, ChangePasswordActivity::class.java)
        put(RETAIL_REGISTER_SCREEN, RetailRegisterActivity::class.java)
        put(BENEFICIARY_DETAIL_SCREEN, RetailBeneficiaryDetailActivity::class.java)
        put(MONEY_TRANSFER_SCREEN, MoneyTransferDetailActivity::class.java)
        put(STATE_LIST_SCREEN, StateListActivity::class.java)
        put(KYC_SCREEN,KYCAdharNumberActivity::class.java)
    }
}