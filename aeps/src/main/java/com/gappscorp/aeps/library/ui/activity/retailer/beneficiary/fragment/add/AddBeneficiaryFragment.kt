package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.add

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.allowAlphanumericOnly
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.navigateToBankListScreen
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.data.model.BeneficiaryData
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsFragmentAddBeneficiaryBinding
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.base.BaseBeneficiaryFragment
import kotlinx.android.synthetic.main.aeps_fragment_add_beneficiary.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class AddBeneficiaryFragment :
    BaseBeneficiaryFragment<AddBeneficiaryViewModel, AepsFragmentAddBeneficiaryBinding>() {

    override val viewModel: AddBeneficiaryViewModel by viewModel()

    override val layoutRes = R.layout.aeps_fragment_add_beneficiary

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.remitterResponse.value =
            arguments?.getParcelable(Constants.EXTRA_REMITTER_RESPONSE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edtIfscCode.allowAlphanumericOnly()
    }

    override fun registerObserver() {
        viewModel.selectBankClickEvent.observe(this, Observer {
            requireActivity().navigateToBankListScreen(BankListType.AEPS_MINI_STATEMENT_BANK_LIST) { _, data ->
                viewModel.bankName.value = data?.bankName
                viewModel.bankCode.value = data?.bankCode
            }
        })

        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> activity?.hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    requireActivity().setResult(
                        BeneficiaryData(
                            addBeneficiaryResponse = it.data,
                            phoneRemitterResponse = viewModel.remitterResponse.value
                        )
                    )
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })
    }

    companion object {
        val TAG = AddBeneficiaryFragment::class.java.simpleName
        fun newInstance(remitterResponse: PhoneRemitterResponse?): AddBeneficiaryFragment =
            AddBeneficiaryFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(Constants.EXTRA_REMITTER_RESPONSE, remitterResponse)
                }
            }
    }

    override fun getPhoneRemitterResponse(): PhoneRemitterResponse? {
        return viewModel.remitterResponse.value
    }
}