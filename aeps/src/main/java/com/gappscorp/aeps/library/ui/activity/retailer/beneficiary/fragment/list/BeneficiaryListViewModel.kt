package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.RemoveBeneficiaryRequest
import com.gappscorp.aeps.library.domain.network.response.*
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class BeneficiaryListViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()
    val beneficiaryInfo = MutableLiveData<BeneficiaryInfo>()
    val currentPosition = MutableLiveData<Int>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<GenericResponse>>()
    val resultData: LiveData<Result<GenericResponse>>
        get() = _resultData

    val isDataAvailable = MutableLiveData<Boolean>(false)

    fun removeBeneficiary(item: BeneficiaryInfo, position: Int) {
        beneficiaryInfo.value = item
        currentPosition.value = position
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.removeBeneficiary(
                RemoveBeneficiaryRequest(
                    beneficiaryId = item.slug!!,
                    remitterId = remitterResponse.value?.remitterInfo?.slug!!
                )
            )
            when (result) {
                is Result.Success -> {
                    _statusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> {
                    _statusData.postValue(Status.ERROR)
                }
            }
            _resultData.postValue(result)
        }
    }

    fun mappedBeneficiaryData(item: BeneficiaryInfo): AddBeneficiaryResponse {
        return AddBeneficiaryResponse(
            beneficiaryInfo = AddBeneficiaryInfo(
                beneficiaryId = item.beneficiaryId,
                remitterId = item.remitterId,
                beneficiaryName = item.beneficiaryName,
                accountNumber = item.accountNumber,
                ifscCode = item.ifscCode,
                bankCode = item.bankCode,
                mobileNumber = item.mobileNumber,
                bankName = item.bankName,
                balanceType = item.balanceType,
                createdAt = item.createdAt,
                isOtpVerified = item.isOtpVerified,
                isVerified = item.isVerified,
                refId = item.refId,
                slug = item.slug,
                updatedAt = item.updatedAt,
                isListingFlow = true
            ), message = "", status = "Success"
        )
    }
}