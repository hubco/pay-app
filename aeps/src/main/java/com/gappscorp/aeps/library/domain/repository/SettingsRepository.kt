package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.BiometricDevice
import com.gappscorp.aeps.library.data.model.Settings
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.repository.base.IRepository

interface SettingsRepository : IRepository {

    suspend fun getSettings() : Result<Settings>

    suspend fun hasSettings() : Boolean

    suspend fun getBiometricDeviceList() : List<BiometricDevice>

    suspend fun getPayoutTypes() : List<String>

    suspend fun getDmtTypes() : List<String>

    suspend fun getBankList() : List<BankInfo>

    suspend fun getAepsBankList() : List<BankInfo>

    suspend fun getAepsMsBankList() : List<BankInfo>

    suspend fun getStateList() : List<State>
}