package com.gappscorp.aeps.library.ui.fragment.retailer.beneficiary

import `in`.aabhasjindal.otptextview.OTPListener
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.startTimer
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsDialogRemoveBeneficiaryOtpBinding
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo
import com.gappscorp.aeps.library.ui.fragment.base.BaseDialogFragment
import kotlinx.android.synthetic.main.aeps_dialog_register_otp_screen.otpView
import kotlinx.android.synthetic.main.aeps_dialog_remove_beneficiary_otp.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RemoveBeneficiaryOtpDialog : BaseDialogFragment<AepsDialogRemoveBeneficiaryOtpBinding>() {

    override val viewModel: RemoveBeneficiaryOtpViewModel by viewModel()

    override val layoutRes = R.layout.aeps_dialog_remove_beneficiary_otp
    lateinit var dialogFinished: IDialogFinished

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.beneficiaryInfo.value =
            arguments?.getParcelable(Constants.EXTRA_BENEFICIARY_INFO)
        viewModel.indexPosition.value = arguments?.getInt(Constants.EXTRA_INDEX)
        viewModel.remitterId.value = arguments?.getString(Constants.EXTRA_REMITTER_ID)
        viewModel.responseMsg.value = arguments?.getString(Constants.EXTRA_RESPONSE_MESSAGE)
    }

    fun setDialogListener(listener: IDialogFinished) {
        dialogFinished = listener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register otp event listener
        otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                viewModel.otpCode.value = ""
            }

            override fun onOTPComplete(otp: String) {
                viewModel.otpCode.value = otp
            }
        }
        startCountTimer()
    }

    override fun registerObserver() {
        viewModel.verifyRemoveBeneficiaryData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    dialogFinished.onDialogFinished(viewModel.indexPosition.value!!)
                    dismiss()
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })

        viewModel.resendResultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    startCountTimer()
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })

        viewModel.closeClickEvent.observe(this, Observer {
            dismiss()
        })
    }


    companion object {
        val TAG = RemoveBeneficiaryOtpDialog::class.java.simpleName
        fun newInstance(
            benficiaryInfo: BeneficiaryInfo,
            index: Int,
            remitterId: String,
            message: String?
        ): RemoveBeneficiaryOtpDialog =
            RemoveBeneficiaryOtpDialog().apply {
                arguments = Bundle().apply {
                    putParcelable(Constants.EXTRA_BENEFICIARY_INFO, benficiaryInfo)
                    putInt(Constants.EXTRA_INDEX, index)
                    putString(Constants.EXTRA_REMITTER_ID, remitterId)
                    putString(Constants.EXTRA_RESPONSE_MESSAGE, message)
                }
            }
    }

    private fun startCountTimer() {
        startTimer { timerResult, isFinished ->
            tvCountTime?.text = timerResult
            viewModel.isTimerEnabled.value = isFinished
        }
    }

    interface IDialogFinished {
        fun onDialogFinished(position: Int)
    }
}