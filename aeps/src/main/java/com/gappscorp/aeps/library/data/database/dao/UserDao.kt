package com.gappscorp.aeps.library.data.database.dao

import androidx.room.*
import com.gappscorp.aeps.library.data.model.RetailerInfo
import com.gappscorp.aeps.library.data.model.UserDetail
import com.gappscorp.aeps.library.data.model.UserInfo

@Dao
interface UserDao {

    @Transaction
    suspend fun saveUserDetails(userDetail: UserDetail) {
        // update user info
        userDetail.userInfo?.let {
            deleteUserInfo()
            insertUserInfo(it)
        }

        // update retailer info
        userDetail.retailerInfo?.let {
            deleteRetailerInfo()
            insertRetailerInfo(it)
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserInfo(info: UserInfo)

    @Query("DELETE FROM UserInfo")
    suspend fun deleteUserInfo()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRetailerInfo(info: RetailerInfo)

    @Query("DELETE FROM RetailerInfo")
    suspend fun deleteRetailerInfo()

    @Query("SELECT * from UserInfo")
    fun getUserInfo(): UserInfo?

    @Query("SELECT * from RetailerInfo")
    fun getRetailerInfo(): RetailerInfo?

}