package com.gappscorp.aeps.library.ui.activity.aeps.statement.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.domain.network.response.MiniStatement

class MiniStatementListViewModel : ViewModel() {

    val shareClickEvent = SingleLiveEvent<Void>()

    val totalBalance = MutableLiveData<String>()
    val miniStatementListData = MutableLiveData<List<MiniStatement>>()

    fun onShareClick() {
        shareClickEvent.call()
    }
}