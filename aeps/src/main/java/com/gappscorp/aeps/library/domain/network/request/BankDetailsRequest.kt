package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class BankDetailsRequest(
    @SerializedName("beneficiary_name")
    val name: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("ifsc_code")
    val ifscCode: String?,
    @SerializedName("account_no")
    val accountNumber: String?,
    @SerializedName("upi_id")
    val upiId: String?
)