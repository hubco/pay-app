package com.gappscorp.aeps.library.ui.fragment.retailer.otp

import `in`.aabhasjindal.otptextview.OTPListener
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.extensions.startTimer
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsDialogRegisterOtpScreenBinding
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.fragment.base.BaseDialogFragment
import kotlinx.android.synthetic.main.aeps_dialog_register_otp_screen.otpView
import kotlinx.android.synthetic.main.aeps_dialog_remove_beneficiary_otp.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RegisterOtpVerificationDialog : BaseDialogFragment<AepsDialogRegisterOtpScreenBinding>(){

    override val viewModel: RegisterOtpVerificationViewModel by viewModel()

    override val layoutRes = R.layout.aeps_dialog_register_otp_screen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.registerResponse.value =
            arguments?.getParcelable(Constants.EXTRA_REGISTER_RESPONSE)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register otp event listener
        otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                viewModel.otpCode.value = ""
            }

            override fun onOTPComplete(otp: String) {
                viewModel.otpCode.value = otp
            }
        }

        startCountTimer()
    }


    override fun registerObserver() {
        viewModel.statusData.observe(this,Observer{
            when (it) {
                Status.LOADING -> activity?.hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    //Open Detail Page
                    navigateTo(Routes.MONEY_TRANSFER_SCREEN, mutableMapOf<String,PhoneRemitterResponse>()
                        .apply {

                           // viewModel.registerResponse.value!!.remitterInfo = it.data.remitterInfo
                           // put(Constants.EXTRA_REMITTER_RESPONSE,viewModel.registerResponse.value!!)
                            put(Constants.EXTRA_REMITTER_RESPONSE,it.data)
                        })

                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })

        viewModel.resendOtpData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    startCountTimer()
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })
    }

    private fun startCountTimer() {
        startTimer { timerResult, isFinished ->
            tvCountTime?.text = timerResult
            viewModel.isTimerEnabled.value = isFinished
        }
    }

    companion object {
        val TAG = RegisterOtpVerificationDialog::class.java.simpleName
        fun newInstance(registerResponse: PhoneRemitterResponse?): RegisterOtpVerificationDialog =
            RegisterOtpVerificationDialog().apply {
                arguments = Bundle().apply {
                putParcelable(Constants.EXTRA_REGISTER_RESPONSE, registerResponse)
                }

            }
    }

}