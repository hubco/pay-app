package com.gappscorp.aeps.library.app

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gappscorp.aeps.library.BuildConfig
import com.gappscorp.aeps.library.common.config.AepsConfig
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.logger.FileLoggingTree
import com.gappscorp.aeps.library.di.koinModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

open class CoreApp : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
        // get aeps configuration
        configuration = aepsConfiguration()

        // Set Timber
        if (BuildConfig.DEBUG)
            Timber.plant(FileLoggingTree())
//            Timber.plant(Timber.DebugTree())

        // Initialize Koin
        startKoin {
            androidLogger()
            androidContext(this@CoreApp)
            modules(koinModules)
        }
    }

    companion object {
        lateinit var appContext: Context
        lateinit var configuration: AepsConfig

        fun notifySessionExpired() {
            LocalBroadcastManager.getInstance(appContext).sendBroadcastSync(Intent().apply {
                action = Constants.ACTION_SESSION_EXPIRED
            })
        }
    }

    // override this function in child class to launch
    // app's main activity when aeps's is about to finish
    open fun onLeavingAeps(isTaskRoot: Boolean) {}

    // override this function in child class to
    // provide flavour specific configuration to aeps module
    open fun aepsConfiguration(): AepsConfig = AepsConfig()
}