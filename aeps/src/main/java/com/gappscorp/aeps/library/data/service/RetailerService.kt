package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.domain.network.request.*
import com.gappscorp.aeps.library.domain.network.response.*
import retrofit2.Response
import retrofit2.http.*

interface RetailerService {

    @POST("api/retailer-api/v1/dmt/remitters")
    @FormUrlEncoded
    suspend fun verifyPhone(@Field("mobile_no") phoneNumber: String) : Response<PhoneRemitterResponse>

    @POST("api/retailer-api/v1/dmt/remitters/register")
    suspend fun retailRegister(@Body request: RetailRegisterRequest) : Response<PhoneRemitterResponse>

    @POST("api/retailer-api/v1/dmt/remitters/verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(@Field("otp") otpCode: String,@Field("remitter_id") remitterId: String) : Response<PhoneRemitterResponse>

    @POST("api/retailer-api/v1/dmt/remitters/resend-otp")
    @FormUrlEncoded
    suspend fun resendOtp(@Field("remitter_id") remitterId: String) : Response<ResendOtpResponse>

    @POST("api/retailer-api/v1/dmt/remitters/beneficiary")
    suspend fun addBeneficiary(@Body addBeneficiaryData: AddBeneficiaryRequest) : Response<AddBeneficiaryResponse>

    @POST("api/retailer-api/v1/dmt/remitters/beneficiary/transfer")
    suspend fun postMoneyTransfer(@Body moneyTransferData: MoneyTransferRequest) : Response<MoneyTransferResponse>

    @HTTP(method = "DELETE",path = "api/retailer-api/v1/dmt/remitters/beneficiary/remove",hasBody = true)
    suspend fun removeBeneficiary(@Body request: RemoveBeneficiaryRequest)  : Response<GenericResponse>

    @HTTP(method = "DELETE",path = "api/retailer-api/v1/dmt/remitters/beneficiary/verify-remove",hasBody = true)
    suspend fun verifyBeneficiary(@Body verifyRemoveBeneficiaryRequest: VerifyRemoveBeneficiaryRequest) : Response<GenericResponse>

    @POST("api/retailer-api/v1/dmt/remitters/beneficiary/transfer/verify-otp")
    suspend fun verifyMoneyTransfer(@Body verifyMoneyTransferRequest: VerifyMoneyTransferRequest) : Response<MoneyTransferVerifyResponse>

    @POST("api/retailer-api/v1/dmt/remitters/beneficiary/transfer/resend-otp")
    suspend fun resetMoneyTransferOtp(@Body verifyMoneyTransferRequest: VerifyMoneyTransferRequest): Response<MoneyTransferResponse>

}