package com.gappscorp.aeps.library.domain.network.response

data class GenericResponse(
    val message: String?
)