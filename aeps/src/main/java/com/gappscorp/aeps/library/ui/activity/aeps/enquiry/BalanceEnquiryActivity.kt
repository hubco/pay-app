package com.gappscorp.aeps.library.ui.activity.aeps.enquiry

import android.location.Location
import android.os.Bundle
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.capitalize
import com.gappscorp.aeps.library.common.extensions.checkDeviceStatus
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.observeOnce
import com.gappscorp.aeps.library.common.extensions.scanFingerprint
import com.gappscorp.aeps.library.common.formatter.formatAadharNumber
import com.gappscorp.aeps.library.common.navigation.navigateToBankListScreen
import com.gappscorp.aeps.library.common.navigation.navigateToSuccessErrorScreen
import com.gappscorp.aeps.library.data.mapper.BalanceEnquirySuccessErrorMapper
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityBalanceEnquiryBinding
import com.gappscorp.aeps.library.ui.activity.aeps.base.BaseAepsActivity
import kotlinx.android.synthetic.main.aeps_activity_cash_withdrawal.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class BalanceEnquiryActivity : BaseAepsActivity<AepsActivityBalanceEnquiryBinding>() {

    override val viewModel: BalanceEnquiryViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_balance_enquiry

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.etAadhaarNumber.formatAadharNumber()

        // show/hide options for auto-fill details
        viewModel.hasLastUsedPhoneNumber.value = sessionManager.lastUsedPhoneNumber.isNotEmpty()
        viewModel.hasLastUsedAadhaarNumber.value = sessionManager.lastUsedAadhaarNumber.isNotEmpty()
    }

    override fun registerObserver() {
        viewModel.settingsErrorEvent.observe(this, Observer {
            errorAlert(getString(R.string.error_getting_settings_data)) {
                onBackPressed()
            }
        })

        viewModel.deviceName.observeOnce(this) {
            if (!it.equals(sessionManager.lastUsedDevice, ignoreCase = true)) {
                viewModel.deviceName.value =
                    sessionManager.lastUsedDevice.capitalize(Locale.getDefault())
            }
        }

        viewModel.autoFillPhoneEvent.observe(this, Observer {
            viewModel.phoneNumber.value = sessionManager.lastUsedPhoneNumber
        })

        viewModel.autoFillAadhaarEvent.observe(this, Observer {
            viewModel.aadhaarNumber.value = sessionManager.lastUsedAadhaarNumber
        })

        viewModel.selectDeviceClickEvent.observe(this, Observer {
            spScanDevice.performClick()
        })

        viewModel.deviceScanClickEvent.observe(this, Observer {
            // check for device online
            checkDeviceStatus(viewModel.selectedDevice!!)
        })

        viewModel.selectBankClickEvent.observe(this, Observer {
            navigateToBankListScreen(BankListType.AEPS_BANK_LIST) { _, data ->
                viewModel.bankName.value = data?.bankName
                viewModel.bankCode.value = data?.bankCode
            }
        })

        viewModel.fingerprintScanClickEvent.observe(this, Observer {
            scanFingerprint(viewModel.selectedDevice!!, pidInputXml)
        })

        viewModel.dataSubmissionStatusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> showProgressDialog()
                else -> hideProgressDialog()
            }
        })

        viewModel.dataSubmissionResult.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    navigateToSuccessErrorScreen(
                        Constants.StatusScreenType.SUCCESS_SCREEN,
                        BalanceEnquirySuccessErrorMapper().success(it.data)
                    ) {
                        finish()
                    }
                }
                is Result.Error -> {
                    //  errorAlert(it.error)
                    navigateToSuccessErrorScreen(
                        Constants.StatusScreenType.ERROR_SCREEN,
                        BalanceEnquirySuccessErrorMapper().failure(it, viewModel.request)
                    ) { retry ->
                        if (retry == true) {
                            // reset fingerprint scan status
                            viewModel.onFingerprintScanCompleted(false)
                        } else {
                            finish()
                        }
                    }
                }
            }

            // save last used details
            sessionManager.setLastUsedPhoneNumber(viewModel.request.mobileNumber)
            sessionManager.setLastUsedAadhaarNumber(viewModel.request.aadhaarNumber)
        })
    }

    override fun onDeviceServiceConnected(connected: Boolean, serviceInfo: String?) {
        viewModel.onDeviceScanCompleted(connected)
        viewModel.isDeviceOnline.value = connected
    }

    override fun onFingerprintScanned(scanned: Boolean, pidData: String?) {
        viewModel.onFingerprintScanCompleted(scanned)
        viewModel.fingerPrintScanData.value = pidData
    }

    override fun onLocationReceived(location: Location?) {
        location?.let {
            viewModel.latitude.value = "${it.latitude}"
            viewModel.longitude.value = "${it.longitude}"
        }
    }
}