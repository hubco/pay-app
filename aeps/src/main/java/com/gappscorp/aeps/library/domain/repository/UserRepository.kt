package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.model.RetailerInfo
import com.gappscorp.aeps.library.data.model.User
import com.gappscorp.aeps.library.data.model.UserDetail
import com.gappscorp.aeps.library.data.model.UserInfo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.repository.base.IRepository
import com.gappscorp.aeps.library.data.model.KYCResponse

interface UserRepository : IRepository {

    suspend fun login(userName: String, password: String): Result<User>

    suspend fun quickLogin(): Result<User>

    suspend fun sendAadhaarOtp(get: String?): Result<KYCResponse>

    suspend fun submitAadhaarOtp(tId: String, otp: String): Result<KYCResponse>

    suspend fun submitBiometric(tId: String,deviceType: String,txtPidData: String): Result<KYCResponse>

    suspend fun resendAadhaarOtp(tId: String): Result<KYCResponse>

    suspend fun forgetPassword(userName: String): Result<OtpVerificationResponse>

    suspend fun forgetUsername(mobileNumber: String): Result<OtpVerificationResponse>

    suspend fun verifyOtp(
        userName: String,
        otpId: String,
        otpCode: String
    ): Result<GenericResponse>

    suspend fun changePassword(oldPassword: String, newPassword: String): Result<GenericResponse>

    suspend fun userDetails(): Result<UserDetail>

    suspend fun getUserInfo() : UserInfo?

    suspend fun getRetailerInfo() : RetailerInfo?
}