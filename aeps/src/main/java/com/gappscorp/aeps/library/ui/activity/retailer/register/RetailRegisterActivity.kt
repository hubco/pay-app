package com.gappscorp.aeps.library.ui.activity.retailer.register

import android.os.Bundle
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.architecture.singleClickEvent
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateToStateListScreen
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityRetailRegisterBinding
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import com.gappscorp.aeps.library.ui.fragment.retailer.otp.RegisterOtpVerificationDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RetailRegisterActivity : BaseActivity<AepsActivityRetailRegisterBinding>() {

    override val viewModel: RetailRegisterViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_retail_register
    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.mobileNumberData.value = intent?.getStringExtra(Constants.EXTRA_PHONE_NUMBER)

        dataBinding.edtStateId.singleClickEvent {
            navigateToStateListScreen { success, data ->
                if (success) {
                    dataBinding.edtStateId.setText(data?.name)
                    viewModel.stateIdData.value = data?.code
                }
            }
        }
    }

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.backButtonClickEvent.observe(this, Observer {
            onBackPressed()
        })

        viewModel.forgotClickEvent.observe(this, Observer {
            navigateTo(Routes.FORGET_PASSWORD_SCREEN)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    registerOtpVerificationScreen(it.data)

                }
                is Result.Error -> errorAlert(it.error)
            }
        })
    }

    private fun registerOtpVerificationScreen(registerResponse: PhoneRemitterResponse?) {
        val ft = supportFragmentManager.beginTransaction()
        val newFragment = RegisterOtpVerificationDialog.newInstance(registerResponse)
        newFragment.show(ft, RegisterOtpVerificationDialog.TAG)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        (application as CoreApp).onLeavingAeps(isTaskRoot)
    }
}