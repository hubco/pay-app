package com.gappscorp.aeps.library.ui.activity.dashboard

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Switch
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.authentication.Biometric
import com.gappscorp.aeps.library.common.callback.AepsModuleCallback
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.alert
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.dialog.matmCWAlert
import com.gappscorp.aeps.library.common.extensions.load
import com.gappscorp.aeps.library.common.extensions.toast
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateToLogin
import com.gappscorp.aeps.library.common.navigation.navigateToSuccessErrorScreen
import com.gappscorp.aeps.library.data.mapper.MAtmTransactionSuccessErrorMapper
import com.gappscorp.aeps.library.data.model.AepsModule
import com.gappscorp.aeps.library.data.model.ModuleType
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityMainBinding
import com.gappscorp.aeps.library.domain.network.response.MAtmBEResponse
import com.gappscorp.aeps.library.ui.activity.aeps.base.BaseAepsActivity
import com.gappscorp.aeps.library.ui.activity.dashboard.adapter.AepsModuleAdapter
import com.gappscorp.aeps.library.ui.fragment.retailer.PhoneVerificationBottomSheet
import com.rnfi.microatmlib.activities.HostActivity
import kotlinx.android.synthetic.main.aeps_activity_main.*
import kotlinx.android.synthetic.main.aeps_activity_main.view.*
import kotlinx.android.synthetic.main.aeps_common_tool_bar.*
import kotlinx.android.synthetic.main.aeps_nav_header_main.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val MATM_REQUEST_CODE: Int = 999

class MainActivity : BaseAepsActivity<AepsActivityMainBinding>(), AepsModuleCallback {

    override val viewModel: MainViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_main

    override val toolBarId: Int = R.id.commonToolBar

    private val cashWithdrawalModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_cash_withdrawal,
            getString(R.string.label_cash_withdrawal),
            sessionManager.isAepsCwEnabled,
            ModuleType.CASH_WITHDRAWAL
        )
    }
    private val balanceEnquiryModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_balance_enquiry,
            getString(R.string.label_balance_enquiry),
            sessionManager.isAepsBeEnabled,
            ModuleType.BALANCE_ENQUIRY
        )
    }
    private val miniStatementModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_mini_statement,
            getString(R.string.label_mini_statement),
            sessionManager.isAepsMsEnabled,
            ModuleType.MINI_STATEMENT
        )
    }
    private val cashDepositModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_cash_deposit,
            getString(R.string.label_cash_deposit),
            sessionManager.isAepsCdEnabled,
            ModuleType.CASH_DEPOSIT
        )
    }

    private val retailerModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_cash_deposit,
            getString(R.string.label_dmt),
            sessionManager.isDmtEnabled,
            ModuleType.DMT
        )
    }

    private val matmBEModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_matm_cw,
            getString(R.string.label_matm_be),
            sessionManager.isMAtmBeEnable,
            ModuleType.MATM_BE
        )
    }

    private val matmCWModule by lazy {
        AepsModule(
            R.drawable.aeps_ic_matm_cw,
            getString(R.string.label_matm_cw),
            sessionManager.isMAtmCWEnable,
            ModuleType.MATM_CW
        )
    }

    private val aepsModuleList: List<AepsModule> by lazy {
        mutableListOf<AepsModule>().apply {
            if (sessionManager.isAepsCwEnabled)
                add(cashWithdrawalModule)
            if (sessionManager.isAepsBeEnabled)
                add(balanceEnquiryModule)
            if (sessionManager.isAepsMsEnabled)
                add(miniStatementModule)
            if (sessionManager.isAepsCdEnabled)
                add(cashDepositModule)
            if (sessionManager.isDmtEnabled)
                add(retailerModule)
            if (sessionManager.isMAtmBeEnable)
                add(matmBEModule)
            if (sessionManager.isMAtmCWEnable)
                add(matmCWModule)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // show username and image
        viewModel.userName.value = sessionManager.getUsername()
        val header = drawer.navView.getHeaderView(0)
        header.tvName.text = sessionManager.getUsername()
        header.ivAvatar.load(sessionManager.getUserAvatar(), R.drawable.aeps_ic_person)

        // setup navigation drawer
        val toggle = ActionBarDrawerToggle(
            this, drawer, commonToolBar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()


        val biometricSwitch = (navView.menu.findItem(R.id.nav_biometric).actionView as Switch?)
        // show biometric login only if device supported
        Biometric.isAvailable.run {
            navView.menu.setGroupVisible(R.id.biometric, this)
            biometricSwitch?.isChecked = sessionManager.isBiometricLoginEnabled()
        }

        // add check change listener on switch button of biometric login
        biometricSwitch?.setOnClickListener {
            authenticateBiometricLogin {
                biometricSwitch.isChecked = it
            }
        }

        // add navigation item click event
        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_home -> closeDrawer()
                R.id.nav_cash_withdrawal -> closeDrawer {
                    onAepsModuleClick(cashWithdrawalModule)
                }
                R.id.nav_balance_enquiry -> closeDrawer {
                    onAepsModuleClick(balanceEnquiryModule)
                }
                R.id.nav_mini_statement -> closeDrawer {
                    onAepsModuleClick(miniStatementModule)
                }
                R.id.nav_cash_deposit -> closeDrawer {
                    onAepsModuleClick(cashDepositModule)
                }
                R.id.nav_matm_be -> closeDrawer {
                    onAepsModuleClick(matmBEModule)
                }
                R.id.nav_matm_cw -> closeDrawer {
                    onAepsModuleClick(matmCWModule)
                }
                R.id.nav_biometric -> authenticateBiometricLogin {
                    biometricSwitch?.isChecked = it
                }
                R.id.nav_change_password -> closeDrawer {
                    navigateTo(Routes.CHANGE_PASSWORD_SCREEN)
                }
                R.id.nav_logout -> closeDrawer {
                    confirmLogout()
                }
            }
            true
        }

        // setup aeps module
        rvAepsModule.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = AepsModuleAdapter(aepsModuleList, this@MainActivity)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_profile, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_profile) {
            navigateTo(Routes.USER_PROFILE_SCREEN)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun registerObserver() {
        viewModel.walletCardClickEvent.observe(this, {
            navigateTo(Routes.PAYOUT_SCREEN)
        })

        viewModel.mAtmBeResultData.observe(this, {
            when (it) {
                is Result.Success -> {
                    try {
                        createMatmData(it.data)
                    } catch (ex: ActivityNotFoundException) {
                        errorAlert(getString(R.string.error_matm_service_not_found))
                        hideProgressDialog()
                    }
                }
                is Result.Error -> errorAlert(it.error)
            }
        })

        viewModel.beStatusData.observe(this, {
            if(it == Status.ERROR) hideProgressDialog()
        })

        viewModel.processMatmResultData.observe(this, {
            hideProgressDialog()
            when (it) {
                is Result.Success -> {
                    viewModel.setTransactionResponse(it.data)
                    navigateToSuccessErrorScreen(
                        Constants.StatusScreenType.SUCCESS_SCREEN,
                        MAtmTransactionSuccessErrorMapper().success(it.data)
                    )
                }
                is Result.Error -> {
                    navigateToSuccessErrorScreen(
                        Constants.StatusScreenType.ERROR_SCREEN,
                        MAtmTransactionSuccessErrorMapper().failure(it, viewModel.amount.value)
                    )
                }
            }
        })
    }

    private fun closeDrawer(callback: (() -> Unit)? = null) {
        drawer.closeDrawer(GravityCompat.START)
        lifecycleScope.launch {
            delay(300)
            callback?.invoke()
        }
    }

    private fun authenticateBiometricLogin(callback: ((success: Boolean) -> Unit)?) {
        Biometric.authenticate(this) { authenticated, error ->
            if (authenticated) {
                val newValue = !sessionManager.isBiometricLoginEnabled()
                sessionManager.setBiometricLogin(newValue)
                callback?.invoke(newValue)
            } else {
                toast(error ?: getString(R.string.biometric_authentication_error))
                callback?.invoke(sessionManager.isBiometricLoginEnabled())
            }
        }
    }

    private fun confirmLogout() {
        alert {
            title = getString(R.string.title_logout)
            message = getString(R.string.title_logout_confirm)
            positiveButton(android.R.string.yes) {
                sessionManager.logout()
                navigateToLogin()
            }
            negativeButton(android.R.string.no) {}
        }.show()
    }

    override fun onAepsModuleClick(module: AepsModule) {
        when (module.type) {
            ModuleType.BALANCE_ENQUIRY -> navigateTo(Routes.BALANCE_ENQUIRY_SCREEN)
            ModuleType.CASH_DEPOSIT -> toast(R.string.feature_under_development)//navigateTo(Routes.CASH_DEPOSIT_SCREEN)
            ModuleType.CASH_WITHDRAWAL -> navigateTo(Routes.CASH_WITHDRAWAL_SCREEN)
            ModuleType.MINI_STATEMENT -> navigateTo(Routes.MINI_STATEMENT_SCREEN)
            ModuleType.DMT -> openBottomSheet()
            ModuleType.MATM_BE -> callMATMBe()
            ModuleType.MATM_CW -> openMATMCWAlert()
        }
    }

    private fun openBottomSheet() {
        val fragment = PhoneVerificationBottomSheet.newInstance()
        fragment.show(supportFragmentManager, PhoneVerificationBottomSheet.TAG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MATM_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                data?.let {
                    val bundle = it.extras
                    viewModel.amount.value = bundle?.getString(Constants.PARAM_AMOUNT, "0")
                    val json = JSONObject()
                    bundle?.keySet()?.forEach { key ->
                        try {
                            json.put(key, JSONObject.wrap(bundle[key]))
                        } catch (e: JSONException) {
                            //Handle exception here
                        }
                    }
                    viewModel.processMatmTransaction(json)
                }
            } else {
                hideProgressDialog()
            }
        }
    }

    private fun openMATMCWAlert() {
        matmCWAlert(isMATMCw = true) { amount, mobile, remark ->
            showProgressDialog()
            viewModel.postMATMBeData(amount, mobile, remark, Constants.MAtmType.MATM_CW)
        }
    }

    private fun callMATMBe() {
        matmCWAlert(isMATMCw = false) { _, mobile, remark ->
            showProgressDialog()
            viewModel.postMATMBeData("0", mobile, remark, Constants.MAtmType.MATM_BE)
        }
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else {
            confirmLogout()
        }
    }

    private fun createMatmData(mAtmBEResponse: MAtmBEResponse) {
        val intent = Intent(applicationContext, HostActivity::class.java)
        intent.putExtra(Constants.PARAM_PARTNER_ID, mAtmBEResponse.partnerId)
        intent.putExtra(Constants.PARAM_API_KEY, mAtmBEResponse.jwtKey)
        intent.putExtra(Constants.PARAM_TRANSACTION_TYPE, mAtmBEResponse.matmType) // BE for Balance Enquiry and CW for Cash Withdrawal
        intent.putExtra(Constants.PARAM_AMOUNT, mAtmBEResponse.amount) // 0 for Balance Enquiry and Amount for Cash Withdrawal
        intent.putExtra(Constants.PARAM_REMARKS, mAtmBEResponse.remark) // Transaction remarks
        intent.putExtra(Constants.PARAM_MOBILE_NUMBER, mAtmBEResponse.mobileNumber) // Customer Mobile Number
        intent.putExtra(Constants.PARAM_REFERENCE_NUMBER, mAtmBEResponse.transactionId) // Reference Number
        intent.putExtra(Constants.PARAM_LATITUDE, mAtmBEResponse.latitude) // Latitude
        intent.putExtra(Constants.PARAM_LONGITUDE, mAtmBEResponse.longitude) // Longitude
        intent.putExtra(Constants.PARAM_SUB_MERCHANT_ID, mAtmBEResponse.subMerchantId) // Sub Merchant Id
        intent.putExtra(Constants.PARAM_DEVICE_MANUFACTURED_ID, mAtmBEResponse.manufacturerId) // manufacturer id
        startActivityForResult(intent, MATM_REQUEST_CODE)
    }

    override fun onDeviceServiceConnected(connected: Boolean, serviceInfo: String?) {
    }

    override fun onFingerprintScanned(scanned: Boolean, pidData: String?) {
    }

    override fun onLocationReceived(location: Location?) {
        location?.let {
            viewModel.latitude.value = "${it.latitude}"
            viewModel.longitude.value = "${it.longitude}"
        }
    }
}