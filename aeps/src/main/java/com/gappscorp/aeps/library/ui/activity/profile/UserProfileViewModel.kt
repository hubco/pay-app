package com.gappscorp.aeps.library.ui.activity.profile

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.mapper.UserRetailerMapper
import com.gappscorp.aeps.library.data.model.UserProfileItem
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserProfileViewModel(private val repository: UserRepository) : ViewModel() {

    val username = MutableLiveData<String>()
    val userAvatar = MutableLiveData<String>()
    val retailerAvatar = MutableLiveData<String>()
    val isBioKyc = ObservableBoolean(false)
    val getKycInvoked = SingleLiveEvent<Boolean>()

    val userList = MutableLiveData<List<UserProfileItem>>()
    val retailerList = MutableLiveData<List<UserProfileItem>>()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            // load retailer details
            repository.getRetailerInfo()?.let {
                isBioKyc.set(it.isBioKyc.equals("false"))
                retailerAvatar.postValue(it.avatar)
                retailerList.postValue(UserRetailerMapper().map(it))
            }
            //load user info
            repository.getUserInfo().let {
                username.postValue(it?.name)
                userAvatar.postValue(it?.avatar)
                userList.postValue(UserRetailerMapper().map(it))
            }
        }
    }

    fun onGetKycClick(){
        getKycInvoked.call()
    }
}