package com.gappscorp.aeps.library.data.model


data class UserProfileItem(val title: String, val value: String?)