package com.gappscorp.aeps.library.ui.activity.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.MATMProcessRequest
import com.gappscorp.aeps.library.domain.network.response.MAtmBEResponse
import com.gappscorp.aeps.library.domain.network.response.MAtmTransactionResponse
import com.gappscorp.aeps.library.domain.repository.AepsRepository
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

class MainViewModel(
    private val repository: UserRepository,
    private val serviceRepository: SettingsRepository,
    private val aepsRepository: AepsRepository
) : ViewModel() {

    val walletCardClickEvent = SingleLiveEvent<Void>()
    val userName = MutableLiveData<String>()

    val latitude = MutableLiveData<String>()
    val longitude = MutableLiveData<String>()
    private var _tId: String? = null
    val amount = MutableLiveData<String>()

    private val _balanceData = MutableLiveData("0")
    val balanceData: LiveData<String>
        get() = _balanceData

    private val _mAtmBeResultData = MutableLiveData<Result<MAtmBEResponse>>()
    val mAtmBeResultData: LiveData<Result<MAtmBEResponse>>
        get() = _mAtmBeResultData

    private val _processMatmResultData = MutableLiveData<Result<MAtmTransactionResponse>>()
    val processMatmResultData: LiveData<Result<MAtmTransactionResponse>>
        get() = _processMatmResultData

    private var _processTransactionResponse: MAtmTransactionResponse? = null
    val processTransactionResponse: MAtmTransactionResponse?
        get() = _processTransactionResponse

    fun setTransactionResponse(response: MAtmTransactionResponse) {
        _processTransactionResponse = response
    }

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _beStatusData = MutableLiveData<Status>()
    val beStatusData: LiveData<Status>
        get() = _beStatusData

    init {
        viewModelScope.launch {
            serviceRepository.getSettings()
        }
        onRefreshBalanceClicked()
    }

    fun onWalletCardClicked() {
        walletCardClickEvent.call()
    }

    fun onRefreshBalanceClicked() {
        if (_statusData.value == Status.LOADING) return
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            when (val result = repository.userDetails()) {
                is Result.Success -> {
                    _balanceData.postValue("${result.data.retailerInfo?.balance ?: 0}")
                    _statusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun postMATMBeData(amount: String?, mobile: String?, remark: String?, matmType: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _beStatusData.postValue(Status.LOADING)
            val result = aepsRepository.sendMATMBeData(
                MATMProcessRequest(
                    mobileNumber = mobile,
                    amount = amount,
                    remarks = remark,
                    latitude = latitude.value,
                    longitude = longitude.value
                )
            )
            _mAtmBeResultData.postValue(result)
            when (result) {
                is Result.Success -> {
                    result.data.matmType = matmType
                    result.data.remark = remark
                    _tId = result.data.tId
                    _beStatusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> {
                    _beStatusData.postValue(Status.ERROR)
                }
            }
        }
    }

    fun processMatmTransaction(response: JSONObject) {
        viewModelScope.launch {
            response.put("t_id", _tId)
            val requestBody =
                RequestBody.create(MediaType.parse("application/json"), response.toString())
            val result = aepsRepository.processMAtmTransation(requestBody)
            _processMatmResultData.postValue(result)
        }
    }
}