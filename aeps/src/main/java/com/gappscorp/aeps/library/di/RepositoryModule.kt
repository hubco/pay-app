package com.gappscorp.aeps.library.di

import com.gappscorp.aeps.library.data.database.dao.SettingsDao
import com.gappscorp.aeps.library.data.database.dao.UserDao
import com.gappscorp.aeps.library.data.repository.*
import com.gappscorp.aeps.library.data.service.*
import com.gappscorp.aeps.library.domain.repository.*

fun provideUserRepository(dao: UserDao, service: UserService): UserRepository = UserRepositoryImpl(dao, service)

fun provideAepsRepository(service: AepsService): AepsRepository = AepsRepositoryImpl(service)

fun provideSettingsRepository(dao: SettingsDao, service: SettingsService): SettingsRepository = SettingsRepositoryImpl(dao, service)

fun providePayoutRepository(service: PayoutService): PayoutRepository = PayoutRepositoryImpl(service)

fun provideBankRepository(service: BankService): BankRepository = BankRepositoryImpl(service)

fun provideRetailerRepository(service: RetailerService): RetailerRepository = RetailerRepositoryImpl(service)