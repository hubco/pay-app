package com.gappscorp.aeps.library.ui.activity.dashboard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.callback.AepsModuleCallback
import com.gappscorp.aeps.library.data.model.AepsModule
import com.gappscorp.aeps.library.databinding.AepsLayoutCardAepsModuleBinding

class AepsModuleAdapter(
    private val list: List<AepsModule>,
    private val callback: AepsModuleCallback
) : RecyclerView.Adapter<AepsModuleAdapter.AepsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AepsViewHolder {
        val binding = DataBindingUtil.inflate<AepsLayoutCardAepsModuleBinding>(
            LayoutInflater.from(parent.context),
            R.layout.aeps_layout_card_aeps_module,
            parent,
            false
        )
        return AepsViewHolder(binding, callback)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: AepsViewHolder, position: Int) {
        holder.setData(list[position])
    }

    inner class AepsViewHolder(
        private val binding: AepsLayoutCardAepsModuleBinding,
        private val callback: AepsModuleCallback
    ) : RecyclerView.ViewHolder(binding.root) {
        fun setData(module: AepsModule) {
            binding.module = module
            binding.icon = ContextCompat.getDrawable(binding.root.context, module.icon)
            binding.callback = callback
        }
    }
}