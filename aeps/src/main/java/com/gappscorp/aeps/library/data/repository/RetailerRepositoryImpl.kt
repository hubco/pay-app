package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.RetailerService
import com.gappscorp.aeps.library.domain.network.request.*
import com.gappscorp.aeps.library.domain.network.response.*
import com.gappscorp.aeps.library.domain.repository.RetailerRepository

class RetailerRepositoryImpl(private val service: RetailerService) : RetailerRepository {

    override suspend fun verifyNumber(phoneNumber: String): Result<PhoneRemitterResponse> {
        return safeCall { service.verifyPhone(phoneNumber) }
    }

    override suspend fun retailRegister(request: RetailRegisterRequest): Result<PhoneRemitterResponse> {
        return safeCall { service.retailRegister(request) }
    }

    override suspend fun verifyOtp(otpCode: String, remitterId: String): Result<PhoneRemitterResponse> {
        return safeCall { service.verifyOtp(otpCode,remitterId) }
    }

    override suspend fun verifyResendOtp(remitterId: String): Result<ResendOtpResponse> {
        return safeCall { service.resendOtp(remitterId) }
    }

    override suspend fun addBeneficiary(addRequest: AddBeneficiaryRequest): Result<AddBeneficiaryResponse> {
        return safeCall { service.addBeneficiary(addRequest) }
    }

    override suspend fun moneyTransfer(request: MoneyTransferRequest): Result<MoneyTransferResponse> {
        return safeCall { service.postMoneyTransfer(request) }
    }

    override suspend fun removeBeneficiary(request: RemoveBeneficiaryRequest): Result<GenericResponse> {
        return safeCall { service.removeBeneficiary(request) }

    }

    override suspend fun verifyRemovedBeneficiary(verifyRemoveBeneficiaryRequest: VerifyRemoveBeneficiaryRequest): Result<GenericResponse> {
        return safeCall { service.verifyBeneficiary(verifyRemoveBeneficiaryRequest) }
    }

    override suspend fun verifyMoneyTransfer(verifyMoneyTransferRequest: VerifyMoneyTransferRequest): Result<MoneyTransferVerifyResponse> {
        return safeCall { service.verifyMoneyTransfer(verifyMoneyTransferRequest) }
    }

    override suspend fun resetMoneyTransferOtp(verifyMoneyTransferRequest: VerifyMoneyTransferRequest): Result<MoneyTransferResponse> {
        return safeCall { service.resetMoneyTransferOtp(verifyMoneyTransferRequest) }
    }


}