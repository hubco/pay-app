package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CashWithdrawalResponse(
    val status: String,
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    @SerializedName("bankrrn")
    val bankRrnNumber: String?,
    @SerializedName("balance")
    val availableBalance: String?,
    var amount: String?,
    var aadhaarNumber: String?,
    var bankName: String?
): Parcelable {
    val isPending: Boolean get() = status.equals("pending", ignoreCase = true)
}