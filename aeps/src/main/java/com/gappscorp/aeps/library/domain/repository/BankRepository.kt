package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.request.BankDetailsRequest
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.repository.base.IRepository

interface BankRepository : IRepository {

    suspend fun updateBankDetails(request: BankDetailsRequest) : Result<GenericResponse>
}