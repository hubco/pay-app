package com.gappscorp.aeps.library.di

import com.gappscorp.aeps.library.data.service.*
import retrofit2.Retrofit

private inline fun <reified T> createService(retrofit: Retrofit): T = retrofit.create(T::class.java)

fun provideUserService(retrofit: Retrofit) = createService<UserService>(retrofit)

fun provideAepsService(retrofit: Retrofit) = createService<AepsService>(retrofit)

fun provideSettingsService(retrofit: Retrofit) = createService<SettingsService>(retrofit)

fun providePayoutService(retrofit: Retrofit) = createService<PayoutService>(retrofit)

fun provideBankService(retrofit: Retrofit) = createService<BankService>(retrofit)

fun provideRetailerService(retrofit: Retrofit) = createService<RetailerService>(retrofit)