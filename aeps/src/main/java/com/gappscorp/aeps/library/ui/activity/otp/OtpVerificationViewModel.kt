package com.gappscorp.aeps.library.ui.activity.otp

import androidx.lifecycle.*
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.launch

class OtpVerificationViewModel(private val repository: UserRepository) : ViewModel() {

    val otpVerificationData = MutableLiveData<OtpVerificationResponse>()
    val otpCode = MutableLiveData<String>()

    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(otpCode) {
            value = !otpCode.value.isNullOrEmpty()
        }
    }

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<GenericResponse>>()
    val resultData: LiveData<Result<GenericResponse>>
        get() = _resultData

    fun onSubmitButtonClick() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.verifyOtp(
                otpVerificationData.value!!.userName!!,
                otpVerificationData.value!!.otpId,
                otpCode.value!!
            )
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(otpCode)
    }
}