package com.gappscorp.aeps.library.ui.activity.base

import androidx.databinding.ViewDataBinding
import com.gappscorp.aeps.library.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import com.karumi.dexter.listener.single.PermissionListener

abstract class PermissionActivity<VDB : ViewDataBinding> : BaseActivity<VDB>() {

    private val genericPermissionListener by lazy {
        DialogOnDeniedPermissionListener.Builder
            .withContext(this)
            .withTitle(getString(R.string.permission_rational_title))
            .withMessage(getString(R.string.permission_rational_message))
            .withButtonText(android.R.string.ok)
            .withIcon(R.drawable.aeps_ic_alert)
            .build()
    }

    protected fun requestPermission(permission: String, callback: (granted: Boolean) -> Unit) {
        Dexter.withContext(this)
            .withPermission(permission)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    callback.invoke(true)
                    genericPermissionListener.onPermissionGranted(response)
                }

                override fun onPermissionRationaleShouldBeShown(
                    request: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    genericPermissionListener.onPermissionRationaleShouldBeShown(request, token)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    callback.invoke(false)
                    genericPermissionListener.onPermissionDenied(response)
                }
            }).check()
    }

//    protected fun requestPermissions(vararg permissions: String, callback: (granted: Boolean) -> Unit) {
//        Dexter.withContext(this)
//            .withPermissions(permissions)
//            .withListener(object : PermissionListener {
//                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
//                    callback.invoke(true)
//                    genericPermissionListener.onPermissionGranted(response)
//                }
//
//                override fun onPermissionRationaleShouldBeShown(
//                    request: PermissionRequest?,
//                    token: PermissionToken?
//                ) {
//                    genericPermissionListener.onPermissionRationaleShouldBeShown(request, token)
//                }
//
//                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
//                    callback.invoke(false)
//                    genericPermissionListener.onPermissionDenied(response)
//                }
//            }).check()
//    }
}