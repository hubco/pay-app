package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class RetailRegisterRequest(
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("address")
    val address: String?,
    @SerializedName("state_id")
    val stateId: String?,
    @SerializedName("pincode")
    val pinCode: String?
)