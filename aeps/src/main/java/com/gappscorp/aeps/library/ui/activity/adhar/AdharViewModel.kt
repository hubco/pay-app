package com.gappscorp.aeps.library.ui.activity.adhar

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.model.BiometricDevice
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.concurrent.schedule

class AdharViewModel(
    private val userRepository: UserRepository,
    private val repository: SettingsRepository
) : ViewModel() {
    val statusData = MutableLiveData<Status>()
    val displayMessage = MutableLiveData<String>()
    var transactionId: String? = null
    val observeAadhaarNumber: ObservableField<String> = ObservableField()
    val observeOtp = ObservableField<String>()
    val onOtpSent = ObservableBoolean(false)
    val showFingerPrintBtn = ObservableBoolean(false)
    val resendOtpEnabled = ObservableBoolean(false)
    val showSubmitLayout = ObservableBoolean(false)
    val isKYCSuccess = MutableLiveData<Boolean>()

    private val _deviceList = MutableLiveData<List<String>>()
    val deviceList: LiveData<List<String>>
        get() = _deviceList
    private val _deviceScanStatusData = MutableLiveData<Status>()
    val deviceScanStatusData: LiveData<Status>
        get() = _deviceScanStatusData
    private val _fingerprintScanStatusData = MutableLiveData<Status>()
    val fingerprintScanStatusData: LiveData<Status>
        get() = _fingerprintScanStatusData
    private val _dataSubmissionStatusData = MutableLiveData<Status>()
    val dataSubmissionStatusData: LiveData<Status>
        get() = _dataSubmissionStatusData
    val deviceName = MutableLiveData<String>()
    val fingerPrintScanData = MutableLiveData<String>()
    val isFingerprintScanned = MutableLiveData<Boolean>()
    val isDeviceOnline = MediatorLiveData<Boolean>().apply {
        addSource(deviceName) {
            value = false
            onDeviceScanCompleted(false)
        }
    }

    private val biometricDeviceList = mutableListOf<BiometricDevice>()
    val selectedDevice get() = biometricDeviceList.find { it.name.equals(deviceName.value, true) }

    val selectDeviceClickEvent = SingleLiveEvent<Void>()
    val selectBankClickEvent = SingleLiveEvent<Void>()
    val deviceScanClickEvent = SingleLiveEvent<Void>()
    val fingerprintScanClickEvent = SingleLiveEvent<Void>()
    val latitude = MutableLiveData<String>()
    val longitude = MutableLiveData<String>()

    init {
        getDevicesList()
    }

    fun onSelectDeviceClicked() {
        selectDeviceClickEvent.call()
    }

    fun onDeviceScanClicked() {
        _deviceScanStatusData.value = Status.LOADING
        deviceScanClickEvent.call()
    }

    fun onDeviceScanCompleted(success: Boolean) {
        when (success) {
            true -> _deviceScanStatusData.value = Status.SUCCESS
            false -> _deviceScanStatusData.value = Status.ERROR
        }
    }

    fun onFingerprintScanClicked() {
        fingerprintScanClickEvent.call()
    }

    fun onProceedButtonClicked() {
        viewModelScope.launch {
            when(val result = userRepository.submitBiometric(transactionId!!,deviceName.value!!,fingerPrintScanData.value!!)){
                is Result.Success -> {
                    displayMessage.postValue(result.data.message)
                    isKYCSuccess.postValue(true)
                }
                is Result.Error -> {
                    statusData.postValue(Status.ERROR)
                }
            }
        }
    }

    fun onFingerprintScanCompleted(success: Boolean) {
        when (success) {
            true -> {
                _fingerprintScanStatusData.value = Status.SUCCESS
                isFingerprintScanned.postValue(true)
            }
            false -> _fingerprintScanStatusData.value = Status.ERROR
        }
    }

    fun sendAadhaarOtp() {
        if (observeAadhaarNumber.get()!!.length != 12) {
            displayMessage.value = "Aadhaar number must be of 12 digit"
            return
        }
        viewModelScope.launch {
            statusData.postValue(Status.LOADING)
            when (val result = userRepository.sendAadhaarOtp(observeAadhaarNumber.get())) {
                is Result.Success -> {
                    transactionId = result.data.t_id
                    displayMessage.postValue(result.data.message)
                    statusData.postValue(Status.SUCCESS)
                    onOtpSent.set(true)
                    showSubmitLayout.set(true)
                    Timer().schedule(30000) {
                        resendOtpEnabled.set(true)
                    }
                }
                is Result.Error -> {
                    statusData.postValue(Status.ERROR)
                }
            }
        }
    }

    fun onOtpSubmit() {
        if (observeOtp.get()!!.length < 7) {
            displayMessage.value = "Otp length must be greater than equal to 7"
            return
        }
        viewModelScope.launch {
            when (val result =
                userRepository.submitAadhaarOtp(transactionId!!, observeOtp.get()!!)) {
                is Result.Success -> {
                    transactionId = result.data.t_id
                    displayMessage.postValue(result.data.message)
                    statusData.postValue(Status.SUCCESS)
                    showFingerPrintBtn.set(true)
                    showSubmitLayout.set(false)
                }
                is Result.Error -> statusData.postValue(Status.ERROR)
            }
        }
    }

    fun resendOtp() {
        resendOtpEnabled.set(false)
        Timer().schedule(15000) {
            resendOtpEnabled.set(true)
        }
        viewModelScope.launch {
            statusData.postValue(Status.LOADING)
            when (val result = userRepository.resendAadhaarOtp(transactionId!!)) {
                is Result.Success -> {
                    transactionId = result.data.t_id
                    displayMessage.postValue(result.data.message)
                    statusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> statusData.postValue(Status.ERROR)
            }
        }
    }

    fun submitBiometric(tId: String, deviceType: String, txtPidData: String) {
        viewModelScope.launch {
            statusData.postValue(Status.LOADING)
            when (val result = userRepository.submitBiometric(tId, deviceType, txtPidData)) {
                is Result.Success -> {
                    displayMessage.postValue(result.data.message)
                    statusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> statusData.postValue(Status.ERROR)
            }
        }
    }

    private fun getDevicesList() {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.hasSettings()) {
                biometricDeviceList.addAll(repository.getBiometricDeviceList())
            } else {
                when (repository.getSettings()) {
                    is Result.Success -> biometricDeviceList.addAll(repository.getBiometricDeviceList())
                    //is Result.Error -> settingsErrorEvent.call()
                }
            }
            _deviceList.postValue(biometricDeviceList.map { it.name })
            deviceName.value?.let {
                deviceName.postValue(it)
            }
        }
    }

}