package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.common.callback.BeneficiaryItemCallback
import com.gappscorp.aeps.library.databinding.AepsBeneficiaryItemBinding
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo

class GetBeneficiaryListAdapter(
    private val beneficiaryList: List<BeneficiaryInfo>,
    private val callback: BeneficiaryItemCallback?
) :  RecyclerView.Adapter<GetBeneficiaryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GetBeneficiaryListAdapter.ViewHolder {
        val binding = AepsBeneficiaryItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return  ViewHolder(binding,callback)
    }

    override fun getItemCount(): Int {
        return if (beneficiaryList.isNullOrEmpty()) 0 else beneficiaryList.size
    }

    override fun onBindViewHolder(holder: GetBeneficiaryListAdapter.ViewHolder, position: Int) {
            holder.bind(beneficiaryList[position])
    }

    inner class ViewHolder(
        val binding: AepsBeneficiaryItemBinding,
        val callback: BeneficiaryItemCallback?
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: BeneficiaryInfo) {
            binding.item = item
            binding.index = adapterPosition
            binding.callback = callback
        }
    }
}