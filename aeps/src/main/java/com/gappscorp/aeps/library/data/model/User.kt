package com.gappscorp.aeps.library.data.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("active_modules")
    val activeModules: ActiveModules?,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    val name: String,
    @SerializedName("referral_code")
    val referralCode: String,
    val status: String,
    val token: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("last_used_device")
    val lastUsedDevice: String?
)

data class ActiveModules(
    @SerializedName("rnfi_aeps")
    val aeps: Aeps?,
    @SerializedName("ipay_dmt")
    val dmt: Dmt?,
    @SerializedName("rnfi_matm")
    val matm: MAtm?
)

data class Aeps(
    val active: Boolean,
    @SerializedName("aeps_be")
    val rnfiAepsBe: Boolean,
    @SerializedName("aeps_cd")
    val rnfiAepsCd: Boolean,
    @SerializedName("aeps_cw")
    val rnfiAepsCw: Boolean,
    @SerializedName("aeps_ms")
    val rnfiAepsMs: Boolean
)

data class Dmt(
    @SerializedName("active")
    val active: Boolean
)

data class MAtm(
    val active: Boolean,
    @SerializedName("matm_be")
    val matmBe: Boolean,
    @SerializedName("matm_cw")
    val matmCw: Boolean
)