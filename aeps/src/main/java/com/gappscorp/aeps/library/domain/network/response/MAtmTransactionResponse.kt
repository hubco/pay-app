package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MAtmTransactionResponse(
    val status: String,
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    @SerializedName("bankrrn")
    val bankRrnNumber: String?,
    @SerializedName("balance")
    val availableBalance: String?,
    var amount: String?,
    @SerializedName("card_no")
    var cardNumber: String?,
    @SerializedName("mobile_no")
    var mobileNo: String?
): Parcelable