package com.gappscorp.aeps.library.domain.network.response

import com.google.gson.annotations.SerializedName

data class PayoutResponse(
    val status: String,
    val message: String?,
    @SerializedName("tranx_id")
    val transactionId: String?,
    val amount: String?,
    val charges: String?,
    @SerializedName("t_amount")
    val totalAmount: String?
) {
    val isPending: Boolean get() = status.equals("pending", ignoreCase = true)
}