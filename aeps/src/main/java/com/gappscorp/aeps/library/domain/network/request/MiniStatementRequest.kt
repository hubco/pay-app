package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class MiniStatementRequest(
    @SerializedName("bank_name")
    val bankName: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("aadhaar_no")
    val aadhaarNumber: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("latitude")
    val latitude: String?,
    @SerializedName("longitude")
    val longitude: String?,
    @SerializedName("txtPidData")
    val pidData: String?,
    @SerializedName("device_type")
    val deviceType: String?
)