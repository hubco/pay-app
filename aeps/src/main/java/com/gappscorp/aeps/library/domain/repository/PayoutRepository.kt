package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.network.response.PayoutResponse
import com.gappscorp.aeps.library.domain.repository.base.IRepository

interface PayoutRepository : IRepository {

    suspend fun transfer(
        payoutMode: String?,
        amount: String?,
        remarks: String?
    ): Result<OtpVerificationResponse>

    suspend fun resendOtp(otpId: String): Result<OtpVerificationResponse>

    suspend fun verifyOtp(otpId: String, otpCode: String?) : Result<PayoutResponse>
}