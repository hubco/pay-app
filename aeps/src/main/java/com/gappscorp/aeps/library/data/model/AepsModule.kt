package com.gappscorp.aeps.library.data.model

enum class ModuleType {
    CASH_WITHDRAWAL,
    CASH_DEPOSIT,
    BALANCE_ENQUIRY,
    MINI_STATEMENT,
    DMT,
    MATM_BE,
    MATM_CW,
}

data class AepsModule(
    val icon: Int,
    val name: String,
    val enabled: Boolean,
    val type: ModuleType
)