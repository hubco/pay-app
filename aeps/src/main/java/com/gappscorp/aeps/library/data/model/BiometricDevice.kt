package com.gappscorp.aeps.library.data.model

data class BiometricDevice(
    val name: String,
    val packageName: String
)