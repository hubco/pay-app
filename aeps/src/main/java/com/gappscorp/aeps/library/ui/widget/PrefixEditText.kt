package com.gappscorp.aeps.library.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.gappscorp.aeps.library.R
class PrefixEditText : AppCompatEditText {
    private var _originalLeftPadding = -1f
    private var _prefix: String? = null
    private var _completeText: String? = null

    var prefix: String?
        get() = _prefix
        set(prefix) {
            _prefix = prefix
            invalidate()
        }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(
        context: Context,
        attributeSet: AttributeSet?
    ) {
        if (attributeSet != null) {
            val typedArray =
                context.obtainStyledAttributes(attributeSet, R.styleable.PrefixEditText)
            _prefix = typedArray.getString(R.styleable.PrefixEditText_prefix)
            _completeText = typedArray.getString(R.styleable.PrefixEditText_completedText)
            typedArray.recycle()
            _completeText = completedText

        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (_prefix != null) {
            calculatePrefix()
            canvas.drawText(
                _prefix!!,
                _originalLeftPadding,
                getLineBounds(0, null).toFloat(),
                paint
            )
        }
    }

    private fun calculatePrefix() {
        if (_originalLeftPadding == -1f) {
            val widths = FloatArray(_prefix!!.length)
            paint.getTextWidths(_prefix, widths)
            var textWidth = 0f
            for (w in widths) {
                textWidth += w
            }
            _originalLeftPadding = compoundPaddingLeft.toFloat()
            setPadding(
                (textWidth + _originalLeftPadding).toInt(),
                paddingRight, paddingTop,
                paddingBottom
            )
        }
    }

    val completedText: String
        get() = _prefix + text.toString()
}