package com.gappscorp.aeps.library.ui.activity.forgot

import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.navigateUpTo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityForgetPasswordBinding
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ForgetPasswordActivity : BaseActivity<AepsActivityForgetPasswordBinding>() {

    override val viewModel: ForgetPasswordViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_forget_password

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when(it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.forgetUsernameClickEvent.observe(this, Observer {
            navigateTo(Routes.FORGET_USER_NAME_SCREEN)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> navigateTo(
                    Routes.OTP_VERIFICATION_SCREEN,
                    mutableMapOf<String, OtpVerificationResponse>().apply {
                        put(Constants.EXTRA_OTP_VERIFICATION_DATA, it.data.apply {

                        })
                    })
                is Result.Error -> errorAlert(it.error)
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateUpTo(Routes.LOGIN_SCREEN)
    }
}