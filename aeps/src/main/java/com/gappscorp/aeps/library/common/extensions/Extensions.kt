package com.gappscorp.aeps.library.common.extensions

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.CountDownTimer
import android.text.InputFilter
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.reflect.Field
import java.util.*

/**
 * Show hidden view
 */
fun View.showView() {
    if (!this.isShown) this.visibility = View.VISIBLE
}

/**
 * Hide View
 */
fun View.hideView() {
    if (this.isShown) this.visibility = View.GONE
}

/**
 * Show multiple views
 */
fun showViews(vararg views: View) {
    views.forEach { view -> view.showView() }
}

/**
 * Hide multiple views
 */
fun hideViews(vararg views: View) {
    views.forEach { view -> view.hideView() }
}

/**
 * Extension method to show a keyboard for View.
 */
fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun Activity.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun View.rotate() {
    val anim = RotateAnimation(
        0.0f, 360.0f,
        Animation.RELATIVE_TO_SELF, .5f,
        Animation.RELATIVE_TO_SELF, .5f
    ).apply {
        interpolator = LinearInterpolator()
        repeatCount = Animation.INFINITE
        duration = 800
    }
    animation = anim
    startAnimation(anim)
}

/**
 * Extension method to show toast for Context.
 */
fun Context.toast(text: CharSequence, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, text, duration).show()

fun Context.toDp(value: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics)

fun String.removeSpecialChars(): String {
    return Regex("[^a-zA-Z0-9]").replace(this, "")
}

fun Context.toast(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toastInCenter(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toastInCenter(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
    }.show()
}

fun Context.toast(textId: Int) {
    Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show()
}

fun Context.readFileFromAssets(fileName: String): String {
    val builder = StringBuilder()
    val `is`: InputStream

    try {
        `is` = getAssets().open(fileName)
        val bufferedReader =
            BufferedReader(InputStreamReader(`is`, "UTF-8"))
        var str: String?
        while (bufferedReader.readLine().also { str = it } != null) {
            builder.append(str)
        }
        bufferedReader.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return builder.toString()
}

fun <T> List<T>.toLiveDataList(): LiveData<List<T>> = MutableLiveData(this)

fun String.capitalize(locale: Locale): String {
    if (isNotEmpty()) {
        val firstChar = this[0]
        if (firstChar.isLowerCase()) {
            return buildString {
                val titleChar = firstChar.toTitleCase()
                if (titleChar != firstChar.toUpperCase()) {
                    append(titleChar)
                } else {
                    append(this@capitalize.substring(0, 1).toUpperCase(locale))
                }
                append(this@capitalize.substring(1))
            }
        }
    }
    return this
}

fun <T> LiveData<T>.observeOnce(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, object : Observer<T> {
        override fun onChanged(value: T) {
            removeObserver(this)
            observer(value)
        }
    })
}

fun View.getBitmap(): Bitmap {
    val returnedBitmap =
        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(returnedBitmap)
    if (background != null) background.draw(canvas) else canvas.drawColor(Color.WHITE)
    draw(canvas)
    return returnedBitmap
}

fun String?.maskAadharNumber(): String? {
    return if (this != null && this.length >= 12) {
        val length = this.length - 4
        val s = this.substring(0, length)
        s.replace("[A-Za-z0-9]".toRegex(), "X") + this.substring(length)
    } else this
}

inline fun <reified T> Context.getConfigValue(key: String): T? {
    try {
        val clazz = Class.forName("$packageName.BuildConfig")
        val field: Field = clazz.getField(key)
        return field.get(null) as T
    } catch (e: ClassNotFoundException) {
        e.printStackTrace()
    } catch (e: NoSuchFieldException) {
        e.printStackTrace()
    } catch (e: IllegalAccessException) {
        e.printStackTrace()
    }
    return null
}

fun DialogFragment.startTimer(callback: ((timerTime: String, isFinished: Boolean) -> Unit)? = null) {
    val timer = object : CountDownTimer(50000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            val timeResult =
                "${(millisUntilFinished / 1000 / 60).toString().padStart(2, '0')}:" +
                        "${(millisUntilFinished / 1000 % 60).toString().padStart(2, '0')} "

            callback?.invoke(timeResult, false)
        }

        override fun onFinish() {
            callback?.invoke("", true)
        }
    }
    timer.start()
}

fun EditText.allowAlphanumericOnly() {
    filters = filters.plus(
        listOf(
            InputFilter { source, _, _, _, _, _ ->
                source.replace(Regex("[^A-Za-z0-9]"), "")
            },
            InputFilter.AllCaps()
        )
    )
}