package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.add.AddBeneficiaryFragment
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.base.BaseBeneficiaryFragment
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list.BeneficiaryListFragment

class BeneficiaryPagerAdapter(
    context: Context,
    fragmentManager: FragmentManager,
    remitterResponse: PhoneRemitterResponse?
) : FragmentPagerAdapter(fragmentManager,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    private val addBeneficiaryFragment by lazy {
        AddBeneficiaryFragment.newInstance(response)
    }

    private val beneficiaryListFragment by lazy {
        BeneficiaryListFragment.newInstance(response)
    }

    val response = remitterResponse
    private val TITLES = arrayOf(context.resources.getString(R.string.label_add_beneficiary), context.resources.getString(R.string.label_beneficiary_list))

    override fun getItem(position: Int): BaseBeneficiaryFragment<*,*> {
        return when (position) {
            1 -> beneficiaryListFragment
            else -> addBeneficiaryFragment
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TITLES[position]
    }

    override fun getCount(): Int {
       return TITLES.size
    }
}