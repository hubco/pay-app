package com.gappscorp.aeps.library.ui.activity.aeps.statement.list

import android.os.Bundle
import androidx.core.view.drawToBitmap
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.share.Share
import com.gappscorp.aeps.library.databinding.AepsActivityMiniStatementListBinding
import com.gappscorp.aeps.library.domain.network.response.MiniStatementResponse
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class MiniStatementListActivity : BaseActivity<AepsActivityMiniStatementListBinding>() {

    override val viewModel: MiniStatementListViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_mini_statement_list

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.getParcelableExtra<MiniStatementResponse>(Constants.EXTRA_MINI_STATEMENT_LIST_DATA)?.let {
            viewModel.miniStatementListData.value = it.statementList
            viewModel.totalBalance.value = it.availableBalance
        }
    }

    override fun registerObserver() {
        viewModel.shareClickEvent.observe(this, Observer {
            dataBinding.btnShare.isVisible = false
            Share.shareImage(this, dataBinding.container.drawToBitmap())
            dataBinding.btnShare.isVisible = true
        })
    }
}