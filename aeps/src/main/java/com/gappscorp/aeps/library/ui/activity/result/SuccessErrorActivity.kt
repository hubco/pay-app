package com.gappscorp.aeps.library.ui.activity.result

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.core.view.drawToBitmap
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.toastInCenter
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.common.share.Share
import com.gappscorp.aeps.library.databinding.AepsActivitySuccessErrorBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SuccessErrorActivity : BaseActivity<AepsActivitySuccessErrorBinding>() {

    override val viewModel: SuccessErrorViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_success_error

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val isSuccess = intent.getIntExtra(
            Constants.EXTRA_STATUS_TYPE,
            0
        ) == Constants.StatusScreenType.SUCCESS_SCREEN
        viewModel.isSuccessData.value = isSuccess

        viewModel.data.value = intent.getParcelableExtra(Constants.EXTRA_API_STATUS_DATA)

        updateStatusSarColor(isSuccess)
    }

    override fun registerObserver() {
        viewModel.copyClickEvent.observe(this, Observer {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Transaction Id", viewModel.transactionId.value)
            clipboard.setPrimaryClip(clip)
            toastInCenter("Copied!")
        })

        viewModel.shareClickEvent.observe(this, Observer {
            val bitmap = dataBinding.parent.drawToBitmap()
            Share.shareImage(this, bitmap)
        })

        viewModel.retryClickEvent.observe(this, Observer {
            val intent = Intent()
            intent.putExtra(Constants.EXTRA_IS_RETRY, true)
            setResult(intent)
        })
    }

    private fun updateStatusSarColor(success: Boolean) {
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        // finally change the color
        val color = if (success) R.color.colorLightBlue else R.color.colorLightRed
        window.statusBarColor = ContextCompat.getColor(this, color)
    }
}