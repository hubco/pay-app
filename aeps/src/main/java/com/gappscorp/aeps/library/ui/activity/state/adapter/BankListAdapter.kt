package com.gappscorp.aeps.library.ui.activity.state.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.common.callback.StateItemCallback
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.databinding.AepsLayoutStateItemBinding
import java.util.*

class StateListAdapter(
    private val stateList: List<State>,
    private val callback: StateItemCallback?
) :
    RecyclerView.Adapter<StateListAdapter.ViewHolder>() {

    private val filteredStateList = stateList.toMutableList()

    fun searchState(query: String?, callback: (found: Boolean) -> Unit) {
        filteredStateList.clear()
        if (query.isNullOrEmpty()) {
            filteredStateList.addAll(stateList)
        } else {
            filteredStateList.addAll(stateList.filter {
                it.name.toLowerCase(Locale.getDefault()).contains(query)
            })
        }
        callback.invoke(filteredStateList.isNotEmpty())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AepsLayoutStateItemBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolder(binding, callback)
    }

    override fun getItemCount(): Int {
        return if (filteredStateList.isNullOrEmpty()) 0 else filteredStateList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filteredStateList[position])
    }

    inner class ViewHolder(
        val binding: AepsLayoutStateItemBinding,
        private val callback: StateItemCallback?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: State) {
            binding.state = item
            binding.callback = callback
        }
    }
}

