package com.gappscorp.aeps.library.common.callback

import com.gappscorp.aeps.library.data.model.State

interface StateItemCallback {
    fun onStateClicked(state: State)
}