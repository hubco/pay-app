package com.gappscorp.aeps.library.di

import com.gappscorp.aeps.library.data.database.AepsDatabase
import com.gappscorp.aeps.library.data.database.dao.SettingsDao
import com.gappscorp.aeps.library.data.database.dao.UserDao

fun provideSettingsDao(db: AepsDatabase): SettingsDao = db.settingsDao()

fun provideUserDao(db: AepsDatabase): UserDao = db.userDao()