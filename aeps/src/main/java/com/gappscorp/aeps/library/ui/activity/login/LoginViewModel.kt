package com.gappscorp.aeps.library.ui.activity.login

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.model.User
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: UserRepository) : ViewModel() {

    val userNameData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()
    val loginButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(userNameData) {
            value = hasLoginData()
        }
        addSource(passwordData) {
            value = hasLoginData()
        }
    }

    val backButtonClickEvent = SingleLiveEvent<Void>()
    val quickLoginClickEvent = SingleLiveEvent<Void>()
    val forgotClickEvent = SingleLiveEvent<Void>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<User>>()
    val resultData: LiveData<Result<User>>
        get() = _resultData

    private fun hasLoginData(): Boolean {
        return !userNameData.value.isNullOrEmpty() && !passwordData.value.isNullOrEmpty()
    }

    fun onLoginButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.login(userNameData.value!!, passwordData.value!!)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun onBackButtonClicked() {
        backButtonClickEvent.call()
    }

    fun onQuickLoginButtonClicked() {
        quickLoginClickEvent.call()
    }
    fun onForgotClick() {
        forgotClickEvent.call()
    }

    fun authenticateQuickLogin() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.quickLogin()
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        loginButtonEnabled.removeSource(userNameData)
        loginButtonEnabled.removeSource(passwordData)
    }
}