package com.gappscorp.aeps.library.ui.activity.bankdetails

import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.dialog.successAlert
import com.gappscorp.aeps.library.common.navigation.navigateToBankListScreen
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsActivityEditBankDetailsBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class UpdateBankDetailsActivity : BaseActivity<AepsActivityEditBankDetailsBinding>() {

    override val viewModel: UpdateBankDetailsViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_edit_bank_details

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        viewModel.selectBankClickEvent.observe(this, Observer {
            navigateToBankListScreen(BankListType.BANK_LIST) { _, data ->
                viewModel.bankName.value = data?.bankName
                viewModel.bankCode.value = data?.bankCode
            }
        })

        viewModel.resultData.observe(this, Observer {
            when(it) {
                is Result.Success -> successAlert(it.data.message) {
                    onBackPressed()
                }
                is Result.Error -> errorAlert(it.error)
            }
        })

        viewModel.cancelClickEvent.observe(this, Observer {
            onBackPressed()
        })
    }
}