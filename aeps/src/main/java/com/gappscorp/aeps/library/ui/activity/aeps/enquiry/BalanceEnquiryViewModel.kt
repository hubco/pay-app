package com.gappscorp.aeps.library.ui.activity.aeps.enquiry

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.model.BiometricDevice
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.BalanceEnquiryRequest
import com.gappscorp.aeps.library.domain.network.response.BalanceEnquiryResponse
import com.gappscorp.aeps.library.domain.repository.AepsRepository
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class BalanceEnquiryViewModel(
    private val repository: SettingsRepository,
    private val aepsRepository: AepsRepository
) : ViewModel() {

    val settingsErrorEvent = SingleLiveEvent<Void>()
    val autoFillPhoneEvent = SingleLiveEvent<Void>()
    val autoFillAadhaarEvent = SingleLiveEvent<Void>()

    private val _deviceList = MutableLiveData<List<String>>()
    val deviceList: LiveData<List<String>>
        get() = _deviceList

    private val _deviceScanStatusData = MutableLiveData<Status>()
    val deviceScanStatusData: LiveData<Status>
        get() = _deviceScanStatusData

    private val _fingerprintScanStatusData = MutableLiveData<Status>()
    val fingerprintScanStatusData: LiveData<Status>
        get() = _fingerprintScanStatusData

    private val _dataSubmissionStatusData = MutableLiveData<Status>()
    val dataSubmissionStatusData: LiveData<Status>
        get() = _dataSubmissionStatusData

    private val _dataSubmissionResult = MutableLiveData<Result<BalanceEnquiryResponse>>()
    val dataSubmissionResult: LiveData<Result<BalanceEnquiryResponse>>
        get() = _dataSubmissionResult

    private val biometricDeviceList = mutableListOf<BiometricDevice>()
    val selectedDevice get() = biometricDeviceList.find { it.name.equals(deviceName.value, true) }

    val deviceName = MutableLiveData<String>()
    val phoneNumber = MutableLiveData<String>()
    val aadhaarNumber = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()
    val bankCode = MutableLiveData<String>()
    val fingerPrintScanData = MutableLiveData<String>()
    private val isFingerprintScanned = MutableLiveData<Boolean>()

    val hasLastUsedPhoneNumber = MutableLiveData<Boolean>()
    val hasLastUsedAadhaarNumber = MutableLiveData<Boolean>()

    val isDeviceOnline = MediatorLiveData<Boolean>().apply {
        addSource(deviceName) {
            value = false
            onDeviceScanCompleted(false)
        }
    }

    val selectDeviceClickEvent = SingleLiveEvent<Void>()
    val selectBankClickEvent = SingleLiveEvent<Void>()
    val deviceScanClickEvent = SingleLiveEvent<Void>()
    val fingerprintScanClickEvent = SingleLiveEvent<Void>()
    val latitude = MutableLiveData<String>()
    val longitude = MutableLiveData<String>()

    val isProceedButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(phoneNumber) {
            value = hasWithdrawalData()
        }
        addSource(aadhaarNumber) {
            value = hasWithdrawalData()
        }
        addSource(bankName) {
            value = hasWithdrawalData()
        }
        addSource(isDeviceOnline) {
            value = hasWithdrawalData()
        }
        addSource(isFingerprintScanned) {
            value = hasWithdrawalData()
        }
    }

    val request
        get() =
            BalanceEnquiryRequest(
                bankName = bankName.value,
                bankCode = bankCode.value,
                aadhaarNumber = aadhaarNumber.value,
                mobileNumber = phoneNumber.value,
                latitude = latitude.value,
                longitude = longitude.value,
                pidData = fingerPrintScanData.value,
                deviceType = deviceName.value?.toLowerCase(Locale.getDefault())
            )

    init {
        getDevicesList()
    }

    private fun hasWithdrawalData(): Boolean {
        return !phoneNumber.value.isNullOrEmpty()
                && !aadhaarNumber.value.isNullOrEmpty()
                && !bankName.value.isNullOrEmpty()
                && isDeviceOnline.value ?: false
                && isFingerprintScanned.value ?: false
    }

    private fun getDevicesList() {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.hasSettings()) {
                biometricDeviceList.addAll(repository.getBiometricDeviceList())
            } else {
                when (repository.getSettings()) {
                    is Result.Success -> biometricDeviceList.addAll(repository.getBiometricDeviceList())
                    is Result.Error -> settingsErrorEvent.call()
                }
            }
            _deviceList.postValue(biometricDeviceList.map { it.name })
            deviceName.value?.let {
                deviceName.postValue(it)
            }
        }
    }

    fun onSelectDeviceClicked() {
        selectDeviceClickEvent.call()
    }

    fun onAutoFillPhoneClicked() {
        autoFillPhoneEvent.call()
    }

    fun onAutoFillAadhaarClicked() {
        autoFillAadhaarEvent.call()
    }

    fun onSelectBankClicked() {
        selectBankClickEvent.call()
    }

    fun onDeviceScanClicked() {
        _deviceScanStatusData.value = Status.LOADING
        deviceScanClickEvent.call()
    }

    fun onDeviceScanCompleted(success: Boolean) {
        when (success) {
            true -> _deviceScanStatusData.value = Status.SUCCESS
            false -> _deviceScanStatusData.value = Status.ERROR
        }
    }

    fun onFingerprintScanClicked() {
        fingerprintScanClickEvent.call()
    }

    fun onFingerprintScanCompleted(success: Boolean) {
        when (success) {
            true -> _fingerprintScanStatusData.value = Status.SUCCESS
            false -> _fingerprintScanStatusData.value = Status.ERROR
        }
        isFingerprintScanned.value = success
    }

    fun onProceedButtonClicked() {
        viewModelScope.launch {
            _dataSubmissionStatusData.postValue(Status.LOADING)
            val result = aepsRepository.balanceEnquiry(request)
            _dataSubmissionResult.postValue(result)
            when (result) {
                is Result.Success -> {
                    val resultData = result.data
                    resultData.aadhaarNumber = aadhaarNumber.value
                    resultData.bankName = bankName.value
                    _dataSubmissionStatusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> _dataSubmissionStatusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        isDeviceOnline.removeSource(deviceName)
        isProceedButtonEnabled.removeSource(phoneNumber)
        isProceedButtonEnabled.removeSource(aadhaarNumber)
        isProceedButtonEnabled.removeSource(bankName)
        isProceedButtonEnabled.removeSource(isDeviceOnline)
        isProceedButtonEnabled.removeSource(isFingerprintScanned)
    }
}