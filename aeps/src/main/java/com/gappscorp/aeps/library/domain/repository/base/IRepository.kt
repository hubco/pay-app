package com.gappscorp.aeps.library.domain.repository.base

import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.extensions.readFileFromAssets
import com.gappscorp.aeps.library.data.network.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeoutException

interface IRepository {

    suspend fun <T> safeCall(
        offlineFilePath: String? = null,
        call: suspend () -> Response<T>
    ): Result<T> {
        return when (CoreApp.configuration.offlineMode) {
            true -> getOfflineResponse(offlineFilePath)
            false -> getOnlineResponse(call)
        }
    }

    private suspend fun <T> getOfflineResponse(offlineFilePath: String?): Result<T> {
        return if (offlineFilePath == null) {
            Result.Error(error = "Offline response file not found!!!")
        } else try {
            val data: T = Gson().fromJson(
                CoreApp.appContext.readFileFromAssets(offlineFilePath),
                object : TypeToken<T>() {}.type
            )
            Result.Success(data)
        } catch (ex: Exception) {
            Result.Error(error = ex.message)
        }
    }

    private suspend fun <T> getOnlineResponse(call: suspend () -> Response<T>): Result<T> {
        return try {
            val response = call.invoke()
            if (response.isSuccessful && response.body() != null) {
                Result.Success(response.body()!!)
            } else {
                var status: String? = null
                var errorId: String? = null
                var errorMessage: String? = null
                response.errorBody()?.string()?.let {
                    status = getErrorStatus(it)
                    errorId = getErrorId(it)
                    errorMessage = getErrorMessage(it)
                }

                if (errorMessage.equals("unauthorized", ignoreCase = true)) {
                    notifyUnAuthorizedLogin()
                    Result.Unauthorized(error = errorMessage)
                } else {
                    Result.Error(errorId, status, errorMessage)
                }
            }
        } catch (ex: Exception) {
            Timber.d(ex.localizedMessage)
            if (ex is UnknownHostException || ex is TimeoutException)
                Result.Error(error = "Network not available!")
            else
                Result.Error(error = ex.localizedMessage)
        }
    }

    private fun getErrorStatus(data: String): String? {
        return try {
            val jsonObject = JSONObject(data)
            if (jsonObject.has("status"))
                return jsonObject.getString("status")
            else
                null
        } catch (ex: JSONException) {
            Timber.d("Json parsing exception: $ex")
            null
        }
    }

    private fun getErrorId(data: String): String? {
        return try {
            val jsonObject = JSONObject(data)
            if (jsonObject.has("status")
                && (jsonObject.getString("status").equals("error", ignoreCase = true)
                        || jsonObject.getString("status").equals("failed", ignoreCase = true)

                        )
            )
                jsonObject.getString("tranx_id")
            else
                null
        } catch (ex: JSONException) {
            Timber.d("Json parsing exception: $ex")
            null
        }
    }

    private fun getErrorMessage(data: String): String? {
        val genericError = "Something went wrong, please try again!"
        return try {
            val jsonObject = JSONObject(data)
            if (jsonObject.has("status")
                && (jsonObject.getString("status")
                    .toLowerCase(Locale.getDefault()) == "error"
                        || jsonObject.getString("status")
                    .toLowerCase(Locale.getDefault()) == "failed")
            )
                jsonObject.getString("message")
            else
                genericError
        } catch (ex: JSONException) {
            Timber.d("Json parsing exception: $ex")
            ex.message
        }
    }

    private fun notifyUnAuthorizedLogin() {
        CoreApp.notifySessionExpired()
    }
}