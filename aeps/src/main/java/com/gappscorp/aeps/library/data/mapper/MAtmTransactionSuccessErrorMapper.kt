package com.gappscorp.aeps.library.data.mapper

import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.data.model.SuccessErrorData
import com.gappscorp.aeps.library.data.model.SuccessErrorItem
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.response.MAtmTransactionResponse
import org.koin.core.KoinComponent
import org.koin.core.inject

class MAtmTransactionSuccessErrorMapper : KoinComponent {

    private val sessionManager: SessionManager by inject()

    fun success(entity: MAtmTransactionResponse?): SuccessErrorData {
        val context = CoreApp.appContext
        val itemList = mutableListOf<SuccessErrorItem>().apply {
            add(
                SuccessErrorItem(
                    context.getString(R.string.label_withdrawal_amount), "${sessionManager.currency} ${entity?.amount}"
                )
            )
            add(
                SuccessErrorItem(
                    context.getString(R.string.available_balance_txt), "${sessionManager.currency} ${entity?.availableBalance}"
                )
            )
            add(SuccessErrorItem(context.getString(R.string.rrn_number_txt), entity?.bankRrnNumber))
            add(
                SuccessErrorItem(
                    context.getString(R.string.card_number_txt), entity?.cardNumber
                )
            )
            add(SuccessErrorItem(context.getString(R.string.label_mobile_number), entity?.mobileNo))
        }
        return SuccessErrorData(
            orderId = entity?.transactionId,
            amount = entity?.amount,
            message = entity?.message,
            itemList = itemList
        )
    }

    fun failure(error: Result.Error?,amount: String?): SuccessErrorData {
        val context = CoreApp.appContext
        val notApplicable = context.getString(R.string.not_applicable)

        return SuccessErrorData(
            orderId = error?.id ?: notApplicable,
            message = error?.error ?:notApplicable,
            amount = amount?:"0",
            itemList = mutableListOf()
        )
    }
}