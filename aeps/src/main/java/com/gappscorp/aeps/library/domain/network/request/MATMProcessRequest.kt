package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class MATMProcessRequest(
    @SerializedName("mobile_no")
    val mobileNumber: String?,
    @SerializedName("latitude")
    val latitude: String?,
    @SerializedName("longitude")
    val longitude: String?,
    @SerializedName("remarks")
    val remarks: String?,
    @SerializedName("amount")
    val amount: String?
)