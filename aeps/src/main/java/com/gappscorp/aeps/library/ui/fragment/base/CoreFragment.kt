package com.gappscorp.aeps.library.ui.fragment.base

import androidx.fragment.app.Fragment

abstract class CoreFragment : Fragment()