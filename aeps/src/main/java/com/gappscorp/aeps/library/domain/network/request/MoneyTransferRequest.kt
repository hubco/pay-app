package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class MoneyTransferRequest(
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("beneficiary_id")
    val beneficiaryId: String?,
    @SerializedName("amount")
    val amount: String?,
    @SerializedName("t_mode")
    val transferMode: String?,
    @SerializedName("remarks")
    val remarks: String?
)