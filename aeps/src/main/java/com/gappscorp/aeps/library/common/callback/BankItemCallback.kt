package com.gappscorp.aeps.library.common.callback

import com.gappscorp.aeps.library.data.model.BankInfo

interface BankItemCallback {
    fun onBankItemClicked(item: BankInfo)
}