package com.gappscorp.aeps.library.common.config

import com.gappscorp.aeps.library.BuildConfig

data class AepsConfig(
    var baseUrl: String = BuildConfig.BASE_AEPS_URL,
    var hasPoweredByInfo: Boolean = true,
    var backButtonEnabled: Boolean = true,
    var dbName : String = "AepsDB",
    var offlineMode: Boolean = false
)