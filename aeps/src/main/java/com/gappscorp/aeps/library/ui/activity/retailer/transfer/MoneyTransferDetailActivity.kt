package com.gappscorp.aeps.library.ui.activity.retailer.transfer

import android.os.Bundle
import android.widget.RadioButton
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.navigateForResult
import com.gappscorp.aeps.library.common.navigation.navigateToDashboard
import com.gappscorp.aeps.library.data.model.BeneficiaryData
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityMoneyTransferDetailBinding
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferVerifyResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.RetailBeneficiaryDetailActivity
import com.gappscorp.aeps.library.ui.fragment.retailer.success.MoneyTransferSuccessDialog
import com.gappscorp.aeps.library.ui.fragment.retailer.transfer.MoneyTransferOtpDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MoneyTransferDetailActivity : BaseActivity<AepsActivityMoneyTransferDetailBinding>(),
    MoneyTransferOtpDialog.IDialogFinished, MoneyTransferSuccessDialog.ISuccessDialogFinished {

    override val viewModel: MoneyTransferDetailViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_money_transfer_detail
    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val remitterResponse: PhoneRemitterResponse? =
            intent.getParcelableExtra(Constants.EXTRA_REMITTER_RESPONSE)
        viewModel.remitterResponse.value = remitterResponse
        viewModel.userName.value = getRemitterName(remitterResponse)

        dataBinding.rgPayout.setOnCheckedChangeListener { group, checkedId ->
            // get radio button using id
            viewModel.modeType.value = group.findViewById<RadioButton>(checkedId).text as String?
        }
    }

    private fun getRemitterName(remitterResponse: PhoneRemitterResponse?): String? {
        return if (!remitterResponse?.remitterInfo?.firstName.isNullOrBlank() && !remitterResponse?.remitterInfo?.lastName.isNullOrBlank())
            remitterResponse?.remitterInfo?.firstName + ' ' + remitterResponse?.remitterInfo?.lastName
        else if (!remitterResponse?.remitterInfo?.firstName.isNullOrBlank())
            remitterResponse?.remitterInfo?.firstName
        else if (!remitterResponse?.remitterInfo?.lastName.isNullOrBlank())
            remitterResponse?.remitterInfo?.lastName
        else ""
    }

    override fun onResume() {
        super.onResume()
        viewModel.getTransferOption()
    }

    override fun registerObserver() {
        viewModel.settingsErrorEvent.observe(this, Observer {
            errorAlert(getString(R.string.error_getting_settings_data)) {
                onBackPressed()
            }
        })
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.addOrSelectBeneficiaryDetail.observe(this, Observer {
            // start activity for beneficiary add/select
            val bundle = Bundle()
            bundle.putParcelable(
                Constants.EXTRA_REMITTER_RESPONSE,
                viewModel.remitterResponse.value
            )
            navigateForResult<RetailBeneficiaryDetailActivity, BeneficiaryData>(
                bundle,
                80
            ) { success, data ->
                if (success) {
                    viewModel.remitterResponse.value = data?.phoneRemitterResponse
                    viewModel.addBeneficiaryResponse.value = data?.addBeneficiaryResponse
                    if (data?.addBeneficiaryResponse != null && !data.addBeneficiaryResponse.beneficiaryInfo?.isListingFlow!!) {
                        viewModel.remitterResponse.value?.beneficiaryList?.add(
                            0,
                            viewModel.mappedBeneficiaryData(data.addBeneficiaryResponse)
                        )
                    }
                }
            }
            // clear last selected beneficiary
            viewModel.addBeneficiaryResponse.postValue(null)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    val ft = supportFragmentManager.beginTransaction()
                    val moneyTransferOtp = MoneyTransferOtpDialog.newInstance(
                        viewModel.addBeneficiaryResponse.value?.beneficiaryInfo?.slug!!,
                        viewModel.remitterResponse.value?.remitterInfo?.slug!!,
                        it.data,
                        viewModel.remitterResponse.value!!,
                        viewModel.addBeneficiaryResponse.value!!,
                        viewModel.modeType.value!!
                    )
                    moneyTransferOtp.setDialogListener(this)
                    moneyTransferOtp.show(ft, MoneyTransferOtpDialog.TAG)
                }
                is Result.Error -> errorAlert(it.error)
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        (application as CoreApp).onLeavingAeps(isTaskRoot)
    }

    override fun onDialogFinished(
        value: PhoneRemitterResponse?,
        data: MoneyTransferVerifyResponse,
        beneficiary: AddBeneficiaryResponse?,
        modeType: String?
    ) {
        // if transaction is successful, show success custom view,
        // otherwise show pending message
        val ft = supportFragmentManager.beginTransaction()
        val moneyTransferOtp =
            MoneyTransferSuccessDialog.newInstance(value, data, beneficiary, modeType)
        moneyTransferOtp.setDialogListener(this)
        moneyTransferOtp.show(ft, MoneyTransferOtpDialog.TAG)
    }

    override fun onDialogFinished() {
        navigateToDashboard()
    }
}