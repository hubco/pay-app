package com.gappscorp.aeps.library.ui.fragment.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.BR
import com.gappscorp.aeps.library.R

abstract class BaseDialogFragment<VDB : ViewDataBinding> : DialogFragment() {

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutRes: Int

    protected lateinit var dataBinding: VDB
    protected val bindingVariable = BR.viewModel

    protected open fun registerObserver() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Pick a style based on the num.
        val theme = R.style.Theme_AppCompat_Dialog_MinWidth
        setStyle(STYLE_NO_FRAME, theme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate<VDB>(inflater, layoutRes, container, false).apply {
            lifecycleOwner = this@BaseDialogFragment
            setVariable(bindingVariable, viewModel)
            executePendingBindings()
        }
        return dataBinding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register required observers
        registerObserver()
    }



}