package com.gappscorp.aeps.library.ui.fragment.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.BR

abstract class BaseFragment<VM : ViewModel, VDB : ViewDataBinding> : CoreFragment() {

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutRes: Int

    protected lateinit var dataBinding: VDB
    protected val bindingVariable = BR.viewModel

    protected open fun registerObserver() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate<VDB>(inflater, layoutRes, container, false).apply {
            lifecycleOwner = this@BaseFragment
            setVariable(bindingVariable, viewModel)
            executePendingBindings()
        }
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register required observers
        registerObserver()
    }
}