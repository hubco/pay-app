package com.gappscorp.aeps.library.ui.activity.otp

import `in`.aabhasjindal.otptextview.OTPListener
import android.os.Bundle
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.dialog.infoAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateUpTo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityOtpScreenBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import kotlinx.android.synthetic.main.aeps_activity_otp_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class OtpVerificationActivity : BaseActivity<AepsActivityOtpScreenBinding>() {

    override val viewModel: OtpVerificationViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_otp_screen

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // get otp verification data
        viewModel.otpVerificationData.value =
            intent.getParcelableExtra(Constants.EXTRA_OTP_VERIFICATION_DATA)!!

        // register otp event listener
        otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                viewModel.otpCode.value = ""
            }

            override fun onOTPComplete(otp: String) {
                viewModel.otpCode.value = otp
            }
        }
    }

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when(it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    infoAlert(it.data.message) {
                        navigateUpTo(Routes.LOGIN_SCREEN)
                    }
                }
                is Result.Error -> {
                    // clear otp
                    otpView.setOTP("")
                    // show error message
                    errorAlert(it.error)
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateUpTo(Routes.LOGIN_SCREEN)
    }
}