package com.gappscorp.aeps.library.domain.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoneyTransferResponse(
    @SerializedName("status")
    var status: String,
    @SerializedName("message")
    val message: String?,
    @SerializedName("otp_prefix")
    val otpPrefix: String?,
    @SerializedName("t_id")
    val transferId: String?
) : Parcelable