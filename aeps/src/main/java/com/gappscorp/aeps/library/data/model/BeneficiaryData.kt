package com.gappscorp.aeps.library.data.model

import android.os.Parcelable
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
class BeneficiaryData(
    val addBeneficiaryResponse: AddBeneficiaryResponse? = null,
    val phoneRemitterResponse: PhoneRemitterResponse? = null
) : Parcelable