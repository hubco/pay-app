package com.gappscorp.aeps.library.ui.activity.result

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.model.SuccessErrorData

class SuccessErrorViewModel : ViewModel() {

    val data = MutableLiveData<SuccessErrorData>()
    val isSuccessData = MutableLiveData<Boolean>()

    val iconId = MediatorLiveData<Int>().apply {
        addSource(isSuccessData) {
            value = if(it) R.drawable.aeps_ic_success_icon else R.drawable.aeps_ic_error_icon
        }
    }

    val backgroundResId = MediatorLiveData<Int>().apply {
        addSource(isSuccessData) {
            value = if(it) R.drawable.aeps_bg_success else R.drawable.aeps_bg_error
        }
    }

    val bankName = MutableLiveData<String>()
    val aadhaarNumber = MutableLiveData<String>()
    val availableBalance = MutableLiveData<String>()
    val bankRnnNumber = MutableLiveData<String>()
    val transactionId = MutableLiveData<String>()
    val resourceId = MutableLiveData<Int>()
    val statusMessage = MutableLiveData<String>()
//    val backgroundResId = MutableLiveData<Drawable>()
    val isRetry = MutableLiveData<Boolean>()

    val isBankName = MutableLiveData<Boolean>()
    val isBalance = MutableLiveData<Boolean>()
    val isTopBalance = MutableLiveData<Boolean>()
    val isAadhaar = MutableLiveData<Boolean>()
    val isRnnNumber = MutableLiveData<Boolean>()
    val isTransactionId = MutableLiveData<Boolean>()

    val copyClickEvent = SingleLiveEvent<Void>()
    val shareClickEvent = SingleLiveEvent<Void>()
    val retryClickEvent = SingleLiveEvent<Void>()


    fun onCopyClick() {
        copyClickEvent.call()
    }

    fun onShareClick() {
        shareClickEvent.call()
    }

    fun onRetryClick() {
        retryClickEvent.call()
    }

    override fun onCleared() {
        super.onCleared()
        iconId.removeSource(isSuccessData)
        backgroundResId.removeSource(isSuccessData)
    }
}