package com.gappscorp.aeps.library.ui.fragment.retailer.success

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferVerifyResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse

class MoneyTransferSuccessViewModel : ViewModel() {
    val moneyTransferResponse = MutableLiveData<MoneyTransferVerifyResponse>()
    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()
    val beneficiaryInfo = MutableLiveData<AddBeneficiaryResponse>()
    val modeType = MutableLiveData<String>()
    val status = MutableLiveData<String>()

    val isBankAvailable = MediatorLiveData<Boolean>().apply {
        value = true
        addSource( beneficiaryInfo) {
            value = beneficiaryInfo.value?.beneficiaryInfo?.bankName?.isNotEmpty()
        }
    }

    val isAccountAvailable = MediatorLiveData<Boolean>().apply {
        value = true
        addSource( beneficiaryInfo) {
            value = beneficiaryInfo.value?.beneficiaryInfo?.accountNumber?.isNotEmpty()
        }
    }

    val isBeneficiaryAvailable = MediatorLiveData<Boolean>().apply {
        value = true
        addSource( beneficiaryInfo) {
            value = beneficiaryInfo.value?.beneficiaryInfo?.beneficiaryName?.isNotEmpty()
        }
    }

    val closeClickEvent = SingleLiveEvent<Void>()
    val shareClickEvent = SingleLiveEvent<Void>()

    fun onCloseDialog(){
        closeClickEvent.call()
    }

    fun onShareClick() {
        shareClickEvent.call()
    }

    override fun onCleared() {
        super.onCleared()
        isBankAvailable.removeSource(beneficiaryInfo)
        isAccountAvailable.removeSource(beneficiaryInfo)
        isBeneficiaryAvailable.removeSource(beneficiaryInfo)
    }
}