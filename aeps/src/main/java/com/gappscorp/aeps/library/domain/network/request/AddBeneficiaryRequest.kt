package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class AddBeneficiaryRequest(
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("name")
    val beneficiaryName: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("ifsc_code")
    val ifscCode: String?,
    @SerializedName("account_no")
    val accountNumber: String?,
    @SerializedName("mobile_no")
    val mobileNumber: String?
)