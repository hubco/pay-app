package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.callback.BeneficiaryItemCallback
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.confirmAlert
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.data.model.BeneficiaryData
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsFragmentBeneficiaryListBinding
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.base.BaseBeneficiaryFragment
import com.gappscorp.aeps.library.ui.fragment.retailer.beneficiary.RemoveBeneficiaryOtpDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class BeneficiaryListFragment :
    BaseBeneficiaryFragment<BeneficiaryListViewModel, AepsFragmentBeneficiaryListBinding>(),
    RemoveBeneficiaryOtpDialog.IDialogFinished {

    override val viewModel: BeneficiaryListViewModel by viewModel()

    override val layoutRes = R.layout.aeps_fragment_beneficiary_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.remitterResponse.value =
            arguments?.getParcelable(Constants.EXTRA_REMITTER_RESPONSE)

        viewModel.isDataAvailable.value = !viewModel.remitterResponse.value?.beneficiaryList.isNullOrEmpty()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.callback = closeButtonClick
    }

    override fun registerObserver() {
        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    val ft = requireActivity().supportFragmentManager.beginTransaction()
                    val removeFragmentOtp = RemoveBeneficiaryOtpDialog.newInstance(
                        viewModel.beneficiaryInfo.value!!, viewModel.currentPosition.value!!,
                        viewModel.remitterResponse.value?.remitterInfo?.slug!!, it.data.message
                    )
                    removeFragmentOtp.setDialogListener(this)
                    removeFragmentOtp.show(ft, RemoveBeneficiaryOtpDialog.TAG)
                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })
    }

    private val closeButtonClick = object : BeneficiaryItemCallback {
        override fun onBeneficiaryItemClick(item: BeneficiaryInfo, position: Int) {
            requireActivity().confirmAlert(getString(R.string.alert_confirmation_msg)) {
                viewModel.removeBeneficiary(item, position)
            }
        }

        override fun onBeneficiaryRowClick(item: BeneficiaryInfo) {
            requireActivity().setResult(
                BeneficiaryData(
                    addBeneficiaryResponse = viewModel.mappedBeneficiaryData(item),
                    phoneRemitterResponse = viewModel.remitterResponse.value
                )
            )
        }
    }

    companion object {
        val TAG = BeneficiaryListFragment::class.java.simpleName
        fun newInstance(remitterResponse: PhoneRemitterResponse?): BeneficiaryListFragment =
            BeneficiaryListFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(Constants.EXTRA_REMITTER_RESPONSE, remitterResponse)
                }
            }
    }

    override fun onDialogFinished(position: Int) {
        val modifiedBeneficiaryList = viewModel.remitterResponse.value?.beneficiaryList
        modifiedBeneficiaryList?.removeAt(position)
        viewModel.remitterResponse.value = viewModel.remitterResponse.value?.apply {
            beneficiaryList = modifiedBeneficiaryList!!
        }

        viewModel.isDataAvailable.value = modifiedBeneficiaryList?.isNotEmpty()
    }

    override fun getPhoneRemitterResponse(): PhoneRemitterResponse? {
        return viewModel.remitterResponse.value
    }
}