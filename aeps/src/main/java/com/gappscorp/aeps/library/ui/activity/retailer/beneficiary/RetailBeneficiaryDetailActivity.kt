package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.common.navigation.setResult
import com.gappscorp.aeps.library.data.model.BeneficiaryData
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityBeneficiaryDetailBinding
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import com.gappscorp.aeps.library.ui.fragment.retailer.otp.RegisterOtpVerificationDialog
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.aeps_activity_beneficiary_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RetailBeneficiaryDetailActivity : BaseActivity<AepsActivityBeneficiaryDetailBinding>() {

    override val viewModel: RetailBeneficiaryDetailViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_beneficiary_detail
    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.remitterResponse.value =
            intent.getParcelableExtra(Constants.EXTRA_REMITTER_RESPONSE)!!

        val fragmentManager: FragmentManager = supportFragmentManager

        val adapter =
            BeneficiaryPagerAdapter(this, fragmentManager, viewModel.remitterResponse.value)
        viewpager.adapter = adapter
        tabLayout.setupWithViewPager(viewpager)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewpager.currentItem = tab!!.position
            }
        })
    }

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.forgotClickEvent.observe(this, Observer {
            navigateTo(Routes.FORGET_PASSWORD_SCREEN)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    registerOtpVerificationScreen(it.data)

                }
                is Result.Error -> errorAlert(it.error)
            }
        })
    }

    override fun onBackPressed() {
        val data = BeneficiaryData(
            phoneRemitterResponse = (viewpager?.adapter as BeneficiaryPagerAdapter).getItem(viewpager.currentItem)
                .getPhoneRemitterResponse()
        )
        setResult(data)
    }

    private fun registerOtpVerificationScreen(registerResponse: PhoneRemitterResponse?) {
        val ft = supportFragmentManager.beginTransaction()
        val newFragment = RegisterOtpVerificationDialog.newInstance(registerResponse)
        newFragment.show(ft, RegisterOtpVerificationDialog.TAG)
    }
}