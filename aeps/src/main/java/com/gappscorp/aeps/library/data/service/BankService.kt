package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.domain.network.request.BankDetailsRequest
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BankService {

    @POST("api/retailer-api/v1/user/change-bank-info")
    suspend fun updateBankDetails(@Body request: BankDetailsRequest) : Response<GenericResponse>
}