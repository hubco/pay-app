package com.gappscorp.aeps.library.data.mapper

import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.session.SessionManager
import com.gappscorp.aeps.library.data.model.SuccessErrorData
import com.gappscorp.aeps.library.data.model.SuccessErrorItem
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.response.PayoutResponse
import org.koin.core.KoinComponent
import org.koin.core.inject

class PayoutSuccessErrorMapper() : KoinComponent {

    private val sessionManager: SessionManager by inject()

    fun success(entity: PayoutResponse?): SuccessErrorData {
        val context = CoreApp.appContext
        val itemList = mutableListOf<SuccessErrorItem>().apply {
            add(
                SuccessErrorItem(
                    context.getString(R.string.label_amount),
                    "${sessionManager.currency} ${entity?.amount}"
                )
            )
            add(
                SuccessErrorItem(
                    context.getString(R.string.label_charges),
                    "${sessionManager.currency} ${entity?.charges}"
                )
            )
            add(
                SuccessErrorItem(
                    context.getString(R.string.label_total_amount),
                    "${sessionManager.currency} ${entity?.totalAmount}"
                )
            )
        }
        return SuccessErrorData(
            orderId = entity?.transactionId,
            amount = entity?.totalAmount,
            message = entity?.message,
            itemList = itemList
        )
    }

    fun failure(error: Result.Error?): SuccessErrorData {
        val context = CoreApp.appContext
        val notApplicable = context.getString(R.string.not_applicable)
        val itemList = mutableListOf<SuccessErrorItem>().apply {
            add(SuccessErrorItem(context.getString(R.string.label_amount), notApplicable))
            add(SuccessErrorItem(context.getString(R.string.label_charges), notApplicable))
            add(SuccessErrorItem(context.getString(R.string.label_total_amount), notApplicable))
        }
        return SuccessErrorData(
            orderId = error?.id ?: notApplicable,
            amount = notApplicable,
            message = error?.error,
            itemList = itemList
        )
    }
}