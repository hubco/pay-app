package com.gappscorp.aeps.library.domain.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.domain.network.request.BalanceEnquiryRequest
import com.gappscorp.aeps.library.domain.network.request.CashWithdrawalRequest
import com.gappscorp.aeps.library.domain.network.request.MATMProcessRequest
import com.gappscorp.aeps.library.domain.network.request.MiniStatementRequest
import com.gappscorp.aeps.library.domain.network.response.*
import com.gappscorp.aeps.library.domain.repository.base.IRepository
import okhttp3.RequestBody

interface AepsRepository : IRepository {
    suspend fun cashWithdrawal(request: CashWithdrawalRequest) : Result<CashWithdrawalResponse>
    suspend fun balanceEnquiry(request: BalanceEnquiryRequest) : Result<BalanceEnquiryResponse>
    suspend fun miniStatement(request: MiniStatementRequest) : Result<MiniStatementResponse>
    suspend fun sendMATMBeData(request: MATMProcessRequest): Result<MAtmBEResponse>
    suspend fun processMAtmTransation(response: RequestBody?): Result<MAtmTransactionResponse>
}