package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.fragment.base.BaseFragment

abstract class BaseBeneficiaryFragment<VM: ViewModel, VDB: ViewDataBinding> : BaseFragment<VM, VDB>() {

    abstract fun getPhoneRemitterResponse() : PhoneRemitterResponse?
}