package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.data.model.User
import com.gappscorp.aeps.library.data.model.UserDetail
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.data.model.KYCResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface UserService {

    @POST("/api/sign-in")
    @FormUrlEncoded
    suspend fun login(
        @Field("username") userName: String,
        @Field("password") password: String
    ): Response<User>

    @POST("api/sign-in-biometric")
    suspend fun quickLogin(): Response<User>

    @POST("/api/forget-password")
    @FormUrlEncoded
    suspend fun forgetPassword(@Field("username") userName: String): Response<OtpVerificationResponse>

    @POST("/api/forget-password")
    @FormUrlEncoded
    suspend fun forgetUsername(@Field("username") userName: String): Response<OtpVerificationResponse>

    @POST("api/retailer-api/v1/user/change-password")
    @FormUrlEncoded
    suspend fun changePassword(@Field("password") oldPassword: String, @Field("new_password") newPassword: String): Response<GenericResponse>

    @POST("api/forget-password-verify-otp")
    @FormUrlEncoded
    suspend fun verifyOtp(
        @Field("username") userName: String,
        @Field("t_id") otpId: String,
        @Field("otp") otpCode: String
    ): Response<GenericResponse>

    @GET("api/retailer-api/v1/user-info")
    suspend fun userDetails(): Response<UserDetail>

    @POST("api/retailer-api/v1/kyc/send-otp")
    @FormUrlEncoded
    suspend fun sendAdharOtp(
        @Field("aadhaar_no") adhaarNo: String
    ) : Response<KYCResponse>

    @POST("api/retailer-api/v1/kyc/submit-otp")
    @FormUrlEncoded
    suspend fun submitAadhaarOtp(
        @Field("t_id") t_id: String,
        @Field("otp") otp: String
    ) : Response<KYCResponse>

    @POST("api/retailer-api/v1/kyc/resend-otp")
    @FormUrlEncoded
    suspend fun resendAadhaarOtp(
        @Field("t_id") t_id: String
    ) : Response<KYCResponse>

    @POST("api/retailer-api/v1/kyc/submit-biometric")
    @FormUrlEncoded
    suspend fun submitBiometric(
        @Field("t_id") tId: String,
        @Field("device_type") deviceType: String,
        @Field("txtPidData") txtPidData: String
    ) : Response<KYCResponse>
}