package com.gappscorp.aeps.library.ui.fragment.retailer

import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateTo
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsBottomSheetVerifyModelBinding
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.ui.fragment.base.BaseBottomSheetFragment
import com.gappscorp.aeps.library.ui.fragment.retailer.otp.RegisterOtpVerificationDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class PhoneVerificationBottomSheet : BaseBottomSheetFragment<AepsBottomSheetVerifyModelBinding>() {

    override val viewModel: PhoneVerificationViewModel by viewModel()

    override val layoutRes = R.layout.aeps_bottom_sheet_verify_model

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> activity?.hideKeyboard()
                else -> Timber.d(it.name)
            }
        })

        viewModel.closeScreenEvent.observe(this, Observer {
            dismiss()
        })

        viewModel.onRegisterEvent.observe(this, Observer {
            dismiss()
            navigateTo(Routes.RETAIL_REGISTER_SCREEN)
        })


        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    dismiss()
                    if (it.data.remitterInfo == null) {
                        navigateTo(Routes.RETAIL_REGISTER_SCREEN, HashMap<String, String?>().apply {
                            put(Constants.EXTRA_PHONE_NUMBER, viewModel.phoneNumberData.value)
                        })
                    } else {
                        if (it.data.remitterInfo!!.isVerified!!) {
                            navigateTo(
                                Routes.MONEY_TRANSFER_SCREEN,
                                mutableMapOf<String, PhoneRemitterResponse>().apply {
                                    put(Constants.EXTRA_REMITTER_RESPONSE, it.data)
                                }
                            )
                        } else {
                            registerOtpVerificationScreen(it.data)
                        }
                    }

                }
                is Result.Error -> context?.errorAlert(it.error)
            }
        })
    }

    private fun registerOtpVerificationScreen(registerResponse: PhoneRemitterResponse?) {
        val ft = requireActivity().supportFragmentManager.beginTransaction()
        val newFragment = RegisterOtpVerificationDialog.newInstance(registerResponse)
        newFragment.show(ft, RegisterOtpVerificationDialog.TAG)
    }


    companion object {
        val TAG = PhoneVerificationBottomSheet::class.java.simpleName
        fun newInstance(): PhoneVerificationBottomSheet =
            PhoneVerificationBottomSheet().apply {
            }
    }
}