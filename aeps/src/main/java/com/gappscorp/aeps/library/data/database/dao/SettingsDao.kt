package com.gappscorp.aeps.library.data.database.dao

import androidx.room.*
import com.gappscorp.aeps.library.data.model.Settings

@Dao
interface SettingsDao {

    @Transaction
    suspend fun saveSettings(settings: Settings) {
        deleteSettings()
        insertSettings(settings)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSettings(settings: Settings)

    @Query("DELETE FROM Settings")
    suspend fun deleteSettings()

    @Query("SELECT * from Settings")
    fun getSettings(): Settings

}