package com.gappscorp.aeps.library.data.service

import com.gappscorp.aeps.library.data.model.Settings
import retrofit2.Response
import retrofit2.http.GET

interface SettingsService {

    @GET("api/retailer-api/v1/app-settings")
    suspend fun getSettings() : Response<Settings>
}