package com.gappscorp.aeps.library.ui.fragment.retailer.transfer

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.VerifyMoneyTransferRequest
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferResponse
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferVerifyResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class MoneyTransferOtpViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val beneficiaryId = MutableLiveData<String>()
    val otpCode = MutableLiveData<String>()
    val remitterId = MutableLiveData<String>()
    val moneyTransferResponse = MutableLiveData<MoneyTransferResponse>()
    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()
    val beneficiaryInfo = MutableLiveData<AddBeneficiaryResponse>()
    val modeType = MutableLiveData<String>()

    val isTimerEnabled = MutableLiveData<Boolean>()

    private val _statusData = MutableLiveData<Status>()

    val statusData: LiveData<Status>
        get() = _statusData

    private val _verifyStatusData = MutableLiveData<Status>()

    val verifyStatusData: LiveData<Status>
        get() = _verifyStatusData

    private val _verifyTransferData = MutableLiveData<Result<MoneyTransferVerifyResponse>>()

    val verifyTransferData: LiveData<Result<MoneyTransferVerifyResponse>>
        get() = _verifyTransferData

    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(otpCode) {
            value = !otpCode.value.isNullOrEmpty()
        }
    }
    val closeClickEvent = SingleLiveEvent<Void>()


    private val _resendResultData = MutableLiveData<Result<MoneyTransferResponse>>()

    val resendResultData: LiveData<Result<MoneyTransferResponse>>
        get() = _resendResultData


    fun onCloseDialog(){
        closeClickEvent.call()
    }

    fun onSubmitButtonClicked()  {
        viewModelScope.launch {
            _verifyStatusData.postValue(Status.LOADING)
            val result = retailerRepository.verifyMoneyTransfer(
                VerifyMoneyTransferRequest(
                    remitterId = remitterId.value,
                    beneficiaryId = beneficiaryId.value,
                    otp = otpCode.value,
                    transferId = moneyTransferResponse.value?.transferId
                )
            )
            _verifyTransferData.postValue(result)
            when (result) {
                is Result.Success ->{
                    _verifyStatusData.postValue(Status.SUCCESS)
                }
                is Result.Error ->{
                    _verifyStatusData.postValue(Status.ERROR)
                }
            }
        }
    }


    fun onResendOtpClick(){
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.resetMoneyTransferOtp(
                VerifyMoneyTransferRequest(
                    beneficiaryId = beneficiaryId.value,
                    remitterId = remitterId.value,
                    transferId = moneyTransferResponse.value?.transferId,
                    otp = null
                )
            )
            _resendResultData.postValue(result)
            when (result) {
                is Result.Success ->{
                    moneyTransferResponse.value = result.data
                    _statusData.postValue(Status.SUCCESS)
                }
                is Result.Error ->{
                    _statusData.postValue(Status.ERROR)
                }
            }
        }
    }



    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(otpCode)
    }

}