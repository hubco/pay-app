package com.gappscorp.aeps.library.common.session

import android.content.Context
import android.content.SharedPreferences
import com.gappscorp.aeps.library.common.preference.Preferences
import com.gappscorp.aeps.library.common.preference.Preferences.get
import com.gappscorp.aeps.library.common.preference.Preferences.set
import com.gappscorp.aeps.library.data.model.*

private const val IS_FIRST_LAUNCH = "pref_is_first_launch"
private const val LOGGED_IN = "pref_logged_in"
private const val BIOMETRIC_LOGIN_ENABLED = "pref_biometric_login_status"
private const val USER_ID = "pref_user_id"
private const val USER_NAME = "pref_user_name"
private const val USER_AVATAR = "pref_user_avatar"
private const val USER_REFERRAL_CODE = "pref_user_referral_code"
private const val USER_TOKEN = "pref_user_phone"
private const val USER_STATUS = "pref_user_status"
private const val LAST_USED_DEVICE = "pref_last_used_device"
private const val USER_CURRENCY = "pref_user_currency"
private const val USER_AEPS_ENABLED = "pref_aeps_status"
private const val USER_AEPS_BE_ENABLED = "pref_aeps_be_status"
private const val USER_AEPS_CD_ENABLED = "pref_aeps_cd_status"
private const val USER_AEPS_CW_ENABLED = "pref_aeps_cw_status"
private const val USER_AEPS_MS_ENABLED = "pref_aeps_ms_status"
private const val USER_DMT_ENABLED = "pref_dmt_status"
private const val USER_MATM_ENABLED = "pref_matm_status"
private const val MATM_BE_ENABLED = "pref_matm_be_status"
private const val MATM_CW_ENABLED = "pref_matm_cw_status"

private const val LAST_USED_PHONE_NUMBER = "pref_last_used_phone_number"
private const val LAST_USED_AADHAAR_NUMBER = "pref_last_used_aadhaar_number"

class SessionManager(context: Context) {
    private val prefs: SharedPreferences = Preferences.defaultPrefs(context)

    val isAepsBeEnabled: Boolean get() = prefs[USER_AEPS_BE_ENABLED, false]
    val isAepsCdEnabled: Boolean get() = prefs[USER_AEPS_CD_ENABLED, false]
    val isAepsCwEnabled: Boolean get() = prefs[USER_AEPS_CW_ENABLED, false]
    val isAepsMsEnabled: Boolean get() = prefs[USER_AEPS_MS_ENABLED, false]
    val isDmtEnabled: Boolean get() = prefs[USER_DMT_ENABLED, false]
    val isMAtmBeEnable: Boolean get() = prefs[MATM_BE_ENABLED, false]
    val isMAtmCWEnable: Boolean get() = prefs[MATM_CW_ENABLED, false]
    val currency: String get() = prefs[USER_CURRENCY, "\u20B9"]

    val lastUsedPhoneNumber: String get() = prefs[LAST_USED_PHONE_NUMBER, ""]
    val lastUsedAadhaarNumber: String get() = prefs[LAST_USED_AADHAAR_NUMBER, ""]

    val lastUsedDevice: String get() = prefs[LAST_USED_DEVICE, ""]

    /**
     * Save User details on login
     * @param user - User model
     */
    fun saveUser(user: User) {
        prefs[USER_ID] = user.userId
        prefs[USER_NAME] = user.name
        prefs[USER_AVATAR] = user.avatarUrl
        prefs[USER_REFERRAL_CODE] = user.referralCode
        prefs[USER_TOKEN] = user.token
        prefs[USER_STATUS] = user.status
        prefs[LAST_USED_DEVICE] = user.lastUsedDevice
        prefs[USER_AEPS_ENABLED] = user.activeModules?.aeps?.active ?: false
        prefs[USER_AEPS_BE_ENABLED] = user.activeModules?.aeps?.rnfiAepsBe ?: false
        prefs[USER_AEPS_CD_ENABLED] = user.activeModules?.aeps?.rnfiAepsCd ?: false
        prefs[USER_AEPS_CW_ENABLED] = user.activeModules?.aeps?.rnfiAepsCw ?: false
        prefs[USER_AEPS_MS_ENABLED] = user.activeModules?.aeps?.rnfiAepsMs ?: false
        prefs[USER_DMT_ENABLED] = user.activeModules?.dmt?.active ?: false
        prefs[MATM_BE_ENABLED] = user.activeModules?.matm?.matmBe ?: false
        prefs[MATM_CW_ENABLED] = user.activeModules?.matm?.matmCw ?: false
        prefs[LOGGED_IN] = true
        prefs[IS_FIRST_LAUNCH] = false
    }

    /**
     * Check if user is logged in
     */
    fun isLoggedIn(): Boolean {
        return prefs[LOGGED_IN, false]
    }

    /**
     * Check if is first launch
     */
    fun isFirstLaunch(): Boolean {
        return prefs[IS_FIRST_LAUNCH, true]
    }

    /**
     * Get the logged in User ID
     */
    fun getUserId(): String {
        return prefs[USER_ID, ""]
    }

    /**
     * Get the logged in User token
     */
    fun getUserToken(): String {
        return prefs[USER_TOKEN, ""]
    }

    /**
     * Get the logged in User username
     */
    fun getUsername(): String {
        return prefs[USER_NAME, ""]
    }

    /**
     * Get the logged in User image
     */
    fun getUserAvatar(): String {
        return prefs[USER_AVATAR, ""]
    }

    /**
     * Get the logged in User
     */
    fun getUser(): User {
        return User(
            userId = prefs[USER_ID, ""],
            name = prefs[USER_NAME, ""],
            avatarUrl = prefs[USER_AVATAR, ""],
            token = prefs[USER_TOKEN, ""],
            referralCode = prefs[USER_REFERRAL_CODE, ""],
            status = prefs[USER_STATUS, ""],
            lastUsedDevice = prefs[LAST_USED_DEVICE, ""],
            activeModules = ActiveModules(
                aeps = Aeps(
                    active = prefs[USER_AEPS_ENABLED, true],
                    rnfiAepsBe = prefs[USER_AEPS_BE_ENABLED, false],
                    rnfiAepsCd = prefs[USER_AEPS_CD_ENABLED, false],
                    rnfiAepsCw = prefs[USER_AEPS_CW_ENABLED, false],
                    rnfiAepsMs = prefs[USER_AEPS_MS_ENABLED, false]
                ),
                dmt = Dmt(
                    active =  prefs[USER_DMT_ENABLED, true]
                ),
                matm = MAtm(
                    active = prefs[USER_MATM_ENABLED, true],
                    matmBe =  prefs[MATM_BE_ENABLED, false],
                    matmCw =  prefs[MATM_CW_ENABLED, false]
                )
            )
        )
    }

    /**
     * Update User details
     */
    fun update(key: String, value: String) {
        prefs[key] = value
    }

    fun setBiometricLogin(enable: Boolean) {
        prefs[BIOMETRIC_LOGIN_ENABLED] = enable
    }

    fun isBiometricLoginEnabled(): Boolean {
        return prefs[BIOMETRIC_LOGIN_ENABLED, false]
    }

    fun setCurrency(currency: String) {
        prefs[USER_CURRENCY] = currency
    }

    fun setLastUsedPhoneNumber(number: String?) {
        prefs[LAST_USED_PHONE_NUMBER] = number
    }

    fun setLastUsedAadhaarNumber(number: String?) {
        prefs[LAST_USED_AADHAAR_NUMBER] = number
    }

    /**
     * Update session as logout but keep user details
     * for quick login
     */
    fun logout() {
        prefs[LOGGED_IN] = false
    }

    /**
     * Function to clear prefs
     */
    fun clearSession() {
        prefs.edit().clear().apply()
    }
}