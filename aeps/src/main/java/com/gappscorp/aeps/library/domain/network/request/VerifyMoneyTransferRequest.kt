package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class VerifyMoneyTransferRequest(
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("beneficiary_id")
    val beneficiaryId: String?,
    @SerializedName("t_id")
    val transferId: String?,
    @SerializedName("otp")
    val otp: String?
)