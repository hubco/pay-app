package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.BankService
import com.gappscorp.aeps.library.domain.network.request.BankDetailsRequest
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.repository.BankRepository

class BankRepositoryImpl(private val service: BankService) : BankRepository {

    override suspend fun updateBankDetails(request: BankDetailsRequest): Result<GenericResponse> {
        return safeCall { service.updateBankDetails(request) }
    }
}