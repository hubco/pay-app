package com.gappscorp.aeps.library.ui.activity.retailer.transfer

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.MoneyTransferRequest
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo
import com.gappscorp.aeps.library.domain.network.response.MoneyTransferResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class MoneyTransferDetailViewModel(private val repository: RetailerRepository, private val settingsRepository: SettingsRepository) : ViewModel() {

    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()
    val addBeneficiaryResponse = MutableLiveData<AddBeneficiaryResponse>()

    var beneficiaryList : ArrayList<BeneficiaryInfo>? = ArrayList()

    val userName = MutableLiveData<String>()
    val amountData = MutableLiveData<String>()
    val remarkData = MutableLiveData<String>()
    val transferModeList = MutableLiveData<List<String>>()
    val modeType = MutableLiveData<String>()
    val settingsErrorEvent = SingleLiveEvent<Void>()

    fun getTransferOption() {
        viewModelScope.launch(Dispatchers.IO) {
            // load payout list
            if(settingsRepository.hasSettings()) {
                transferModeList.postValue(settingsRepository.getDmtTypes())
            } else {
                when(val result = settingsRepository.getSettings()) {
                    is Result.Success -> transferModeList.postValue(result.data.dmtTypes)
                    is Result.Error -> settingsErrorEvent.call()
                }
            }
        }
    }

    val submitButtonEnable = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(amountData) {
            value = hasMoneyData()
        }
        addSource(addBeneficiaryResponse) {
            value = hasMoneyData()
        }
    }

    val request
        get() =
            MoneyTransferRequest(
                remitterId = remitterResponse.value?.remitterInfo?.slug,
                amount = amountData.value,
                beneficiaryId = addBeneficiaryResponse.value?.beneficiaryInfo?.slug, //will be received after adding
                remarks = remarkData.value,
                transferMode = modeType.value?.toLowerCase(Locale.getDefault())
            )

    val addOrSelectBeneficiaryDetail = SingleLiveEvent<Void>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<MoneyTransferResponse>>()
    val resultData: LiveData<Result<MoneyTransferResponse>>
        get() = _resultData

    private fun hasMoneyData(): Boolean {
        return !amountData.value.isNullOrEmpty() && addBeneficiaryResponse.value != null
    }

    fun onSubmitButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.moneyTransfer(request)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun addOrSelectBeneficiaryDetail() {
        addOrSelectBeneficiaryDetail.call()
    }

    fun mappedBeneficiaryData(item: AddBeneficiaryResponse): BeneficiaryInfo {
        return  BeneficiaryInfo(
            beneficiaryId = item.beneficiaryInfo?.beneficiaryId,
            remitterId = item.beneficiaryInfo?.remitterId,
            beneficiaryName = item.beneficiaryInfo?.beneficiaryName,
            accountNumber = item.beneficiaryInfo?.accountNumber,
            ifscCode = item.beneficiaryInfo?.ifscCode,
            bankCode = item.beneficiaryInfo?.bankCode,
            mobileNumber = item.beneficiaryInfo?.mobileNumber,
            bankName = item.beneficiaryInfo?.bankName,
            balanceType = item.beneficiaryInfo?.balanceType,
            createdAt = item.beneficiaryInfo?.createdAt,
            isOtpVerified = item.beneficiaryInfo?.isOtpVerified,
            isVerified = item.beneficiaryInfo?.isVerified,
            refId = item.beneficiaryInfo?.refId,
            slug = item.beneficiaryInfo?.slug,
            updatedAt = item.beneficiaryInfo?.updatedAt
        )
    }



    override fun onCleared() {
        super.onCleared()
        submitButtonEnable.removeSource(amountData)
        submitButtonEnable.removeSource(addBeneficiaryResponse)
    }


}