package com.gappscorp.aeps.library.ui.activity.password

import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.toast
import com.gappscorp.aeps.library.common.extensions.toastInCenter
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.databinding.AepsActivityChangePasswordBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChangePasswordActivity : BaseActivity<AepsActivityChangePasswordBinding>() {

    override val viewModel: ChangePasswordViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_change_password

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        viewModel.passwordLengthFailureEvent.observe(this, Observer {
            toastInCenter(R.string.label_password_length_error)
        })

        viewModel.passwordMismatchEvent.observe(this, Observer {
            toastInCenter(R.string.label_password_mismatch_error)
        })

        viewModel.resultData.observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    toast(it.data.message!!)
                    onBackPressed()
                }
                is Result.Error -> errorAlert(it.error)
            }
        })
    }
}