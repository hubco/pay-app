package com.gappscorp.aeps.library.common.authentication

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import timber.log.Timber

object Biometric {

    val isAvailable: Boolean
        get() {
            val biometricManager = BiometricManager.from(CoreApp.appContext)
            return when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS,
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> true
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> false
                else -> false
            }
        }

    fun authenticate(activity: FragmentActivity, callback: (success: Boolean, error: CharSequence?) -> Unit) {
        BiometricPrompt(activity, ContextCompat.getMainExecutor(activity),
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Timber.d("Authentication error: $errString")
                    callback.invoke(false, errString)
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    callback.invoke(true, null)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    callback.invoke(false, null)
                }
            }).authenticate(
            BiometricPrompt.PromptInfo.Builder()
                .setTitle(activity.getString(R.string.label_quick_login))
                .setSubtitle(activity.getString(R.string.label_quick_login_info))
                .setDeviceCredentialAllowed(true)
                .build()
        )
    }
}