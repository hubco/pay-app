package com.gappscorp.aeps.library.ui.activity.retailer.beneficiary.fragment.add

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.AddBeneficiaryRequest
import com.gappscorp.aeps.library.domain.network.response.AddBeneficiaryResponse
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class AddBeneficiaryViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val remitterResponse = MutableLiveData<PhoneRemitterResponse>()

    val beneficiaryName = MutableLiveData<String>()
    val accountNumber = MutableLiveData<String>()
    val confirmAccountNumber = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()
    val bankCode = MutableLiveData<String>()
    val ifscCode = MutableLiveData<String>()

    val isTnCAccept = MutableLiveData<Boolean>()

    init {
        isTnCAccept.value = false
    }

    val isSubmitEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(beneficiaryName) {
            value = hasValidData()
        }
        addSource(accountNumber) {
            value = hasValidData()
        }
        addSource(confirmAccountNumber) {
            value = hasValidData()
        }
        addSource(bankName) {
            value = hasValidData()
        }
        addSource(ifscCode) {
            value = hasValidData()
        }
        addSource(isTnCAccept) {
            value = hasValidData()
        }
    }


    private fun hasValidData(): Boolean {
        return beneficiaryName.value?.isNotEmpty() ?: false
                && accountNumber.value?.isNotEmpty() ?: false
                && confirmAccountNumber().isEmpty()
                && bankName.value?.isNotEmpty() ?: false
                && ifscCode.value?.isNotEmpty() ?: false
                && isTnCAccept.value!!
    }


    val nameValidator = MediatorLiveData<String>().apply {
        value = Constants.ENTER_BENEFICIARY_NAME
        addSource(beneficiaryName) {
            value = beneficiaryName()
        }
    }

    val accountNumberValidator = MediatorLiveData<String>().apply {
        value = Constants.ENTER_ACCOUNT_NUMBER
        addSource(accountNumber) {
            value = accountNumber()
        }
    }

    val confirmAccountValidator = MediatorLiveData<String>().apply {
        value = ""//Constants.ENTER_CONFIRM_ACCOUNT_NUMBER
        addSource(accountNumber) {
            value = confirmAccountNumber()
        }
        addSource(confirmAccountNumber) {
            value = confirmAccountNumber()
        }
    }

    private fun beneficiaryName(): String {
        return if (beneficiaryName.value.isNullOrEmpty()) Constants.ENTER_BENEFICIARY_NAME else ""
    }

    private fun accountNumber(): String {
        return if (accountNumber.value.isNullOrEmpty()) Constants.ENTER_ACCOUNT_NUMBER else ""
    }

    private fun confirmAccountNumber(): String {
        return if (confirmAccountNumber.value.isNullOrEmpty()) {
            Constants.ENTER_CONFIRM_ACCOUNT_NUMBER
        } else if (!confirmAccountNumber.value.equals(accountNumber.value)) {
            Constants.ENTER_MATCH_ACCOUNT_NUMBER
        } else {
            ""
        }
    }

    private fun bankNameValid(): String {
        return if (bankName.value.isNullOrEmpty()) Constants.ENTER_BANK_NAME else ""
    }

    val addRequest
        get() =
            AddBeneficiaryRequest(
                remitterId = remitterResponse.value?.remitterInfo?.slug,
                mobileNumber = remitterResponse.value?.remitterInfo?.mobileNumber,
                bankCode = bankCode.value,
                ifscCode = ifscCode.value,
                accountNumber = accountNumber.value,
                beneficiaryName = beneficiaryName.value
            )

    fun onRegisterButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.addBeneficiary(addRequest)
            when (result) {
                is Result.Success -> {
                    _statusData.postValue(Status.SUCCESS)
                    result.data.beneficiaryInfo!!.bankName = bankName.value
                }
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
            _resultData.postValue(result)
        }
    }


    val selectBankClickEvent = SingleLiveEvent<Void>()

    private val _statusData = MutableLiveData<Status>()

    val statusData: LiveData<Status>
        get() = _statusData


    private val _resultData = MutableLiveData<Result<AddBeneficiaryResponse>>()

    val resultData: LiveData<Result<AddBeneficiaryResponse>>
        get() = _resultData

    fun onSelectBankClicked() {
        selectBankClickEvent.call()
    }


    override fun onCleared() {
        super.onCleared()
        isSubmitEnabled.removeSource(beneficiaryName)
        isSubmitEnabled.removeSource(accountNumber)
        isSubmitEnabled.removeSource(confirmAccountNumber)
        isSubmitEnabled.removeSource(bankName)
        isSubmitEnabled.removeSource(ifscCode)
        nameValidator.removeSource(beneficiaryName)
        accountNumberValidator.removeSource(accountNumber)
        confirmAccountValidator.removeSource(confirmAccountNumber)
    }
}