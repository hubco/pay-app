package com.gappscorp.aeps.library.ui.activity.result.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.data.model.SuccessErrorItem
import com.gappscorp.aeps.library.databinding.AepsLayoutSuccessErrorListItemBinding

class SuccessErrorAdapter(private val list: List<SuccessErrorItem>) :
    RecyclerView.Adapter<SuccessErrorAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<AepsLayoutSuccessErrorListItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.aeps_layout_success_error_list_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(
        val binding: AepsLayoutSuccessErrorListItemBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SuccessErrorItem) {
            binding.title = item.title
            binding.value = item.value
        }
    }
}