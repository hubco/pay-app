package com.gappscorp.aeps.library.ui.activity.forgot

import androidx.lifecycle.*
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.launch

class ForgetUsernameViewModel(private val repository: UserRepository) : ViewModel() {

    val mobileNoData = MutableLiveData<String>()
    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(mobileNoData) {
            value = !mobileNoData.value.isNullOrEmpty()
        }
    }

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<OtpVerificationResponse>>()
    val resultData: LiveData<Result<OtpVerificationResponse>>
        get() = _resultData

    fun onSubmitButtonClick() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = repository.forgetUsername(mobileNoData.value!!)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(mobileNoData)
    }
}