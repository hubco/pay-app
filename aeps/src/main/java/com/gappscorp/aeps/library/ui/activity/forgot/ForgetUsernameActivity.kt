package com.gappscorp.aeps.library.ui.activity.forgot

import androidx.lifecycle.Observer
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.common.extensions.hideKeyboard
import com.gappscorp.aeps.library.common.navigation.Routes
import com.gappscorp.aeps.library.common.navigation.navigateUpTo
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.databinding.AepsActivityForgetUsernameBinding
import com.gappscorp.aeps.library.ui.activity.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ForgetUsernameActivity : BaseActivity<AepsActivityForgetUsernameBinding>() {

    override val viewModel: ForgetUsernameViewModel by viewModel()
    override val layoutRes = R.layout.aeps_activity_forget_username

    override val toolBarId: Int = R.id.commonToolBar
    override val hasUpAction: Boolean = true

    override fun registerObserver() {
        viewModel.statusData.observe(this, Observer {
            when (it) {
                Status.LOADING -> hideKeyboard()
                else -> Timber.d(it.name)
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateUpTo(Routes.LOGIN_SCREEN)
    }
}