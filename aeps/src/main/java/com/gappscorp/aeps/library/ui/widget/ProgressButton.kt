package com.gappscorp.aeps.library.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.use
import androidx.core.view.isVisible
import com.gappscorp.aeps.library.R
import kotlinx.android.synthetic.main.aeps_view_progresss_button.view.*

class ProgressButton constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    private lateinit var btnText: CharSequence

    init {
        LayoutInflater.from(context).inflate(R.layout.aeps_view_progresss_button, this, true)
        context.obtainStyledAttributes(attrs, R.styleable.ProgressButton, 0, 0).use {
            btnText = it.getText(R.styleable.ProgressButton_android_text)
            btnAction.apply {
                isEnabled = it.getBoolean(R.styleable.ProgressButton_android_enabled, isEnabled)
                text = it.getText(R.styleable.ProgressButton_android_text)
            }
        }

        background = ContextCompat.getDrawable(context, R.drawable.aeps_selector_progress_button)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return btnAction.dispatchTouchEvent(ev)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        btnAction.isClickable = enabled
        btnAction.isSelected = enabled
    }

    fun setLoadingVisibility(loading: Boolean) {
        isClickable = !loading
        progressBar.isVisible = loading
        btnAction.isVisible = !loading
    }
}