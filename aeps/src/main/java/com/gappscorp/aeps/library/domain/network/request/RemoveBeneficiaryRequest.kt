package com.gappscorp.aeps.library.domain.network.request

import com.google.gson.annotations.SerializedName

data class RemoveBeneficiaryRequest(
    @SerializedName("remitter_id")
    val remitterId: String?,
    @SerializedName("beneficiary_id")
    val beneficiaryId: String?
)