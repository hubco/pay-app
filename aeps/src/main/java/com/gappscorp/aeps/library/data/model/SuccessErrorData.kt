package com.gappscorp.aeps.library.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SuccessErrorData(
    var orderId: String?, val amount: String?, val message: String?,
    val itemList: List<SuccessErrorItem>
) : Parcelable

@Parcelize
data class SuccessErrorItem(val title: String, val value: String?) : Parcelable