package com.gappscorp.aeps.library.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class UserDetail(
    @SerializedName("retailer_info")
    val retailerInfo: RetailerInfo?,
    @SerializedName("user_info")
    val userInfo: UserInfo?,
    @SerializedName("user_type")
    val userType: String?
)

@Entity
data class UserInfo(
    val avatar: String?,
    val designation: String?,
    val email: String?,
    val mobile_no: String?,
    val name: String?,
    @SerializedName("user_id")
    @PrimaryKey
    val userId: String,
    val username: String?
)

@Entity
data class RetailerInfo(
    @SerializedName("aadhaar_no")
    val aadhaarNo: String?,
    @SerializedName("account_no")
    val accountNo: String?,
    val address: String?,
    val age: String?,
    @SerializedName("available_balance")
    val availableBalance: String,
    val avatar: String?,
    val balance: String,
    @SerializedName("bank_name")
    val bankName: String?,
    @SerializedName("bank_code")
    val bankCode: String?,
    @SerializedName("beneficiary_name")
    val beneficiaryName: String?,
    val dob: String?,
    val email: String?,
    @SerializedName("enrollment_date")
    val enrollmentDate: String?,
    @SerializedName("father_name")
    val fatherName: String?,
    val gender: String?,
    @SerializedName("hold_balance")
    val holdBalance: String,
    @SerializedName("ifsc_code")
    val ifscCode: String?,
    @SerializedName("kyc_status")
    val kycStatus: String?,
    @SerializedName("mobile_no")
    val mobileNo: String?,
    val name: String?,
    @SerializedName("pan_no")
    val panNo: String?,
    val pincode: String?,
    @SerializedName("retailer_code")
    val retailerCode: String?,
    @SerializedName("retailer_id")
    @PrimaryKey
    val retailerId: String,
    @SerializedName("spouse_name")
    val spouseName: String?,
    val state: String?,
    @SerializedName("upi_id")
    val upiId: String?,
    @SerializedName("currency_format")
    val currency: String?,
    @SerializedName("is_bio_kyc")
    val isBioKyc: String?
)