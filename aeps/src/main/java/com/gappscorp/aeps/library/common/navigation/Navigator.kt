package com.gappscorp.aeps.library.common.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.afollestad.inlineactivityresult.startActivityForResult
import com.gappscorp.aeps.library.common.constant.Constants
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.data.model.State
import com.gappscorp.aeps.library.data.model.SuccessErrorData
import com.gappscorp.aeps.library.ui.activity.aeps.banklist.BankListActivity
import com.gappscorp.aeps.library.ui.activity.result.SuccessErrorActivity
import com.gappscorp.aeps.library.ui.activity.state.StateListActivity
import java.io.Serializable

const val EXTRA_ACTIVITY_RESULT = "extra_activity_result"

private lateinit var navigationMap: Map<String, Class<*>>

fun initializeNavigationMap() {
    if (!::navigationMap.isInitialized) {
        navigationMap = Routes.navigationMap
    }
}

fun Context.navigateToLogin() {
    navigateUpTo(Routes.LOGIN_SCREEN, true)
}

fun Activity.navigateToDashboard() {
    navigateUpTo(Routes.DASHBOARD_SCREEN, true)
}

fun Activity.navigateTo(target: String) {
    navigateTo(target, null)
}

fun Context.navigateUpTo(target: String, newTask: Boolean = false) {
    initializeNavigationMap()
    val clazz = navigationMap[target]
        ?: throw NullPointerException("$target is not registered in navigation map!")

    val intent = Intent(this, clazz).apply {
        if (newTask) {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        } else {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
    }
    startActivity(intent)
}

fun Activity.navigateTo(target: String, data: Map<String, Any?>? = null) {
    initializeNavigationMap()
    val clazz = navigationMap[target]
        ?: throw NullPointerException("$target is not registered in navigation map!")
    this.startActivity(Intent(this, clazz).apply {
        fillIntentExtra(this, data)
    })
}

private fun fillIntentExtra(intent: Intent, data: Map<String, Any?>? = null) {
    data?.let {
        it.forEach { map ->
            when (map.value) {
                is Int? -> intent.putExtra(map.key, map.value as? Int)
                is Byte? -> intent.putExtra(map.key, map.value as? Byte)
                is Char? -> intent.putExtra(map.key, map.value as? Char)
                is Long? -> intent.putExtra(map.key, map.value as? Long)
                is Float? -> intent.putExtra(map.key, map.value as? Float)
                is Short? -> intent.putExtra(map.key, map.value as? Short)
                is Double? -> intent.putExtra(map.key, map.value as? Double)
                is Boolean? -> intent.putExtra(map.key, map.value as? Boolean)
                is Bundle? -> intent.putExtra(map.key, map.value as? Bundle)
                is String? -> intent.putExtra(map.key, map.value as? String)
                is IntArray? -> intent.putExtra(map.key, map.value as? IntArray)
                is ByteArray? -> intent.putExtra(map.key, map.value as? ByteArray)
                is CharArray? -> intent.putExtra(map.key, map.value as? CharArray)
                is LongArray? -> intent.putExtra(map.key, map.value as? LongArray)
                is FloatArray? -> intent.putExtra(map.key, map.value as? FloatArray)
                is ShortArray? -> intent.putExtra(map.key, map.value as? ShortArray)
                is DoubleArray? -> intent.putExtra(map.key, map.value as? DoubleArray)
                is BooleanArray? -> intent.putExtra(map.key, map.value as? BooleanArray)
                is CharSequence? -> intent.putExtra(map.key, map.value as? CharSequence)
                is Parcelable? -> intent.putExtra(map.key, map.value as? Parcelable)
                is Serializable? -> intent.putExtra(map.key, map.value as? Serializable)
                is Array<*>? -> intent.putExtra(map.key, map.value as? Array<*>)
                else -> throw UnsupportedOperationException("${map.value} is not supported for intent")
            }
        }
    }
}

fun Activity.navigateToAndClose(target: String) {
    navigateToAndClose(target, null)
}

fun Activity.navigateToAndClose(target: String, data: Map<String, Any?>? = null) {
    navigateTo(target, data)
    finish()
}

inline fun <reified T : Activity, D : Parcelable> FragmentActivity.navigateForResult(
    bundle: Bundle,
    requestCode: Int,
    crossinline callback: (success: Boolean, data: D?) -> Unit
) {
    startActivityForResult<T>(bundle,requestCode) {success, data ->
        callback.invoke(success, data.getParcelableExtra<D>(EXTRA_ACTIVITY_RESULT))
    }
}

inline fun <reified T : Activity, D : Parcelable> FragmentActivity.navigateForResult(
    bundle: Bundle,
    crossinline callback: (success: Boolean, data: D?) -> Unit
) {
    startActivityForResult<T>(bundle) {success, data ->
        callback.invoke(success, data.getParcelableExtra<D>(EXTRA_ACTIVITY_RESULT))
    }
}

inline fun <reified D : Parcelable> Activity.setResult(data: D?) {
    setResult(Activity.RESULT_OK, Intent().apply {
        putExtra(EXTRA_ACTIVITY_RESULT, data)
    })
    finish()
}

fun FragmentActivity.navigateToSuccessErrorScreen(
    statusType: Int,
    successsErrorData: SuccessErrorData,
    callback: ((isRetry: Boolean?) -> Unit)? = null
) {
    val bundle = Bundle()
    bundle.putInt(Constants.EXTRA_STATUS_TYPE, statusType)
    bundle.putParcelable(Constants.EXTRA_API_STATUS_DATA, successsErrorData)
    navigateForResult<SuccessErrorActivity, Intent>(bundle) { _, data ->
        val retryFlag = data?.getBooleanExtra(Constants.EXTRA_IS_RETRY, false)
        callback?.invoke(retryFlag)
    }
}

fun FragmentActivity.navigateToBankListScreen(
    type: BankListType,
    callback: (success: Boolean, data: BankInfo?) -> Unit
) {
    val bundle = Bundle()
    bundle.putInt(Constants.EXTRA_BANK_INFO_TYPE, type.ordinal)
    navigateForResult<BankListActivity, BankInfo>(bundle) { success, data ->
        callback.invoke(success, data)
    }
}

fun FragmentActivity.navigateToStateListScreen(
    callback: (success: Boolean, data: State?) -> Unit
) {
    navigateForResult<StateListActivity, State>(Bundle.EMPTY) { success, data ->
        callback.invoke(success, data)
    }
}

fun Fragment.navigateTo(target: String) {
    navigateTo(target, null)
}

fun Fragment.navigateTo(target: String, data: Map<String, Any?>? = null) {
    this.activity?.navigateTo(target, data)
}

fun Fragment.navigateToAndClose(target: String) {
    navigateToAndClose(target, null)
}

fun Fragment.navigateToAndClose(target: String, data: Map<String, Any?>? = null) {
    this.activity?.navigateToAndClose(target, data)
}

fun FragmentActivity.replace(containerId: Int, fragment: Fragment, tag: String? = null) {
    if (tag == null) {
        this.supportFragmentManager.beginTransaction()
            .replace(containerId, fragment)
            .commit()
    } else {
        this.supportFragmentManager.beginTransaction()
            .replace(containerId, fragment)
            .addToBackStack(tag)
            .commit()
    }
}

fun Fragment.replace(containerId: Int, fragment: Fragment, tag: String? = null) {
    if (tag == null) {
        this.childFragmentManager.beginTransaction()
            .replace(containerId, fragment)
            .commit()
    } else {
        this.childFragmentManager.beginTransaction()
            .replace(containerId, fragment)
            .addToBackStack(tag)
            .commit()
    }
}

fun FragmentActivity.popBackStack() {
    this.supportFragmentManager.popBackStack()
}

fun Fragment.popBackStack() {
    if (this.childFragmentManager.fragments.size > 0)
        this.childFragmentManager.popBackStack()
    else
        this.activity?.popBackStack()
}