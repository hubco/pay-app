package com.gappscorp.aeps.library.common.share

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

object Share {

    fun shareImage(context: Context, bitmap: Bitmap,
                   callback: ((isSuccessful: Boolean) -> Unit)? = null) {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val f = File.createTempFile(
            "temp", ".png", context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        ).apply { deleteOnExit() }

        try {
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        context.startActivity(createShareIntent(f))
    }

    private fun createShareIntent(imageFile: File): Intent {
        return Intent(Intent.ACTION_SEND).apply {
            type = "image/*"
            putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile))
        }
    }
}