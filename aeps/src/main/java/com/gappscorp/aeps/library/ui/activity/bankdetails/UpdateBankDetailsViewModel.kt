package com.gappscorp.aeps.library.ui.activity.bankdetails

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.request.BankDetailsRequest
import com.gappscorp.aeps.library.domain.network.response.GenericResponse
import com.gappscorp.aeps.library.domain.repository.BankRepository
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UpdateBankDetailsViewModel(
    private val userRepository: UserRepository,
    private val repository: BankRepository
) : ViewModel() {

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<GenericResponse>>()
    val resultData: LiveData<Result<GenericResponse>>
        get() = _resultData

    val name = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()
    val bankCode = MutableLiveData<String>()
    val ifscCode = MutableLiveData<String>()
    val accountNumber = MutableLiveData<String>()
    val confirmAccountNumber = MutableLiveData<String>()
    val upiId = MutableLiveData<String>()

    val selectBankClickEvent = SingleLiveEvent<Void>()
    val cancelClickEvent = SingleLiveEvent<Void>()

    val isUpdateButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(name) {
            value = isFormValidated()
        }
        addSource(bankCode) {
            value = isFormValidated()
        }
        addSource(ifscCode) {
            value = isFormValidated()
        }
        addSource(accountNumber) {
            value = isFormValidated()
        }
        addSource(confirmAccountNumber) {
            value = isFormValidated()
        }
    }

    private fun isFormValidated(): Boolean {
        return !name.value.isNullOrEmpty()
                && !bankCode.value.isNullOrEmpty()
                && !ifscCode.value.isNullOrEmpty()
                && !accountNumber.value.isNullOrEmpty()
                && !confirmAccountNumber.value.isNullOrEmpty()
                && confirmAccountNumber.value.equals(accountNumber.value)
    }

    init {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getRetailerInfo()?.let {
                name.postValue(it.beneficiaryName)
                bankName.postValue(it.bankName)
                bankCode.postValue(it.bankCode)
                ifscCode.postValue(it.ifscCode)
                accountNumber.postValue(it.accountNo)
                upiId.postValue(it.upiId)
            }
        }
    }

    fun onSelectBankClicked() {
        selectBankClickEvent.call()
    }

    fun onUpdateClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val request = BankDetailsRequest(
                name = name.value,
                bankCode = bankCode.value,
                ifscCode = ifscCode.value,
                accountNumber = accountNumber.value,
                upiId = upiId.value
            )
            val result = repository.updateBankDetails(request)
            when (result) {
                is Result.Success -> {
                    // re-fetch user details
                    userRepository.userDetails()
                    _statusData.postValue(Status.SUCCESS)
                }
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
            _resultData.postValue(result)
        }
    }

    fun onCancelClicked() {
        cancelClickEvent.call()
    }

    override fun onCleared() {
        super.onCleared()
        isUpdateButtonEnabled.removeSource(name)
        isUpdateButtonEnabled.removeSource(bankCode)
        isUpdateButtonEnabled.removeSource(ifscCode)
        isUpdateButtonEnabled.removeSource(accountNumber)
        isUpdateButtonEnabled.removeSource(confirmAccountNumber)
    }
}