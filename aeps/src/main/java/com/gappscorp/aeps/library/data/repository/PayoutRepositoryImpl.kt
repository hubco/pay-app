package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.PayoutService
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.network.response.PayoutResponse
import com.gappscorp.aeps.library.domain.repository.PayoutRepository

class PayoutRepositoryImpl(private val service: PayoutService) : PayoutRepository {
    override suspend fun transfer(
        payoutMode: String?,
        amount: String?,
        remarks: String?
    ): Result<OtpVerificationResponse> {
        return safeCall { service.transfer(payoutMode, amount, remarks) }
    }

    override suspend fun resendOtp(otpId: String): Result<OtpVerificationResponse> {
        return safeCall { service.resendOtp(otpId) }
    }

    override suspend fun verifyOtp(otpId: String, otpCode: String?): Result<PayoutResponse> {
        return safeCall { service.verifyOtp(otpId, otpCode) }
    }
}