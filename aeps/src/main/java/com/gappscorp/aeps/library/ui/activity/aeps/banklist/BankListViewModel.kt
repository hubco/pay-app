package com.gappscorp.aeps.library.ui.activity.aeps.banklist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.data.model.BankListType
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BankListViewModel(private val repository: SettingsRepository) : ViewModel() {

    private val _bankList = MutableLiveData<List<BankInfo>>()
    val bankList: LiveData<List<BankInfo>>
        get() = _bankList

    fun getBankList(type: BankListType) {
        viewModelScope.launch(Dispatchers.IO) {
            when (type) {
                BankListType.BANK_LIST -> _bankList.postValue(repository.getBankList())
                BankListType.AEPS_BANK_LIST -> _bankList.postValue(repository.getAepsBankList())
                BankListType.AEPS_MINI_STATEMENT_BANK_LIST -> _bankList.postValue(repository.getAepsMsBankList())
            }
        }
    }
}