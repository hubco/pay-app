package com.gappscorp.aeps.library.ui.activity.aeps.banklist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gappscorp.aeps.library.common.callback.BankItemCallback
import com.gappscorp.aeps.library.data.model.BankInfo
import com.gappscorp.aeps.library.databinding.AepsLayoutBankItemBinding
import java.util.*

class BankListAdapter(
    private val bankList: List<BankInfo>,
    private val callback: BankItemCallback?
) :
    RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

    private val filteredBankList = bankList.toMutableList()

    fun searchBank(query: String?, callback: (found: Boolean) -> Unit) {
        filteredBankList.clear()
        if (query.isNullOrEmpty()) {
            filteredBankList.addAll(bankList)
        } else {
            filteredBankList.addAll(bankList.filter {
                it.bankName.toLowerCase(Locale.getDefault()).contains(query)
            })
        }
        callback.invoke(filteredBankList.isNotEmpty())
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AepsLayoutBankItemBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolder(binding, callback)
    }

    override fun getItemCount(): Int {
        return if (filteredBankList.isNullOrEmpty()) 0 else filteredBankList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filteredBankList[position])
    }

    inner class ViewHolder(
        val binding: AepsLayoutBankItemBinding,
        private val callback: BankItemCallback?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: BankInfo) {
            binding.item = item
            binding.callback = callback
        }
    }
}

