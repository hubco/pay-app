package com.gappscorp.aeps.library.common.constant

object Constants {

    const val ACTION_SESSION_EXPIRED = "action_session_expired"

    const val EXTRA_OTP_VERIFICATION_DATA = "extra_otp_verification_data"

    const val EXTRA_MINI_STATEMENT_LIST_DATA = "extra_mini_statement_data"

    const val EXTRA_STATUS_TYPE = "extra_status_type"

    const val EXTRA_IS_RETRY = "extra_is_retry"

    const val EXTRA_API_STATUS_DATA = "extra_api_status_data"

    const val EXTRA_REGISTER_RESPONSE = "extra_register_response"

    const val EXTRA_REMITTER_RESPONSE = "extra_remitter_response"

    const val EXTRA_BENEFICIARY_LIST = "extra_beneficiary_list"
    const val EXTRA_BENEFICIARY_INFO = "extra_beneficiary_info"

    const val EXTRA_INDEX = "extra_index"
    const val EXTRA_REMITTER_ID = "extra_remitter_id"
    const val EXTRA_TRANSFER_ID = "extra_transfer_id"
    const val EXTRA_BENEFICIARY_ID = "extra_beneficiary_id"
    const val EXTRA_MONEY_TRANSFER_RESPONSE = "extra_money_transfer_response"
    const val EXTRA_RESPONSE_MESSAGE = "extra_response_message"
    const val EXTRA_VERIFY_SUCCESS_DATA = "extra_verify_success_data"
    const val EXTRA_MODE_TYPE = "extra_mode_type"


    object StatusScreenType {
        const val SUCCESS_SCREEN = 1
        const val ERROR_SCREEN = 2
    }

    object MAtmType{
        const val MATM_BE = "BE"
        const val MATM_CW = "CW"
    }

    const val EXTRA_BANK_INFO_TYPE = "extra_bank_info_type"
    const val EXTRA_PHONE_NUMBER = "extra_phone_number"

    const val ENTER_BENEFICIARY_NAME = "Enter beneficiary name"
    const val ENTER_ACCOUNT_NUMBER = "Enter account number"
    const val ENTER_CONFIRM_ACCOUNT_NUMBER = "Confirm account number"
    const val ENTER_MATCH_ACCOUNT_NUMBER = "Account number is mismatch"
    const val ENTER_BANK_NAME = "Enter bank name"
    const val ENTER_IFSC_CODE = "Enter IFSC Code"
    const val ENTER_VALID_IFSC_CODE = "Enter Valid IFSC Code"

    //MATM PARAM FIELDS
    const val PARAM_PARTNER_ID = "partnerId"
    const val PARAM_API_KEY = "apiKey"
    const val PARAM_TRANSACTION_TYPE = "transactionType"
    const val PARAM_AMOUNT = "amount"
    const val PARAM_REMARKS = "remarks"
    const val PARAM_MOBILE_NUMBER = "mobileNumber"
    const val PARAM_REFERENCE_NUMBER = "referenceNumber"
    const val PARAM_LATITUDE = "latitude"
    const val PARAM_LONGITUDE = "longitude"
    const val PARAM_SUB_MERCHANT_ID = "subMerchantId"
    const val PARAM_DEVICE_MANUFACTURED_ID = "deviceManufacturerId"

}