package com.gappscorp.aeps.library.ui.fragment.retailer

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class PhoneVerificationViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val phoneNumberData = MutableLiveData<String>()
    val closeScreenEvent = SingleLiveEvent<Void>()
    val onRegisterEvent = SingleLiveEvent<Void>()


    private val _statusData = MutableLiveData<Status>()

    val statusData: LiveData<Status>
        get() = _statusData

    private val _resultData = MutableLiveData<Result<PhoneRemitterResponse>>()

    val resultData: LiveData<Result<PhoneRemitterResponse>>
        get() = _resultData

    val proceedButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(phoneNumberData) {
            value = hasPhoneNumber()
        }
    }

    private fun hasPhoneNumber(): Boolean {
        return !phoneNumberData.value.isNullOrEmpty() && phoneNumberData.value?.length==10
    }

    fun onCloseScreen(){
     closeScreenEvent.call()
    }

    fun onRegisterClick(){
        onRegisterEvent.call()
    }
    fun onProceedButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.verifyNumber(phoneNumberData.value!!)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        proceedButtonEnabled.removeSource(phoneNumberData)
    }

}