package com.gappscorp.aeps.library.common.callback

import com.gappscorp.aeps.library.domain.network.response.BeneficiaryInfo

interface BeneficiaryItemCallback {
    fun onBeneficiaryItemClick(item: BeneficiaryInfo,position: Int)
    fun onBeneficiaryRowClick(item: BeneficiaryInfo)
}