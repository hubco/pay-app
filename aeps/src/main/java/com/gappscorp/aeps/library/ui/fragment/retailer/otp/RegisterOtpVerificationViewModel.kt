package com.gappscorp.aeps.library.ui.fragment.retailer.otp

import androidx.lifecycle.*
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.PhoneRemitterResponse
import com.gappscorp.aeps.library.domain.network.response.ResendOtpResponse
import com.gappscorp.aeps.library.domain.repository.RetailerRepository
import kotlinx.coroutines.launch

class RegisterOtpVerificationViewModel(private val retailerRepository: RetailerRepository) : ViewModel() {

    val otpCode = MutableLiveData<String>()

    val isTimerEnabled = MutableLiveData<Boolean>()

    val resendEnable = MediatorLiveData<Boolean>().apply {
        value = true
    }

    val submitButtonEnabled = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(otpCode) {
            value = !otpCode.value.isNullOrEmpty()
        }
    }


    val registerResponse = MutableLiveData<PhoneRemitterResponse>()


    private val _statusData = MutableLiveData<Status>()

    val statusData: LiveData<Status>
        get() = _statusData

    private val _resendStatusData = MutableLiveData<Status>()

    val resendStatusData: LiveData<Status>
        get() = _resendStatusData

    private val _resultData = MutableLiveData<Result<PhoneRemitterResponse>>()

    val resultData: LiveData<Result<PhoneRemitterResponse>>
        get() = _resultData


    private val _resendOtpData = MutableLiveData<Result<ResendOtpResponse>>()

    val resendOtpData: LiveData<Result<ResendOtpResponse>>
        get() = _resendOtpData


    fun onSubmitButtonClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = retailerRepository.verifyOtp(otpCode.value!!,registerResponse.value!!.remitterInfo!!.slug!!)
            _resultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
            otpCode.value = ""
            resendEnable.apply {
                value = true
            }
        }
    }

    fun onResendOtpClick(){
        viewModelScope.launch {
            _resendStatusData.postValue(Status.LOADING)
            val result = retailerRepository.verifyResendOtp(registerResponse.value!!.remitterInfo!!.slug!!)
            _resendOtpData.postValue(result)
            when (result) {
                is Result.Success -> _resendStatusData.postValue(Status.SUCCESS)
                is Result.Error -> _resendStatusData.postValue(Status.ERROR)
            }


        }
    }

    override fun onCleared() {
        super.onCleared()
        submitButtonEnabled.removeSource(otpCode)
    }

}