package com.gappscorp.aeps.library.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class State(val name: String, val code: String) : Parcelable