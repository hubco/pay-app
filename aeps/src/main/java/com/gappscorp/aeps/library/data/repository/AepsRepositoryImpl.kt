package com.gappscorp.aeps.library.data.repository

import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.service.AepsService
import com.gappscorp.aeps.library.domain.network.request.BalanceEnquiryRequest
import com.gappscorp.aeps.library.domain.network.request.CashWithdrawalRequest
import com.gappscorp.aeps.library.domain.network.request.MATMProcessRequest
import com.gappscorp.aeps.library.domain.network.request.MiniStatementRequest
import com.gappscorp.aeps.library.domain.network.response.*
import com.gappscorp.aeps.library.domain.repository.AepsRepository
import okhttp3.RequestBody

class AepsRepositoryImpl(private val service: AepsService) : AepsRepository {

    override suspend fun cashWithdrawal(request: CashWithdrawalRequest): Result<CashWithdrawalResponse> {
        return safeCall("cashWithdrawal.json") { service.cashWithdrawal(request) }
//        return testCashWithdrawalResponse()
    }

    override suspend fun balanceEnquiry(request: BalanceEnquiryRequest): Result<BalanceEnquiryResponse> {
        return safeCall("balanceEnquiry.json") { service.balanceEnquiry(request) }
//        return testBalanceEnquiryResponse()
//        return Result.Error("Something went wrong, please try again!")
    }

    override suspend fun miniStatement(request: MiniStatementRequest): Result<MiniStatementResponse> {
        return safeCall("miniStatement.json") { service.miniStatement(request) }
//        return testMiniStatementResponse()
    }

    override suspend fun sendMATMBeData(request: MATMProcessRequest): Result<MAtmBEResponse> {
        return safeCall{ service.sendMATMBeData(request) }
    }

    override suspend fun processMAtmTransation(response: RequestBody?): Result<MAtmTransactionResponse> {
        return safeCall{ service.processMATMTransaction(response) }
    }

    private fun testCashWithdrawalResponse(): Result.Success<CashWithdrawalResponse> {
        return Result.Success(
            CashWithdrawalResponse(
                status = "SUCCESS",
                message = "Cash Withdrawal is pending!",
                transactionId = "CW123456",
                bankRrnNumber = "Rnn23329327",
                bankName = "ICICI Bank",
                aadhaarNumber = "434398329873",
                availableBalance = "23983287.00",
                amount = "123.45"
            )
        )
    }

    private fun testBalanceEnquiryResponse(): Result.Success<BalanceEnquiryResponse> {
        return Result.Success(
            BalanceEnquiryResponse(
                status = "SUCCESS",
                message = "Mini statement response",
                aadhaarNumber = "64793639368936",
                availableBalance = "26329.00",
                bankName = "ICICI",
                bankRrnNumber = "RNN9382627",
                transactionId = "238723982309"
            )
        )
    }

    private fun testMiniStatementResponse(): Result.Success<MiniStatementResponse> {
        return Result.Success(
            MiniStatementResponse(
                status = "SUCCESS",
                message = "Mini statement response",
                transactionId = "39438389332",
                bankRrnNumber = "Rnn23329327",
                bankName = "ICICI Bank",
                aadhaarNumber = "434398329873",
                availableBalance = "3894389",
                statementList = mutableListOf<MiniStatement>().apply {
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                    add(
                        MiniStatement(
                            statementDate = "10-05-2020",
                            amount = "2000.00",
                            naration = "POS/W/347349398",
                            txnType = "Dr"
                        )
                    )
                }
            ))
    }
}