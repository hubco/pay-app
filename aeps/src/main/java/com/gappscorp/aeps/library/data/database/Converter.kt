package com.gappscorp.aeps.library.data.database

import androidx.room.TypeConverter
import com.gappscorp.aeps.library.data.model.BankInfo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converter {

    @TypeConverter
    fun fromStringToList(value: String): List<String> {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromListToString(list: List<String>): String {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun fromStringToBankList(value: String): List<BankInfo> {
        val type = object : TypeToken<List<BankInfo>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromBankListToString(list: List<BankInfo>): String {
        val type = object : TypeToken<List<BankInfo>>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun fromListOfListToString(list: List<List<String>>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun fromStringToListOfListOfoString(value: String): List<List<String>> {
        val type = object : TypeToken<List<List<String>>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromMapToString(map: Map<String, String>): String {
        return Gson().toJson(map)
    }

    @TypeConverter
    fun fromStringToMap(value: String): Map<String, String> {
        val type = object : TypeToken<Map<String, String>>() {}.type
        return Gson().fromJson(value, type)
    }
}