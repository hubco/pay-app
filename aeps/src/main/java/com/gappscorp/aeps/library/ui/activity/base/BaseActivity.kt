package com.gappscorp.aeps.library.ui.activity.base

import android.app.Dialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.gappscorp.aeps.library.BR
import com.gappscorp.aeps.library.ui.widget.ProgressDialog

abstract class BaseActivity<VDB : ViewDataBinding> : CoreActivity() {

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutRes: Int

    protected lateinit var dataBinding: VDB
    protected val bindingVariable = BR.viewModel

    private var dialog: Dialog? = null
    /**
     * override this variable and set true to show
     * and handle action bar back button implementation
     */
    @get:LayoutRes
    protected open val toolBarId: Int? = null
    protected open val hasUpAction: Boolean = false

    protected open fun registerObserver() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView<VDB>(this, layoutRes).apply {
            lifecycleOwner = this@BaseActivity
            setVariable(bindingVariable, viewModel)
            executePendingBindings()
        }

        // setup toolbar
        toolBarId?.let {
            setupToolBar(findViewById(it), hasUpAction)
        }

        // register required observers
        registerObserver()
    }

    protected fun showProgressDialog() {
        hideProgressDialog()
        dialog = ProgressDialog.show(this, false)
    }

    protected fun hideProgressDialog() {
        dialog?.dismiss()
    }
}