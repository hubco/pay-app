package com.gappscorp.aeps.library.common.callback

import com.gappscorp.aeps.library.data.model.AepsModule

interface AepsModuleCallback {
    fun onAepsModuleClick(module: AepsModule)
}