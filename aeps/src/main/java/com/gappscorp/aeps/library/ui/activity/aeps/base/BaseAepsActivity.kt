package com.gappscorp.aeps.library.ui.activity.aeps.base

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.gappscorp.aeps.library.BuildConfig
import com.gappscorp.aeps.library.R
import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.extensions.DEVICE_AUTHENTICATION_REQUEST
import com.gappscorp.aeps.library.common.extensions.GET_DEVICE_INFO
import com.gappscorp.aeps.library.common.extensions.dialog.errorAlert
import com.gappscorp.aeps.library.common.extensions.getDeviceInfo
import com.gappscorp.aeps.library.common.extensions.getFingerprintScanResult
import com.gappscorp.aeps.library.ui.activity.base.PermissionActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import timber.log.Timber
import java.io.StringReader
import java.util.*

abstract class BaseAepsActivity<VDB : ViewDataBinding> : PermissionActivity<VDB>() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    protected val pidInputXml by lazy {
        """<?xml version="1.0"?>
            <PidOptions ver="1.0">
                <Opts
                    fCount="1"
                    fType="0"
                    iCount="0"
                    pCount="0"
                    format="0"
                    pidVer="2.0"
                    timeout="10000"
                    posh="UNKNOWN"
                    env="P" / >
                <CustOpts >
                    <Param
                        name="mantrakey"
                        value="" / >
                </CustOpts>
            </PidOptions>"""
    }

    protected var pidXmlData: String? = null

    private val testRdServiceData by lazy {
        "<RDService info=\"NEXT Biometrics L0 RD Service for UIDAI AADHAAR\" status=\"READY\">\n" +
                "   <Interface id=\"CAPTURE\" path=\"in.gov.uidai.rdservice.fp.CAPTURE\"/>\n" +
                "   <Interface id=\"DEVICEINFO\" path=\"in.gov.uidai.rdservice.fp.INFO\"/>\n" +
                "</RDService>"
    }

    private val testPidXmlData by lazy {
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<PidData>\n" +
                "   <Data type=\"X\">MjAyMC0wNi0xOVQxMzo1MTozMIzlhEAis7/D72hP6AknpahMMNtxvzyjzhAWQrg04fOLNZmCOzBm9hKi/87iILeEhlb2yNftH5X0c2Yw0J0NVOlsj5hh8S8wGJrZk2DoqYwhM/EsoCJjrZozFterpilxrbG3R4ueTYs19TiRncaTaEUrfdclPrJT8SeThzfsRYAoea/a2Vvl6QBPH9rAdpezStM8eJprvmteBaroOTgB2iimgTo0zaoqNDvvAH+IaO2OEyMlxrr+CNQOfb3PUHJUzhSteVtIvvEU7b1joljgG6xeJ3eDaiIM7IUJaBzTrV5zI7PgDDwVTR+VGJdPtB1AMLC3xxya9RUPpGkgWwhS9HKHEIYGSiLVME3syBZgZeYa1vaezVmllkJMv2FnPzPZaEu2beCrnBTJIeUBqtKdVIP9o+ikPZYB1M7Uu8YFSrkLnSc9r0rKndcXQ/57lWd8H9g0DVpQZjr6QoJOmIMLzIV/uwwfG0OGdBUZ4D2XZbZXR6uWwdXxnUOWbX3S21W+wC9EW2cib8d1H6r7w+qiYbOgnbnqut5yPPkT2fJaLVuUxbjRlkbkx5PfZxO2I9ta68HV7JfifKz0xdR1NuOdpZFdgIiYGrGnOuOFT691GSTBfrArXIlWkekJXjT4q/CXU8gerjQY56Q4iA/5OrtrTWt1XfahT5GexdOHvJtPkUYKH6cFaFefzTtuMsQ5NFtp23lUnYfSTIHl1WjJwv4fXMVD5vcU6AyLsbbsE2dMza1PRsFfVhAGr8DIMbv8TJq9334nBIprLww2LF2l3p2lhl4D3BIZeiCbxxbcRFHZfMogg1uHoQgZ9FefiTAPuT/PCMtw4XQpjmH5h/bvb227UoExRS5ryqVqPbkzy+DBYm/wx2k6KhbH9q8g6x+Cf1Rd27oEaxEB5rWqyjOuqJUW8HCvLl8G9qZPILRvuIG9PcU4iJ4kBqtvE/HghmDzyD9EMAwv6lF/bvFu3FDjnDBie1tgXOOomXLSbryPTFcvbgV4a3k9oSxJTgsS1Y6BOATNG9rIUgIroB6ucU2mXLyo5KgL2eQ6gwl8kumPqAeNnBGzo1zfWQg6rpLASEokMbDck2MOOniJIUEbvtPehiU=</Data>\n" +
                "   <DeviceInfo dc=\"59d95e62-3a38-418f-b317-16cb01af771b\" dpId=\"NEXTBIOMETRICS.AQT\" mc=\"MIID8jCCAtqgAwIBAgIGAXLMpK5XMA0GCSqGSIb3DQEBCwUAMHwxKDAmBgNVBAMMH1JhbmdhcHJhc2FkIE1hZ2FkaSBBbmFudGhhcmFtYW4xEjAQBgNVBAgMCUthcm5hdGFrYTEvMC0GA1UECgwmQVFUUk9OSUNTIFRFQ0hOT0xPR0lFUyBQUklWQVRFIExJTUlURUQxCzAJBgNVBAYTAklOMB4XDTIwMDYxOTEyNTQwNFoXDTIwMDcxOTEyNTQwNFowgdkxLzAtBgNVBAoMJkFRVFJPTklDUyBURUNITk9MT0dJRVMgUFJJVkFURSBMSU1JVEVEMRswGQYDVQQLDBJTb2Z0d2FyZSBTb2x1dGlvbnMxIjAgBgkqhkiG9w0BCQEWE2FkbWluQGFxdHJvbmljcy5jb20xEjAQBgNVBAcMCUJhbmdhbG9yZTESMBAGA1UECAwJS0FSTkFUQUtBMQswCQYDVQQGEwJJTjEwMC4GA1UEAwwncmRfNTlkOTVlNjItM2EzOC00MThmLWIzMTctMTZjYjAxYWY3NzFiMIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQC5f8jkq3Hbnotah24WF5oEqPrnjn5rj5IzMJ3Z9l+ZnOtrFeswF0BMryytK/gWTkQ9YS5umYbSi0p77gXv2OG4qCUlUNn23sM+1nqg+skz/GNc2uFwWdQ6S9RNEysFdoeRxHI+cLTBJGYYKZRcOCYHtRxsl/sRy1E/7K+JutY8JP9rpGs2//D1HC58T2E2d9B+JsS8w1JHgaoNwUHAXj8LtvNyhEnXHK0aPSRxoFfM6KJsxQDq50P9h9DT7j4zP8KLDYzVdmvMeAfqm8tiEQN9Wydeh9lM5gVZvjlrv0af/zshQrBK5oW+EvB8K/RGmJ8XypASZG1qh4DwN70aBjy1AgMBAAGjHTAbMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgGGMA0GCSqGSIb3DQEBCwUAA4IBAQBAnzxmvdv/aFS7OQpO1+a+dLxdxn+hfTCsoix+Aw0JgPYpS25Spz7q+PLfibIzK6y8j38doFgMnvEeq8vmSl0V3CXyNh5ismMjhNDexUT1nAaw8F/El+tbIgtwdiqwEgzi/oMrBRsR1dQYo6rmqmV5TQeBLepMHhQqLZHAS+GtQqQOCJ5Q6WSpAcErxw7NBTb+6JJQ/0Gq6LqbTwIDjabVCoWL910SEvCogCdtYhc+3nbYNcPbyNoe7neAUbsWiZhgRbGTykjsui2ltuueAZX3pZlGohVBjeOH6+Xh6tP5h0fA7fmGp28svstzNX37nCmPBTI/c4bYWRYV+gXtkqfg\" mi=\"NB-3023-U-UID\" rdsId=\"NEXTL0.AND.001\" rdsVer=\"1.0.2\">\n" +
                "      <additional_info>\n" +
                "         <Param name=\"serialNo\" value=\"4E450033004B600E48154E45\" />\n" +
                "         <Param name=\"ts\" value=\"2020-06-19T13:51:30\" />\n" +
                "      </additional_info>\n" +
                "   </DeviceInfo>\n" +
                "   <Hmac>e5xNrVor7E+jXImc6jYrUJ79RissC1//ekhGLIYJmJx6aMz9gtpEVFKaGXUi+Am6</Hmac>\n" +
                "   <Resp errCode=\"0\" errInfo=\"\" fCount=\"1\" fType=\"0\" nmPoints=\"32\" qScore=\"74\" />\n" +
                "   <Skey ci=\"20221021\">hS4wlN1GWJgiX+/WBVCLpoxY+Mv1o3FM9WKURssBXfvMYSFCMQLu5Fm+NhtUy9oQFZ5K/ihNbT80+Q8V4hUo4hgbqsy6DZxMPevbjOfgiP1sJJ4Um/31yu0tc3CvUEUlXm4kq+K/0CNAeQ3fhGcuXMg/3AvhKnkcQfyMIUwHpmbFrInKpDidjIXeqigHiceHnHFqUmfd2Xf77ORw4gbEAHCvx02/B6861TsAovnYt+EoXoz/ZxgW8fwPK8+dauFYJss8R8daDFwNOs5a4x7gdW9Fbc2ASdJyJPZvimYXaaEeqy0eHNnnJ2MsGjrj9IrVY5kTPE4cUEdhh4xHME9ELw==</Skey>\n" +
                "</PidData>"
    }

    abstract fun onDeviceServiceConnected(connected: Boolean, serviceInfo: String?)

    abstract fun onFingerprintScanned(scanned: Boolean, pidData: String?)

    abstract fun onLocationReceived(location: Location?)

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // request external storage permission for logging
        if(BuildConfig.DEBUG) {
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) {}
        }

        // request location permission
        requestPermission(Manifest.permission.ACCESS_FINE_LOCATION) { granted ->
            if (granted) {
                // get last known location
                fusedLocationClient.lastLocation.addOnSuccessListener {
                    onLocationReceived(it)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GET_DEVICE_INFO -> {
                parseDeviceInfo(data?.getDeviceInfo())
            }
            DEVICE_AUTHENTICATION_REQUEST -> {
                parseAuthenticationData(data?.getFingerprintScanResult().toString())
            }
        }
    }

    private fun parseDeviceInfo(deviceInfo: String?) {
        Timber.d("parseDeviceInfo: $deviceInfo")
        if (CoreApp.configuration.offlineMode) {
            onDeviceServiceConnected(true, testRdServiceData)
        } else {
            if (deviceInfo.isNullOrEmpty()) {
                onDeviceServiceConnected(false, null)
                if (deviceInfo?.isEmpty() == true) {
                    errorAlert(getString(R.string.device_not_connected))
                }
            } else {
                // get error/success info
                val data = parseDeviceRdServiceData(deviceInfo)
                Timber.d("parseDeviceInfo XML Data: $data")
                onDeviceServiceConnected(data.success, data.info)
                if (!data.success) {
                    errorAlert(data.info)
                }
            }
        }
    }

    private fun parseAuthenticationData(authData: String?) {
        Timber.d("parseAuthenticationData: $authData")
        if (CoreApp.configuration.offlineMode) {
            onFingerprintScanned(true, testPidXmlData)
        } else {
            if (authData.isNullOrEmpty()) {
                onFingerprintScanned(false, null)
                if (authData?.isEmpty() == true) {
                    errorAlert(getString(R.string.empty_auth_data))
                }
            } else {
                val data = parseFingerprintAuthData(authData)
                Timber.d("parseAuthenticationData XML Data: $data")
                onFingerprintScanned(data.success, data.info)
                if (!data.success) {
                    errorAlert(data.info)
                }
            }
        }
    }

    private fun parseDeviceRdServiceData(data: String): XmlData {
        val xmlFactory = XmlPullParserFactory.newInstance()
        val xpp = xmlFactory.newPullParser()
        var statusCode: String? = null
        var info: String? = null
        xpp.setInput(StringReader(data))
        var eventType: Int = xpp.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            when (eventType) {
                XmlPullParser.START_DOCUMENT -> {
                    Timber.d("Start document")
                }
                XmlPullParser.START_TAG -> {
                    Timber.d("Start tag: ${xpp.name}")
                    if (xpp.name.equals("RDService", ignoreCase = true)) {
                        statusCode = xpp.getAttributeValue(null, "status")
                        info = xpp.getAttributeValue(null, "info")
                    }
                }
                XmlPullParser.END_TAG -> {
                    Timber.d("End tag: ${xpp.name}")
                }
                XmlPullParser.TEXT -> {
                    Timber.d("Text: ${xpp.text}")
                }
            }
            eventType = xpp.next()
        }

        return if (statusCode == null) {
            XmlData(false, info)
        } else {
            when (statusCode.toLowerCase(Locale.getDefault())) {
                "ready" -> XmlData(true, info)
                else -> XmlData(false, info)
            }
        }
    }

    private fun parseFingerprintAuthData(data: String): XmlData {
        val xmlFactory = XmlPullParserFactory.newInstance()
        val xpp = xmlFactory.newPullParser()
        var errorCode: String? = null
        var errorInfo: String? = null
        xpp.setInput(StringReader(data))
        var eventType: Int = xpp.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            when (eventType) {
                XmlPullParser.START_DOCUMENT -> {
                    Timber.d("Start document")
                }
                XmlPullParser.START_TAG -> {
                    Timber.d("Start tag: ${xpp.name}")
                }
                XmlPullParser.END_TAG -> {
                    if (xpp.name.equals("Resp", ignoreCase = true)) {
                        errorCode = xpp.getAttributeValue(null, "errCode")
                        errorInfo = xpp.getAttributeValue(null, "errInfo")
                    }
                    Timber.d("End tag: ${xpp.name}")
                }
                XmlPullParser.TEXT -> {
                    Timber.d("Text: ${xpp.text}")
                }
            }
            eventType = xpp.next()
        }

        // if we got no error in fingerprint auth data,
        // then the data is fingerprint pidData
        return if (errorCode == null) {
            XmlData(true, data)
        } else {
            try {
                val int = Integer.parseInt(errorCode)
                if (int == 0) { // 0 means no error
                    XmlData(true, data)
                } else {
                    XmlData(false, errorInfo)
                }
            } catch (ex: NumberFormatException) {
                XmlData(false, errorInfo)
            }
        }
    }

    inner class XmlData(
        val success: Boolean,
        val info: String? = getString(R.string.alert_generic_error_message)
    ) {
        override fun toString(): String {
            return "success: $success" +
                    "\ninfo: $info"
        }
    }
}