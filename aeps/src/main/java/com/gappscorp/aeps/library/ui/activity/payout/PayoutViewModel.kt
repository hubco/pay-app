package com.gappscorp.aeps.library.ui.activity.payout

import androidx.lifecycle.*
import com.gappscorp.aeps.library.common.architecture.SingleLiveEvent
import com.gappscorp.aeps.library.data.network.Result
import com.gappscorp.aeps.library.data.network.Status
import com.gappscorp.aeps.library.domain.network.response.OtpVerificationResponse
import com.gappscorp.aeps.library.domain.network.response.PayoutResponse
import com.gappscorp.aeps.library.domain.repository.PayoutRepository
import com.gappscorp.aeps.library.domain.repository.SettingsRepository
import com.gappscorp.aeps.library.domain.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PayoutViewModel(
    private val userRepository: UserRepository,
    private val settingsRepository: SettingsRepository,
    private val payoutRepository: PayoutRepository
) : ViewModel() {

    val settingsErrorEvent = SingleLiveEvent<Void>()

    private val _statusData = MutableLiveData<Status>()
    val statusData: LiveData<Status>
        get() = _statusData

    private val _otpRequestResultData = MutableLiveData<Result<OtpVerificationResponse>>()
    val otpRequestResultData: LiveData<Result<OtpVerificationResponse>>
        get() = _otpRequestResultData

    private val _payoutResultData = MutableLiveData<Result<PayoutResponse>>()
    val payoutResultData: LiveData<Result<PayoutResponse>>
        get() = _payoutResultData

    val name = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()
    val ifscCode = MutableLiveData<String>()
    val accountNumber = MutableLiveData<String>()
    val upiId = MutableLiveData<String>()

    val payoutList = MutableLiveData<List<String>>()
    val payoutType = MutableLiveData<String>()
    val amount = MutableLiveData<String>()
    val remarks = MutableLiveData<String>()

    val istTransferButtonEnabled = MediatorLiveData<Boolean>().apply {
        addSource(amount) {
            value = !amount.value.isNullOrEmpty()
        }
    }

    val editBankDetailsClickEvent = SingleLiveEvent<Void>()
    val cancelClickEvent = SingleLiveEvent<Void>()

    fun getBankInfoAndPayoutList() {
        viewModelScope.launch(Dispatchers.IO) {
            // load retailer details
            userRepository.getRetailerInfo().let {
                name.postValue(it?.beneficiaryName)
                bankName.postValue(it?.bankName)
                ifscCode.postValue(it?.ifscCode)
                accountNumber.postValue(it?.accountNo)
                upiId.postValue(it?.upiId)
            }

            // load payout list
            if(settingsRepository.hasSettings()) {
                payoutList.postValue(settingsRepository.getPayoutTypes())
            } else {
                when(val result = settingsRepository.getSettings()) {
                    is Result.Success -> payoutList.postValue(result.data.payoutTypes)
                    is Result.Error -> settingsErrorEvent.call()
                }
            }
        }
    }

    fun onTransferClicked() {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = payoutRepository.transfer(
                payoutType.value,
                amount.value,
                remarks.value
            )
            _otpRequestResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun resendOtp(otpId: String) {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = payoutRepository.resendOtp(otpId)
            _otpRequestResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun verifyOtp(otpId: String, otpCode: String?) {
        viewModelScope.launch {
            _statusData.postValue(Status.LOADING)
            val result = payoutRepository.verifyOtp(
                otpId, otpCode
            )
            _payoutResultData.postValue(result)
            when (result) {
                is Result.Success -> _statusData.postValue(Status.SUCCESS)
                is Result.Error -> _statusData.postValue(Status.ERROR)
            }
        }
    }

    fun onEditBankDetailsClicked() {
        editBankDetailsClickEvent.call()
    }

    fun onCancelClicked() {
        cancelClickEvent.call()
    }

    override fun onCleared() {
        super.onCleared()
        istTransferButtonEnabled.removeSource(amount)
    }
}