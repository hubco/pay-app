package com.gappscorp.aeps.withdrawal.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.gappscorp.aeps.withdrawal.R

object AudioPlayer {

    fun playIntro(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(Intent(context, AudioService::class.java))
        } else {
            context.startService(Intent(context, AudioService::class.java))
        }
    }

    class AudioService : Service() {

        lateinit var mMediaPlayer: MediaPlayer

        override fun onCreate() {
            super.onCreate()

            mMediaPlayer = MediaPlayer.create(applicationContext, R.raw.intro_audio);
            mMediaPlayer.setOnCompletionListener { release() }

            val foregroundId = 1338
            startForeground(
                foregroundId,
                buildForegroundNotification(this)
            );
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            try {
                mMediaPlayer.start();
            } catch (ex: Exception) {
                ex.printStackTrace();
            }
            return super.onStartCommand(intent, flags, startId);
        }

        private fun release() {
            try {
                mMediaPlayer.release();
                stopSelf();
            } catch (ex: Exception) {
                ex.printStackTrace();
            }
        }

        private fun buildForegroundNotification(context: Context): Notification {
            val appName = getString(R.string.app_name)
            val b: NotificationCompat.Builder =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationCompat.Builder(
                        this,
                        createNotificationChannel(context, "channel_id", appName)
                    );
                } else {
                    NotificationCompat.Builder(this);
                }

            b.setOngoing(true)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle(appName)
                .setSmallIcon(R.mipmap.ic_launcher);

            return (b.build());
        }

        override fun onBind(intent: Intent?): IBinder? = null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(
        context: Context,
        channelId: String,
        channelName: String
    ): String {
        val chan = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE;
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE;
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan);
        return channelId;
    }
}
