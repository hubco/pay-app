package com.gappscorp.aeps.withdrawal.app

import com.gappscorp.aeps.library.app.CoreApp
import com.gappscorp.aeps.library.common.config.AepsConfig
import com.gappscorp.aeps.withdrawal.BuildConfig
import com.gappscorp.aeps.withdrawal.utils.AudioPlayer

class AepsApp : CoreApp() {

    override fun onCreate() {
        super.onCreate()
        // play intro audio if flag is true
        if (BuildConfig.PLAY_INTRO_AUDIO) {
            AudioPlayer.playIntro(this)
        }
    }

    override fun aepsConfiguration(): AepsConfig {
        val configuration = AepsConfig()
        configuration.baseUrl = BuildConfig.AEPS_URL
        configuration.hasPoweredByInfo = BuildConfig.DislayPowerby == "1"
        configuration.backButtonEnabled = BuildConfig.LOGIN_BACK_BUTTON
        return configuration
    }

}